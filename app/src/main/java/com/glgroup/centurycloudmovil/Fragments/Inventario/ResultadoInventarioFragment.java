package com.glgroup.centurycloudmovil.Fragments.Inventario;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Controllers.Adapters.ResultInventario;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.CierreInventario;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ResCierreInventario;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ResListaRfid;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultadoInventarioFragment extends Fragment implements MainActivity.IOnBackPressed, View.OnClickListener {

    public ResultadoInventarioFragment() {
        // Required empty public constructor
    }

    /** peticion retrofit **/
    private Call<ListaRfid> reqListaRfid;
    private Call<CierreInventario> reqCierreInventario;

    /** datos obtenidos retrofit **/
    private ListaRfid resListaRfid;
    private CierreInventario resCierreInventario;

    /**adaptador **/
    private ResultInventario resultInventarioAdapter;
    private ArrayList<ResListaRfid> listItemInventario = new ArrayList<>();

    /** resultados mostrados en la vista **/
    private RecyclerView recyclerLectura;
    LinearLayoutManager VerticalLayout;

    /** muestra total leido **/
    private TextView sub_title;

    /** boton finalizar **/
    private Button btnEnd;


    /** token **/
    private String token = "";

    private View view;


    private String etiquetas, contador;

    private AlertCarga alert;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_resultado_inventario, container, false);

        token = Local.getData("login", getContext(), "token");
        //token = Constantes.TOKEN;
        recyclerLectura = view.findViewById(R.id.resultadoLectura);
        sub_title = view.findViewById(R.id.sub_title);
        btnEnd = view.findViewById(R.id.btnEnd);
        btnEnd.setOnClickListener(this);

        Bundle datosRecuperados = getArguments();
        if (datosRecuperados != null) {
           etiquetas = datosRecuperados.getString("etiquetas");
           contador = datosRecuperados.getString("contador");
            sub_title.setText(getString(R.string.etiquetas_leidas)+" "+contador);
           Log.d("resultado", etiquetas);
        }

        alert = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alert.alertCarga();
        procesarLectura();



        return  view;
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new InventarioFragment())
                .commit();
        return true;
    }

    public void procesarLectura(){
        alert.alertShow();
        listItemInventario.clear();
        VerticalLayout = new LinearLayoutManager(getActivity(),  LinearLayoutManager.VERTICAL,false);
        recyclerLectura.setHasFixedSize(true);

        String usuario = Local.getData("login", getContext(), "idUsuario");
        String cliente = Local.getData("login", getContext(), "idCLiente");
        String proyecto = Local.getData("login", getContext(), "idProyecto");
        String locacion = Local.getData("login", getContext(), "idLocacion");
        String area = Local.getData("login", getContext(), "idArea");
        String idInventario = Local.getData("inventario", getContext(), "idInventario");
        reqListaRfid = ApiService.getApiService(getContext()).getlistarfid(usuario, cliente , proyecto, locacion, area, idInventario, etiquetas, "1", Constantes.IMEI, token);
        reqListaRfid.enqueue(new Callback<ListaRfid>() {
            @Override
            public void onResponse(Call<ListaRfid> call, Response<ListaRfid> response) {
                if(response.isSuccessful()){
                    resListaRfid = response.body();
                    int secuencial = 0;
                    for(ResListaRfid data : resListaRfid.getDatos().getResultado()){
                        if(data.getSecuencial() < 0){
                            secuencial = -1;
                        }
                        Local.setData("inventario", getContext(), "idInventario", String.valueOf(data.getIdInventario()));
                        listItemInventario.add(data);
                    }
                    if(secuencial != -1){
                        resultInventarioAdapter = new ResultInventario(listItemInventario);
                        recyclerLectura.setLayoutManager(VerticalLayout);
                        recyclerLectura.setAdapter(resultInventarioAdapter);
                        resultInventarioAdapter.setOnItemClickListener(new ResultInventario.OnItemClickListener(){
                            @Override
                            public void onItemClick(int position){
                                Alertas.mostrarImagen(listItemInventario.get(position).getLinkImagen(),listItemInventario.get(position).getDescripcion(), getContext(), getLayoutInflater());
                            }
                        });
                    }
                }
                alert.alerDismiss();
            }

            @Override
            public void onFailure(Call<ListaRfid> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnEnd:
                alertobservacion();
                break;
        }
    }

    /** Alert para solicitar observaciones al cerrar la lectura **/
    public void alertobservacion(){
        final android.app.AlertDialog alertObservacion;
        final android.app.AlertDialog.Builder finalizar = new android.app.AlertDialog.Builder(getContext());


        View dialog = getLayoutInflater().inflate(R.layout.alert_observacion, null);

        final Button ok = dialog.findViewById(R.id.btnCerrar);
        final TextInputLayout observacion = dialog.findViewById(R.id.edObservacion);

        finalizar.setView(dialog);

        alertObservacion = finalizar.create();
        alertObservacion.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertObservacion.dismiss();
                cierreInventario(observacion.getEditText().getText().toString());
            }
        });
        alertObservacion.show();
    }


    /** cierre de inventario **/
    public void cierreInventario(String observacion){
        alert.alertShow();
        String usuario = Local.getData("login", getContext(), "idUsuario");
        String cliente = Local.getData("login", getContext(), "idCLiente");
        String proyecto = Local.getData("login", getContext(), "idProyecto");
        String locacion = Local.getData("login", getContext(), "idLocacion");
        String area = Local.getData("login", getContext(), "idArea");
        String idInventario = Local.getData("inventario", getContext(), "idInventario");
        reqCierreInventario = ApiService.getApiService(getContext()).getcierreinventario(usuario, cliente, proyecto, locacion, area, idInventario, etiquetas, "1", observacion, "866502031287673", token);
        reqCierreInventario.enqueue(new Callback<CierreInventario>() {
            @Override
            public void onResponse(Call<CierreInventario> call, Response<CierreInventario> response) {
                if(response.isSuccessful()){
                    alert.alerDismiss();
                    resCierreInventario = response.body();
                    validaRespuesta();
                }
                alert.alerDismiss();
                if(response.code() == 400){
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Gson gson = new Gson();
                        resCierreInventario = gson.fromJson(String.valueOf(jObjError), CierreInventario.class);
                        validaRespuesta();
                    } catch (Exception e) {
                        Alertas.errorCierre(getString(R.string.error_interno), getContext());
                    }
                }
            }
            @Override
            public void onFailure(Call<CierreInventario> call, Throwable t) {

            }
        });
    }



    public void validaRespuesta(){
        int idMsg = 0;
        String mensaje = "";
        for(ResCierreInventario data : resCierreInventario.getDatos().getResultado()){
            idMsg = data.getIdMensaje();
            mensaje = data.getMensaje();
        }
        if(idMsg > 0){
            Toasty.success(getContext(), mensaje, Toasty.LENGTH_LONG).show();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new DefaultFragment())
                    .commit();
        }else{
            Alertas.errorCierre(mensaje, getContext());
        }
    }
}
