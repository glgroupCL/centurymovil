package com.glgroup.centurycloudmovil.Fragments.Busqueda;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Adapters.AdaptadorListaRfidBusqueda;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.Application.Busqueda.ListaBusquedaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Busqueda.ResListaBusquedaRfid;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.ActivityEscanear;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.textfield.TextInputLayout;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusquedaCenturyFragment extends Fragment implements MainActivity.IOnBackPressed, View.OnClickListener{
    /** vista **/
    private View view;
    private MainActivity mContext;

    /** peticion retrofit **/
    private Call<ListaBusquedaRfid> reqListaRfid;

    /** datos obtenidos retrofit **/
    private ListaBusquedaRfid resListaRfid;


    /** elementos de la vista **/
    private EditText ed_sku;
    private ImageView btn_sku;
    private ImageView imgLectura;
    private TextView textLectura, result;
    private CardView card_clean, card_buscar;
    private ProgressBar searchBar;

    /**adaptador y lista de items rfid **/


    /** variables de datos **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;
    private String epcBuscar ="";


    public String tagEpc = "";
    private long tagLastSeen = 0;
    /* access modifiers changed from: private */
    public int tagRange = 0;
    /* access modifiers changed from: private */
    public String tagRssi = "0";
    public RangeLevel[] ranges;
    public int currentRangeLevel = 0;
    public boolean closing = false;


    class RangeLevel {

        /* renamed from: mp */
        MediaPlayer f32mp;
        String sound;
        int value;

        RangeLevel(int value2, String sound2) {
            this.value = value2;
            this.sound = sound2;
            if (sound2 != null) {
                try {
                    this.f32mp = new MediaPlayer();
                    AssetFileDescriptor afd = getActivity().getAssets().openFd(sound2);
                    this.f32mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    afd.close();
                    this.f32mp.setLooping(true);
                    this.f32mp.prepare();
                } catch (Exception e) {
                    Log.e("ERROR", "Error play range audio : " + e);
                    e.printStackTrace();
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void play() {
            if (this.f32mp != null) {
                this.f32mp.start();
            }
        }

        /* access modifiers changed from: 0000 */
        public void pause() {
            if (this.f32mp != null) {
                this.f32mp.pause();
            }
        }
    }




    /** variable de lectura **/
    boolean isRuning = false;
    private boolean loopFlag = false;

    /** clase para la conexión de dispositivos **/
    private ConnectStatus mConnectStatus = new ConnectStatus();


    /** se controla si el dispositivo se encuentra conectado o no **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Toast.makeText(getContext(),"Conectado en fragment", Toast.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
                //Toast.makeText(getContext(),"Desconectado en fragment", Toast.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }


    /**
     * hilo para recibir datos de lectura
     **/
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        /** error al detener la lectura **/
                        Utils.playSound(2);
                        Toast.makeText(mContext, "R.string.uhf_msg_inventory_stop_fail", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constantes.FLAG_START:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        Utils.playSound(2);
                    }

                    break;

                case Constantes.FLAG_UHFINFO:
                    /** se reciben los datos de las etiquetas **/
                    UHFTAGInfo tag = (UHFTAGInfo) msg.obj;
                    if(tag != null) {
                        if (!TextUtils.isEmpty(tag.getEPC())) {
                            if ((tag.getEPC().contains(epcBuscar))) {
                                TagProximator.addData(tag.getEPC(), Double.parseDouble(tag.getRssi()));
                                double normalizeRssi = (double) TagProximator.getProximity(tag.getEPC());
                                int range = TagProximator.getScaledProximity(tag.getEPC());
                                tagLastSeen = System.currentTimeMillis();
                                tagEpc = tag.getEPC();
                                tagRssi = String.format("%1$,.1f", new Object[]{Double.valueOf(normalizeRssi)});
                                tagRange = Math.min(range, 100);
                                tagRange = Math.max(range, 0);

                                String epc = tagEpc;
                                String rssi = tagRssi;
                                int range2 = tagRange;
                                int index = 0;
                                String sEpc = "";
                                while (index < epc.length()) {
                                    String sEpc2 = sEpc + epc.substring(index, Math.min(index + 4, epc.length()));
                                    index += 4;
                                    sEpc = sEpc2 + (index % 12 != 0 ? " " : "\n");
                                }

                                searchBar.setProgress(range2);
                            } else {
                                refreshDetection();
                            }
                        } else {
                            refreshDetection();
                        }
                    }else {
                        refreshDetection();
                    }




//                    if (!TextUtils.isEmpty(info.getEPC())) {
//                        if ((info.getEPC().contains(epcBuscar))){
//
//
//                            //se obtiene el valor del rssi de la etiqueta encontrada y se eliminan los valores de linea
//                            String rssi = String.valueOf(info.getRssi()).replace("-","");
//
//                            //se transforma a numero
//                            int valorRssi = Integer.parseInt(rssi.replace(",",""));
//
//                            //se valida el valor del rssi para cambiar el estado de la barra de progreso
//                            //mientras menor sea el numero, más cerca está el dispositivo de la etiqueta
//                            if(valorRssi > 8001){
//                                searchBar.setProgress(0);
//                            }
//
//                            if(valorRssi > 7100 && valorRssi < 8000){
//                                searchBar.setProgress(20);
//
//                            }
//                            if(valorRssi > 6900 && valorRssi < 7090){
//                                searchBar.setProgress(35);
//
//                            }
//                            if(valorRssi > 6300 && valorRssi < 6890){
//                                searchBar.setProgress(50);
//
//                            }
//                            if(valorRssi > 5600 && valorRssi < 6290){
//                                searchBar.setProgress(60);
//                            }
//                            if(valorRssi > 4900 && valorRssi < 5590){
//                                searchBar.setProgress(80);
//
//                            }
//                            if(valorRssi > 4400 && valorRssi < 4890){
//                                searchBar.setProgress(90);
//
//                            }
//                            if(valorRssi > 3800 && valorRssi < 4390){
//                                searchBar.setProgress(100);
//
//                            }
//                        }
//                    }

                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_busqueda_century, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();

        ranges = new RangeLevel[]{new RangeLevel(0, null), new RangeLevel(30, "quietest_snd.mp3"), new RangeLevel(40, "default_snd.mp3"), new RangeLevel(60, "loudest_snd.mp3"), new RangeLevel(100, "success_snd.mp3")};
        startSoundFeedbackThread();

        init();

        if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            mContext.uhf.setPower(30);
        }
        mContext.addConnectStatusNotice(mConnectStatus);

        /** acción recibida desde el lector **/
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {
                if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    String nameDevice = Local.getData("dipositivo", getContext(), "nombre");
                    if (nameDevice.contains("R6")) {
                        if (ed_sku.getText().toString().equals("")) {
                            scan();
                        } else {
                            if(!epcBuscar.equals("")) {
                                lectura();
                            }
                        }
                    }else{
                        if(!epcBuscar.equals("")) {
                            lectura();
                        }
                    }
                }
            }
        });
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }


    public void init(){
        ed_sku = view.findViewById(R.id.ed_sku);
        ed_sku.setOnClickListener(this);

        result = view.findViewById(R.id.result);

        textLectura = view.findViewById(R.id.textLectura);
        imgLectura = view.findViewById(R.id.imgLectura);

        btn_sku = view.findViewById(R.id.btn_sku);
        btn_sku.setOnClickListener(this);

        card_clean = view.findViewById(R.id.card_clean);
        card_clean.setOnClickListener(this);

        card_buscar = view.findViewById(R.id.card_buscar);
        card_buscar.setOnClickListener(this);

        searchBar = view.findViewById(R.id.searchBar);

        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
    }

    private void startSoundFeedbackThread() {
        new Thread(new Runnable() {
            public void run() {
                while (!closing) {
                    refreshDetection();
                    int oldRangeLevel = currentRangeLevel;
                    int i = 0;
                    while (true) {
                        if (i >= ranges.length) {
                            break;
                        } else if (tagRange <= ranges[i].value) {
                            currentRangeLevel = i;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (oldRangeLevel != currentRangeLevel) {
                        ranges[oldRangeLevel].pause();
                        ranges[currentRangeLevel].play();
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void refreshDetection() {
        if (System.currentTimeMillis() - this.tagLastSeen > 1000 && !"0".equals(tagRssi)) {
            resetDetection();
        }
    }

    private void resetDetection() {
        this.tagEpc = "";
        this.tagRssi = "0";
        this.tagRange = 0;
        this.ranges[this.currentRangeLevel].pause();
        this.currentRangeLevel = 0;
        searchBar.setProgress(tagRange);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_sku:
                if (!Local.getData("permisos", getContext(), "camara").equals("true")) {
                    // Toast.makeText(getActivity(), "Habilitando permisos, vuelve a intentarlo", Toast.LENGTH_SHORT).show();
                    //  permisoSolicitadoDesdeBoton = true;
                    Local.setData("permisos", getContext(), "boton_camara", "true");
                    verificarYPedirPermisosDeCamara();
                    return;
                }
                verificarYPedirPermisosDeCamara();
                break;

            case R.id.ed_sku:
                alertSolicitaSku();
                break;

            case R.id.card_clean:
                limpiar();
                break;

            case R.id.card_buscar:
                if(mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if(!epcBuscar.equals("")) {
                        lectura();
                    }else{
                        Toasty.warning(getContext(),"Debe seleccionar un item de la lista antes de buscar", Toasty.LENGTH_SHORT).show();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                }

        }
    }

    /** limpiar vista **/
    public void limpiar(){
        result.setText("");
        card_buscar.setVisibility(View.INVISIBLE);
        ed_sku.setText("");
        epcBuscar = "";
    }

    /** ingresar sku manualmente **/
    public void alertSolicitaSku(){
        final android.app.AlertDialog alertSku;
        final android.app.AlertDialog.Builder solicitaSku = new android.app.AlertDialog.Builder(getContext());


        View dialog = getLayoutInflater().inflate(R.layout.alert_ingresa_sku, null);

        final TextInputLayout codigo = dialog.findViewById(R.id.ed_sku);
        final Button ok = dialog.findViewById(R.id.btn_validar);
        final Button cancel = dialog.findViewById(R.id.btn_regresar);

        solicitaSku.setView(dialog);

        alertSku = solicitaSku.create();
        alertSku.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sku = codigo.getEditText().getText().toString().trim();
                if(sku.equals("")){
                    codigo.setErrorEnabled(true);
                    codigo.setError(getString(R.string.requerido));
                }else{
                    codigo.setErrorEnabled(false);
                    alertSku.dismiss();
                    obtenerRfid(sku);
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertSku.dismiss();
            }
        });
        alertSku.show();
    }


    /** se define si detener o comenzar la lectura **/
    public void lectura(){
        if (!loopFlag) {
            if(!mContext.isScanning) {
                detener();
                startThread();
            }else{
                Toasty.warning(getContext(), getString(R.string.proceso_de_lectura), Toasty.LENGTH_LONG).show();
            }
        }else{
            iniciar();
            stopInventory();
        }
    }

    /** cambiar color de botones **/
    public void iniciar(){
        card_buscar.setCardBackgroundColor(Color.parseColor("#003D70"));
        imgLectura.setImageResource(R.drawable.ic_play);
        textLectura.setText(getString(R.string.iniciar));
    }

    public void detener(){
        card_buscar.setCardBackgroundColor(Color.parseColor("#FFB71C1C"));
        imgLectura.setImageResource(R.drawable.ic_pause);
        textLectura.setText(getString(R.string.detener));
        resetDetection();
    }

    /** SDK **/
    public void startThread() {
        if(mContext.uhf.getPower() != 30){
            mContext.uhf.setPower(30);
        }
        if (isRuning) {
            return;
        }
        isRuning = true;
        // cbFilter.setChecked(false);
        new TagThread().start();
    }

    /** proceso de lectura rfid **/
    class TagThread extends Thread {
        public void run() {
            Message msg = handler.obtainMessage(Constantes.FLAG_START);
            if (mContext.uhf.startInventoryTag()) {
                loopFlag = true;
                mContext.isScanning = true;
                //mStrTime = System.currentTimeMillis();
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            handler.sendMessage(msg);
            isRuning = false;//执行完成设置成false
            while (loopFlag) {
                getUHFInfo();
                handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
            }
            stopInventory();
        }
    }

    /** obtiene información de la etiqueta y la envía al handler **/
    private void getUHFInfo() {

        List<UHFTAGInfo> list = mContext.uhf.readTagFromBufferList_EpcTidUser();

        if (list != null && !list.isEmpty()) {
            for (int k = 0; k < list.size(); k++) {
                Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO, list.get(k));
                Log.d("tag", String.valueOf(msg));
                handler.sendMessage(msg);
                if(!loopFlag) {
                    break;
                }
            }
        }
    }

    /** se detiene el proceso de inventario **/
    private void stopInventory() {
        searchBar.setProgress(0);
        stopInventory();
        loopFlag = false;
        boolean result = mContext.uhf.stopInventory();
        if(mContext.isScanning) {
            ConnectionStatus connectionStatus = mContext.uhf.getConnectStatus();
            Message msg = handler.obtainMessage(Constantes.FLAG_STOP);
            if (result || connectionStatus == ConnectionStatus.DISCONNECTED) {
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
                //getUHFInfoEx();
            }
            mContext.isScanning = false;
            handler.sendMessage(msg);
        }
    }




    /**
     * lector de barras
     **/
    private synchronized void scan() {

        if (!isRuning) {
            isRuning = true;
            new ScanThread().start();
        }
    }

    class ScanThread extends Thread {
        public void run() {
            String data = null;
            byte[] temp = mContext.uhf.scanBarcodeToBytes();
            if (temp != null) {
                try {
                    data = new String(temp, "utf8");
                } catch (Exception e) {

                }
                if (data != null && !data.isEmpty()) {
                    obtenerRfid(data);
                }
            }
        }
    }

    /** permisos para usar la camara como lector de barras **/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constantes.CODIGO_PERMISOS_CAMARA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Escanear directamten solo si fue pedido desde el botón
                    if (Local.getData("permisos", getContext(), "boton_camara").equals("true")) {
                        escanear();
                    }
                    Local.setData("permisos", getContext(), "camara", "true");
                    //  permisoCamaraConcedido = true;
                } else {
                    Local.setData("permisos", getContext(), "camara", "false");
                    Toast.makeText(getActivity(), getString(R.string.error_permisos), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void verificarYPedirPermisosDeCamara() {
        int estadoDePermiso = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (estadoDePermiso == PackageManager.PERMISSION_GRANTED) {
            // En caso de que haya dado permisos ponemos la bandera en true
            // y llamar al método
            // permisoCamaraConcedido = true;
            Local.setData("permisos", getContext(), "camara", "true");
            escanear();
        } else {
            // Si no, pedimos permisos. Ahora mira onRequestPermissionsResult
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    Constantes.CODIGO_PERMISOS_CAMARA);
        }
    }

    /**
     * se accede a la vista para la lectura de codigo
     **/
    private void escanear() {
        Intent i = new Intent(getActivity(), ActivityEscanear.class);
        startActivityForResult(i, Constantes.CODIGO_INTENT);
    }

    /**
     * se obtiene la lectura de codigo de barras
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constantes.CODIGO_INTENT) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    String codigo = data.getStringExtra("codigo");
                    obtenerRfid(codigo);
                    //txt_barcode.setText(codigo);
//                    String result = NombreItem.obtenerNombre(getContext(), codigo);
//                    if (!result.equals("")) {
//
//                        ed_sku.setText(codigo);
//                        resultado.setText(result);
//                    } else {
//                        Toasty.warning(getContext(), "El código de barras no se encuentra registrado", Toasty.LENGTH_LONG).show();
//                    }
                    //obtenerNombre(codigo);
                }
            }
        }
    }

    /** obtener lista de rfid century asociados al sku **/
    public void obtenerRfid(final String sku){
        reqListaRfid = ApiService.getApiService(getContext()).getlistabusquedacentury(usuario,cliente,proyecto,locacion,sku,token);
        reqListaRfid.enqueue(new Callback<ListaBusquedaRfid>() {
            @Override
            public void onResponse(Call<ListaBusquedaRfid> call, Response<ListaBusquedaRfid> response) {
                if(response.isSuccessful()){
                    resListaRfid = response.body();
                    int validar = 0;
                    String rfid = "";
                    for(ResListaBusquedaRfid data: resListaRfid.getDatos().getResultado()){
                        validar = data.getIdMsj();
                        rfid = data.getRfid();
                        result.setText("RFID: "+rfid+"\n"+data.getDescripcion());
                    }
                    if(validar > 0){
                        ed_sku.setText(sku);
                        card_buscar.setVisibility(View.VISIBLE);
                        epcBuscar = rfid;
                    }else{
                        ed_sku.setText("");
                    }

                }
            }

            @Override
            public void onFailure(Call<ListaBusquedaRfid> call, Throwable t) {

            }
        });
    }
}