package com.glgroup.centurycloudmovil.Fragments.Salida;

import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Controllers.Adapters.AdaptadorDetalleLecturaRfid;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.EnvioListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ListaRfidDoc;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ResEnvioListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ResListaRfidDoc;
import com.glgroup.centurycloudmovil.Models.Datos.DetalleLecturaRfid;
import com.glgroup.centurycloudmovil.Models.Datos.ObsLecturaRfid;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleLecturaSalidaFragment extends Fragment implements MainActivity.IOnBackPressed, View.OnClickListener{

    /**
     * vista y contexto
     **/
    private MainActivity mContext;
    private View view;


    /**
     * peticion retrofit
     **/
    private Call<ListaRfidDoc> reqListaRfidDoc;
    private Call<EnvioListaRfid> reqEnvioListaRfid;

    /**
     * respuesta retrofit
     **/
    private ListaRfidDoc resListarfidDoc;
    private EnvioListaRfid resEnvioListaRfid;

    /**
     * variables de datos
     **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;
    private String area;
    private String numeroDocumento, idProveedor, idDocumento, tipoDocumento;
    private String etiquetas;

    /** elementos de la vista **/
    private TextView txt_titulo, txt_lectura, txt_efectividad;
    private ListView listDetalle;
    private CardView card_clean, card_send;

    /** lista de datos **/
    private ArrayList<DetalleLecturaRfid> listItems = new ArrayList<>();

    /** adaptador **/
    private AdaptadorDetalleLecturaRfid mAdapter;

    /** ventana de carga **/
    private AlertCarga alert, alertProcesar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_detalle_lectura_salida, container, false);
        init();
        obtenerResutado();
        return view;
    }

    public void init(){
        alert = new AlertCarga(getString(R.string.obteniendo_datos), getActivity(), getLayoutInflater());
        alert.alertCarga();

        alertProcesar = new AlertCarga(getString(R.string.procesando_lectura), getActivity(), getLayoutInflater());
        alertProcesar.alertCarga();

        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
        area = Local.getData("login", getContext(), "idArea");
        numeroDocumento = Local.getData("docSalida", getContext(), "numero_documento");
        idProveedor = Local.getData("docSalida", getContext(), "id_proveedor");
        idDocumento = Local.getData("docSalida", getContext(), "id_documento");
        tipoDocumento = Local.getData("docSalida", getContext(), "tipo_documento");
        etiquetas = Local.getData("docSalida", getContext(), "etiquetas");

        txt_titulo = view.findViewById(R.id.txt_title);
        txt_lectura = view.findViewById(R.id.txt_lectura);
        txt_efectividad = view.findViewById(R.id.txt_efectividad);

        listDetalle = view.findViewById(R.id.listDetalle);

        card_clean = view.findViewById(R.id.card_clean);
        card_clean.setOnClickListener(this);

        card_send = view.findViewById(R.id.card_send);
        card_send.setOnClickListener(this);
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new ProcesarSalidaFragment())
                .commit();

        return true;
    }

    /** se obtiene el resultado de la lectura de salida **/
    public void obtenerResutado() {
        alert.alertShow();
        reqListaRfidDoc = ApiService.getApiService(getContext()).getlistarfidinout(usuario, cliente, proyecto, locacion, area, idDocumento, tipoDocumento, numeroDocumento, idProveedor, etiquetas, token);
        reqListaRfidDoc.enqueue(new Callback<ListaRfidDoc>() {
            @Override
            public void onResponse(Call<ListaRfidDoc> call, Response<ListaRfidDoc> response) {
                if(response.isSuccessful()){
                    listItems.clear();
                    resListarfidDoc = response.body();
                    for(ResListaRfidDoc data : resListarfidDoc.getDatos().getResultado()){
                        /** titulos de la vista **/
                        if(data.getIdSegmento() == 3){
                            if(data.getValor1() == 1){
                                txt_titulo.setText(data.getTxtValor1());
                            }
                            if(data.getValor1() == 2){
                                txt_lectura.setText(data.getTxtValor1());
                            }
                            if(data.getValor1() == 3){
                                txt_efectividad.setText(data.getTxtValor1());
                            }
                        }

                        /** información del detalle de lectura **/
                        if(data.getIdSegmento() == 4){
                            listItems.add(new DetalleLecturaRfid(data.getTxtValor1(),String.valueOf(data.getIntValor1()), String.valueOf(data.getIntValor2()), String.valueOf(data.getIntValor3())));
                        }
                    }
                    alert.alerDismiss();
                    mAdapter = new AdaptadorDetalleLecturaRfid(getContext(),listItems);
                    listDetalle.setAdapter(mAdapter);
                }else{
                    alert.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<ListaRfidDoc> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_clean:
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new LecturaSalidaFragment())
                        .commit();

                break;

            case R.id.card_send:
                alertobservacion();
                break;
        }
    }

    /** Alert para solicitar observaciones al cerrar la lectura **/
    public void alertobservacion(){
        final android.app.AlertDialog alertObservacion;
        final android.app.AlertDialog.Builder finalizar = new android.app.AlertDialog.Builder(getContext());


        View dialog = getLayoutInflater().inflate(R.layout.alert_observacion, null);

        final Button ok = dialog.findViewById(R.id.btnCerrar);
        final TextInputLayout observacion = dialog.findViewById(R.id.edObservacion);

        finalizar.setView(dialog);

        alertObservacion = finalizar.create();
        alertObservacion.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertObservacion.dismiss();
                enviarLectura(observacion.getEditText().getText().toString());
            }
        });
        alertObservacion.show();
    }

    /**envío de lectura **/
    public void enviarLectura(String observacion){
        alertProcesar.alertShow();
        reqEnvioListaRfid = ApiService.getApiService(getContext()).getenviorfidinout(usuario,cliente,proyecto,locacion,area,idDocumento,tipoDocumento,numeroDocumento,idProveedor,etiquetas,observacion,"0", Constantes.IMEI,token);
        reqEnvioListaRfid.enqueue(new Callback<EnvioListaRfid>() {
            @Override
            public void onResponse(Call<EnvioListaRfid> call, Response<EnvioListaRfid> response) {
                if(response.isSuccessful()){
                    resEnvioListaRfid = response.body();
                    int validar = 0;
                    String mensaje = "";
                    ResEnvioListaRfid content = null;
                    for(ResEnvioListaRfid data : resEnvioListaRfid.getDatos().getResultado()){
                        validar = data.getIdMsg();
                        content = data;
                    }
                    alertProcesar.alerDismiss();
                    if(validar > 0){
                        if(content != null && content.getIdMsg() == 1){
                            Alertas.alertCorrecto(getString(R.string.aviso), (content.getMsg1()==null ? "":content.getMsg1()), getContext(), getLayoutInflater(), mContext);
                        }else{
                            Alertas.alertIncorrecto((content.getMsg1()==null ? "":content.getMsg1())+"\n"+(content.getMsg2()==null ? "":content.getMsg2()), (content.getMsg3()==null ? "":content.getMsg3()), getContext(), getLayoutInflater(), mContext);
                        }
                    }else{
                        Alertas.errorCierre(content.getMsg1(), getContext());
                    }
                }else{
                    alertProcesar.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<EnvioListaRfid> call, Throwable t) {
                alertProcesar.alerDismiss();
            }
        });

    }
}