package com.glgroup.centurycloudmovil.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.Peticiones.LoginHttp;
import com.glgroup.centurycloudmovil.Models.Application.Login;
import com.glgroup.centurycloudmovil.R;

import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Validaciones;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.glgroup.centurycloudmovil.ui.UbicacionesActivity;
import com.google.android.material.textfield.TextInputLayout;


import retrofit2.Call;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.TELEPHONY_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginCredentialFragment extends Fragment implements View.OnClickListener {

    public LoginCredentialFragment() {
        // Required empty public constructor
    }

    public static LoginCredentialFragment newInstance() {
        return new LoginCredentialFragment();
    }

    /** elementos de la vista **/
    private TextView selectFinger;
    private Switch switchRecordar;
   // private FingerprintManager fingerprintManager;
    private Button btnLogin;
    private TextInputLayout user, pass;
    private LottieAnimationView animation_view;

    /** vista del fragment **/
    View view;


    /** Validaciones de campos **/
    Validaciones validaciones = new Validaciones();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login_credential, container, false);

        init();

        return view;
    }

    /** inicio de variables **/
    public void init() {
     //   fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);

        animation_view = view.findViewById(R.id.animation_view);

        /** mide los detalles de la pantalla **/
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        if(widthPixels == 1080 || widthPixels == 720){

            animation_view.setVisibility(View.GONE);
            if (widthPixels == 720 && metrics.heightPixels > 1300){
                animation_view.setVisibility(View.VISIBLE);
            }
            if(widthPixels == 1080 && metrics.heightPixels > 1300){
                animation_view.setVisibility(View.VISIBLE);
            }
        }

        selectFinger = view.findViewById(R.id.selecthuella);
        selectFinger.setOnClickListener(this);

        btnLogin = view.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        user = view.findViewById(R.id.edUser);
        pass = view.findViewById(R.id.edPass);

        switchRecordar = view.findViewById(R.id.switchRecordar);
        switchRecordar.setOnClickListener(this);

        if(Local.getData("login",getContext(), "recordar").equals("1")){
            switchRecordar.setChecked(true);
            user.getEditText().setText(Local.getData("login",getContext(), "user"));
            pass.getEditText().setText(Local.getData("login",getContext(), "pass"));
        }


        if (Local.getData("acceso", getContext(), "finger").equals("1")) {
            //if (!fingerprintManager.isHardwareDetected()) {
            if (true) {
                selectFinger.setVisibility(View.GONE);
            }
        }else{
            selectFinger.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.selecthuella:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameContent, LoginFingerFragment.newInstance())
                        .commitNow();
                break;

            case R.id.btnLogin:
              //  startActivity(new Intent(getContext(), UbicacionesActivity.class));
                if(validaciones.camposLogin(user,pass, getContext())){
                    Local.setData("login",getContext(), "user", user.getEditText().getText().toString());
                    Local.setData("login",getContext(), "pass", pass.getEditText().getText().toString());
                    LoginHttp loginClass = new LoginHttp();
                    //logIn("gl","202cb962ac59075b964b07152d234b70", "866502031287673");
                    loginClass.logIn(user.getEditText().getText().toString(), pass.getEditText().getText().toString(),Constantes.IMEI,getActivity(), getLayoutInflater());
                }
                break;

            case R.id.switchRecordar:
                if(switchRecordar.isChecked()){ Local.setData("login",getContext(), "recordar", "1");  }else { Local.setData("login",getContext(), "recordar", "0");  }
        }
    }


    /** no aplica en dispositivos 10+ **/
//    public String Imei(){
//        String imei = "";
//        TelephonyManager device = (TelephonyManager) getActivity().getSystemService(TELEPHONY_SERVICE);
//        try {
//            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                int msg = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE);
//                System.out.println(msg);
//
//            }else {
//                imei = device.getDeviceId();
//            }
//        }catch (Exception e){
//            imei = String.valueOf(e);
//        }
//        return imei;
//    }





}
