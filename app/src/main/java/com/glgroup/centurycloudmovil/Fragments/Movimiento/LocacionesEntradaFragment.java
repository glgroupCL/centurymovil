package com.glgroup.centurycloudmovil.Fragments.Movimiento;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.LocacionesInOut.LocacionInOut;
import com.glgroup.centurycloudmovil.Models.Application.LocacionesInOut.ResLocacion;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LocacionesEntradaFragment extends Fragment {
    /** vista **/
    private View view;

    /** elementos de la vista **/
    private ListView list_locaciones;
    private LinearLayout linInfoDocument;

    /** peticiones retrofit **/
    private Call<LocacionInOut> reqLocaciones;

    /** respuesta retrofit **/
    private LocacionInOut resLocaciones;

    /** locaciones obtenidas **/
    private ArrayList<String> locaciones = new ArrayList<String>();

    /** adaptador para el listview **/
    ArrayAdapter<String> adapterList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mov_locaciones_entrada, container, false);
        init();
        obtenerSalidas();
        return view;
    }

    public void init(){
        list_locaciones = view.findViewById(R.id.list_locaciones);
        linInfoDocument = view.findViewById(R.id.linInfoDocument);
    }

    /** obtener locaciones de salida **/
    private void obtenerSalidas(){
        String token = Local.getData("login", getContext(), "token");
        String usuario = Local.getData("login", getContext(), "idUsuario");
        String cliente = Local.getData("login", getContext(), "idCLiente");
        String proyecto = Local.getData("login", getContext(), "idProyecto");
        final String locacion = Local.getData("login", getContext(), "idLocacion");
        reqLocaciones = ApiService.getApiService(getContext()).getlocacionesinout(usuario,cliente,proyecto,locacion,"18", "1", token);
        reqLocaciones.enqueue(new Callback<LocacionInOut>() {
            @Override
            public void onResponse(Call<LocacionInOut> call, Response<LocacionInOut> response) {
                if(response.isSuccessful()){
                    resLocaciones = response.body();
                    locaciones.clear();
                    int validar = 0;
                    for(ResLocacion data : resLocaciones.getDatos().getResultado()){
                        validar = data.getIdOpcion();
                        locaciones.add(data.getOpcion());
                    }
                    if(validar > 0){
                        linInfoDocument.setVisibility(View.GONE);
                        mostrarLocaciones();
                    }else{
                        linInfoDocument.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<LocacionInOut> call, Throwable t) {

            }
        });
    }


    /** mostrar lista de ubicaciones **/
    public void mostrarLocaciones(){
        adapterList = new ArrayAdapter<>(getContext(), R.layout.item_documentos, locaciones);
        list_locaciones.setAdapter(adapterList);

        list_locaciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for(ResLocacion data : resLocaciones.getDatos().getResultado()){
                    if(locaciones.get(position).equals(data.getOpcion())){
                        Local.setData("movimiento", getContext(), "idLocacion", String.valueOf(data.getIdOpcion()));
                        Local.setData("locEntrada", getContext(), "descripcion", data.getOpcion());
                        Local.setData("movimiento", getContext(), "tipo", "1");
                    }
                }
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new LecturaMovimientoFragment())
                        .commit();
            }
        });
    }
}