package com.glgroup.centurycloudmovil.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.glgroup.centurycloudmovil.Fragments.Desenlace.DesenlaceFragment;
import com.glgroup.centurycloudmovil.Fragments.Enlace.EnlaceFragment;
import com.glgroup.centurycloudmovil.Fragments.Entrada.DocumentosEntradaFragment;
import com.glgroup.centurycloudmovil.Fragments.Inventario.ListaInventariosFragment;
import com.glgroup.centurycloudmovil.Fragments.Movimiento.MovimientoFragment;
import com.glgroup.centurycloudmovil.Fragments.Salida.DocumentosSalidaFragment;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Mensajes;
import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.AppDatabase;
import com.glgroup.centurycloudmovil.ui.MainActivity;


public class DefaultFragment extends Fragment {

    private CardView cardView;

    private GridLayout mainGrid;

    View view;
    MainActivity mContext;

    /** base de datos **/
    private AppDatabase bd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        /** inicializar la base de datos **/
        bd = Room.databaseBuilder(getContext(),AppDatabase.class, Constantes.BD_NAME)
                .allowMainThreadQueries()
                .build();
        view = inflater.inflate(R.layout.fragment_default, container, false);
        mainGrid = view.findViewById(R.id.mainGrid);
        mContext = (MainActivity) getActivity();
        setSingleEvent(mainGrid);
        return view;
    }

    public void setSingleEvent(GridLayout mainGrid){
        for(int i = 0; i < mainGrid.getChildCount(); i++){
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;

            cardView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int permiso = 0;
                    switch (finalI) {
                        case 0:
                            /** se validan los permisos del boton **/
                            if(bd.permisoDao().sp_Sel_Permiso(1) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(1).getPermiso(); }
                            if(permiso == 1) {
                                Alertas.alertEnlaceMenu(getContext(), getLayoutInflater(), mContext);
                               // changeFragment(new EnlaceFragment(), getString(R.string.enlazar));
                            }else{
                                Mensajes.errorPermiso(getContext());
                            }
                            break;
                        case 1:
                            /** se validan los permisos del boton **/
                            if(bd.permisoDao().sp_Sel_Permiso(2) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(2).getPermiso(); }
                            if(permiso == 1) {
                                Alertas.alertDesenlaceMenu(getContext(),getLayoutInflater(),mContext);
                                //changeFragment(new DesenlaceFragment(), getString(R.string.desenlace));
                            }else{
                                Mensajes.errorPermiso(getContext());
                            }
                            break;
                        case 2:
                            /** se validan los permisos del boton **/
                            if(bd.permisoDao().sp_Sel_Permiso(3) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(3).getPermiso(); }
                            if(permiso == 1) {
                                changeFragment(new DocumentosSalidaFragment(), getString(R.string.salida_despacho));
                            }else{
                                Mensajes.errorPermiso(getContext());
                            }
                            break;
                        case 3:
                            /** se validan los permisos del boton **/
                            if(bd.permisoDao().sp_Sel_Permiso(4) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(4).getPermiso(); }
                            if(permiso == 1) {
                                changeFragment(new DocumentosEntradaFragment(), getString(R.string.entrada_ingreso));
                            }else{
                                Mensajes.errorPermiso(getContext());
                            }
                            break;
                        case 4:
                            /** se validan los permisos del boton **/
                            if(bd.permisoDao().sp_Sel_Permiso(5) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(5).getPermiso(); }
                            if(permiso == 1) {
                                changeFragment(new MovimientoFragment(), getString(R.string.movimiento));
                            }else{
                                Mensajes.errorPermiso(getContext());
                            }
                            break;
                        case 5:
                            /** se validan los permisos del boton **/
                            if(bd.permisoDao().sp_Sel_Permiso(6) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(6).getPermiso(); }else{permiso = 0;}
                            if(permiso == 1) {
                                changeFragment(new ListaInventariosFragment(), getString(R.string.inventario));
                            }else{
                                Mensajes.errorPermiso(getContext());
                            }
                            break;
                        case 6:
                            /** se validan los permisos del boton **/
                            if(bd.permisoDao().sp_Sel_Permiso(7) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(7).getPermiso(); }
                            if(permiso == 1) {
                                Alertas.alertBusqueda(getContext(), getLayoutInflater(), mContext);
                            }else{
                                Mensajes.errorPermiso(getContext());
                            }
                            break;
                        default:
                           // Toast.makeText(getContext(), String.valueOf(finalI),Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            });
        }
    }

    /** metodo para cambiar un fragmento por otro en la vista **/
    private void changeFragment(Fragment fragment, String titulo){
        mContext.getSupportActionBar().setTitle(titulo);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame,fragment)
                .addToBackStack(null)
                .commit();

    }
}
