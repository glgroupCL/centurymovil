package com.glgroup.centurycloudmovil.Fragments.Entrada;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;

import com.glgroup.centurycloudmovil.Models.Application.Documentos.Documento;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ResDocumento;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentosEntradaFragment extends Fragment implements MainActivity.IOnBackPressed{

    /** contexto donde se muestra el fragment y su vista **/
    private MainActivity mContext;
    private View view;

    /** elementos de la vista **/
    private EditText buscar;
    private ListView list_documentos;
    private LinearLayout linInfoDocument;

    /** peticiones retrofit **/
    private Call<Documento> reqDocuemtos;

    /** respuestas retrofit **/
    private Documento resDocumentos;
    int validar = 0;

    /** documentos obtenidos **/
    private ArrayList<String> documentos = new ArrayList<String>();

    /** adaptador para el listview **/
    ArrayAdapter<String> adapterList;

    /** ventana de carga **/
    private AlertCarga alert;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_documentos_entrada, container, false);
        init();
        obtenerDocumentos();
        return view;
    }

    public void init(){
        alert = new AlertCarga(getString(R.string.obteniendo_documentos), getActivity(), getLayoutInflater());
        alert.alertCarga();
        alert.alertShow();

        mContext = (MainActivity) getActivity();

        list_documentos = view.findViewById(R.id.list_documentos);

        linInfoDocument = view.findViewById(R.id.linInfoDocument);

        buscar = view.findViewById(R.id.buscar);
        buscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("onTextChanged", String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("afterTextChanged", String.valueOf(s));
                if(validar > 0) {
                    filtrarDatos(String.valueOf(s));
//                    adapterList.getFilter().filter(s);
//                    adapterList.notifyDataSetChanged();
                }
            }
        });
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }

    /** se filtran los datos ingresados por el usuario **/
    public void filtrarDatos(String valor){
        documentos.clear();
        if(valor.equals("")){
            for(ResDocumento data : resDocumentos.getDatos().getResultado()){
                documentos.add(data.getDescripcion());
            }
        }else {
            for (ResDocumento data : resDocumentos.getDatos().getResultado()) {
                if (data.getDescripcion().toLowerCase().contains(valor.toLowerCase())) {
                    documentos.add(data.getDescripcion());
                }
            }
        }
        adapterList.notifyDataSetChanged();
    }


    public void obtenerDocumentos(){
        String token = Local.getData("login", getContext(), "token");
        String usuario = Local.getData("login", getContext(), "idUsuario");
        String cliente = Local.getData("login", getContext(), "idCLiente");
        String proyecto = Local.getData("login", getContext(), "idProyecto");
        String locacion = Local.getData("login", getContext(), "idLocacion");
        String area = Local.getData("login", getContext(), "idArea");

        reqDocuemtos = ApiService.getApiService(getContext()).getdocumentos(usuario,cliente,proyecto,locacion,area, Constantes.DOC_ENTRADA,token);
        reqDocuemtos.enqueue(new Callback<Documento>() {
            @Override
            public void onResponse(Call<Documento> call, Response<Documento> response) {
                if(response.isSuccessful()){
                    resDocumentos = response.body();

                    documentos.clear();
                    for(ResDocumento data : resDocumentos.getDatos().getResultado()){
                        validar = data.getIdDocumento();
                        if(validar > 0) {
                            documentos.add(data.getDescripcion());
                        }

                    }
                    alert.alerDismiss();
                    if(validar > 0){
                        linInfoDocument.setVisibility(View.GONE);
                        mostrarDocumentos();
                    }else{
                        linInfoDocument.setVisibility(View.VISIBLE);
                    }
                }else {
                    alert.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<Documento> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }
    public void mostrarDocumentos(){
        adapterList = new ArrayAdapter<>(getContext(), R.layout.item_documentos, documentos);
        list_documentos.setAdapter(adapterList);

        list_documentos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toasty.error(getContext(), (String)list_documentos.getItemAtPosition(position), Toasty.LENGTH_LONG).show();
                for(ResDocumento data: resDocumentos.getDatos().getResultado()){
                    if(data.getDescripcion().equals(list_documentos.getItemAtPosition(position))){
                        if(data.getObservacionesDcto() == null || data.getObservacionesDcto().equals("")) {
                            Local.setData("docEntrada", getContext(), "observacion", "Sin observaciones");
                        }else{
                            Local.setData("docEntrada", getContext(), "observacion", data.getObservacionesDcto());
                        }
                        Local.setData("docEntrada", getContext(), "descripcion", data.getDescripcion());
                        Local.setData("docEntrada", getContext(), "numero_documento", data.getNumeroDocumento());
                        Local.setData("docEntrada", getContext(), "id_proveedor", String.valueOf(data.getIdProveedor()));
                        Local.setData("docEntrada", getContext(), "id_documento", String.valueOf(data.getIdDocumento()));
                        Local.setData("docEntrada", getContext(), "tipo_documento", String.valueOf(data.getTipoDocumento()));
                    }
                }
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new LecturaEntradaFragment())
                        .addToBackStack(null)
                        .commit();

            }
        });
    }
}