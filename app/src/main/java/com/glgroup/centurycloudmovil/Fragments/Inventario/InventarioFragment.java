package com.glgroup.centurycloudmovil.Fragments.Inventario;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ResListaRfid;
import com.glgroup.centurycloudmovil.Models.ListaInventario;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Filtro;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class InventarioFragment extends Fragment implements MainActivity.IOnBackPressed, View.OnClickListener {

    public InventarioFragment() {
        // Required empty public constructor
    }


    /** vista del fragment **/
    private View view;

    /** elementos de la vista **/
    private CardView limpiar, leer, procesar;
    private TextView contador;
    int cont = 0;
    private ImageView imgLectura;
    private TextView textLectura, txtNumeroInv;

    /** variable de lectura **/
    boolean isRuning = false;
    private boolean loopFlag = false;
    private long mStrTime;

    /** lista de etiquetas leidas **/
    private Map<String, String> etiquetasLeidas = new HashMap<String, String>();


    /** actividad donde se encuentra el lector **/
    private MainActivity mContext;

    /** token **/
    private String token = "";


    private Timer mTimer = new Timer();
    private TimerTask mInventoryPerMinuteTask;


    /** clase para la conexión de dispositivos **/
    private ConnectStatus mConnectStatus = new ConnectStatus();


    /** se controla si el dispositivo se encuentra conectado o no **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Toast.makeText(getContext(),"Conectado en fragment", Toast.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
                //Toast.makeText(getContext(),"Desconectado en fragment", Toast.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }


    @SuppressLint("HandlerLeak")
    private
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:

                    break;
                case Constantes.FLAG_START:

                    break;
                case Constantes.FLAG_UPDATE_TIME:
//                    float useTime = (System.currentTimeMillis() - mStrTime) / 1000.0F;
//                    tv_time.setText(NumberTool.getPointDouble(loopFlag ? 1 : 3, useTime) + "s");
                    break;
                case Constantes.FLAG_UHFINFO:
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    agregarEtiquetas(info.getEPC());
                    break;
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_inventario, container, false);
        init();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        init();

        if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            mContext.uhf.setPower(30);
        }
        mContext.addConnectStatusNotice(mConnectStatus);

        /** acción recibida desde el lector **/
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {
                if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if(loopFlag) {
                        iniciar();
                        stopInventory();
                    } else {
                        detener();
                        startThread();
                    }
                }
            }
        });
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        iniciar();
        stopInventory();

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new ListaInventariosFragment())
                .commit();
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }

    /** inicialización de los componentes **/
    public void init(){

        limpiar = view.findViewById(R.id.btnClear);
        limpiar.setOnClickListener(this);

        leer = view.findViewById(R.id.btnLectura);
        leer.setOnClickListener(this);

        procesar = view.findViewById(R.id.btnSend);
        procesar.setOnClickListener(this);

        contador = view.findViewById(R.id.lbl_contador);

        imgLectura = view.findViewById(R.id.imgLectura);
        textLectura = view.findViewById(R.id.textLectura);

        txtNumeroInv = view.findViewById(R.id.txtInventario);
        String idInventario = Local.getData("inventario", getContext(),"idInventario");
        if(idInventario.equals("0")){
            txtNumeroInv.setText(getString(R.string.inventario_nuevo));
        }else{
            txtNumeroInv.setText(getString(R.string.inventario)+": "+idInventario);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnClear:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                etiquetasLeidas.clear();
                contador.setText("0");
                break;

            case R.id.btnLectura:
                /** se comprueba su el dispositivo está conectado **/
                if(mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if (loopFlag) {
                        iniciar();
                        stopInventory();
                    } else {
                        detener();
                        startThread();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.btnSend:
                if(!contador.getText().toString().equals("0")){
                    iniciar();
                    stopInventory();
                    procesarLectura();
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                }

                break;
        }
    }

    /** cambiar color de botones **/
    public void iniciar(){
        leer.setCardBackgroundColor(Color.parseColor("#003D70"));
        imgLectura.setImageResource(R.drawable.ic_play);
        textLectura.setText(getString(R.string.iniciar));
    }

    public void detener(){
        leer.setCardBackgroundColor(Color.parseColor("#FFB71C1C"));
        imgLectura.setImageResource(R.drawable.ic_pause);
        textLectura.setText(getString(R.string.detener));
    }

    /** agregar las etiquetas a la lista **/
    public void agregarEtiquetas(String epc){
        Log.d("etiquetas", epc);
        if(!etiquetasLeidas.containsKey(epc)) {
            etiquetasLeidas.put(epc,epc);
            if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(1);}
            contador.setText(String.valueOf(etiquetasLeidas.size()));
        }
    }

    /**procesar etiquetas **/
    public void procesarLectura(){
        Bundle bundle = new Bundle();
        String tags = Filtro.Filtro(etiquetasLeidas);
        bundle.putString("etiquetas", tags);
        bundle.putString("contador", contador.getText().toString());
        Fragment fragmento = new ResultadoInventarioFragment();
        fragmento.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragmento);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }





    /** SDK **/
    public void startThread() {
        if(mContext.uhf.getPower() != 30){
            mContext.uhf.setPower(30);
        }
        if (isRuning) {
            return;
        }
        isRuning = true;
        // cbFilter.setChecked(false);
        new TagThread().start();
    }

    class TagThread extends Thread {
        public void run() {
            Message msg = handler.obtainMessage(Constantes.FLAG_START);
            if (mContext.uhf.startInventoryTag()) {
                loopFlag = true;
                mContext.isScanning = true;
                mStrTime = System.currentTimeMillis();
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            handler.sendMessage(msg);
            isRuning = false;//执行完成设置成false
            while (loopFlag) {
                getUHFInfo();
                handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
            }
            stopInventory();
        }
    }

    /** obtiene información de la etiqueta y la envía al handler **/
    private void getUHFInfo() {
        List<UHFTAGInfo> list = mContext.uhf.readTagFromBufferList_EpcTidUser();
        if (list != null && !list.isEmpty()) {
            for (int k = 0; k < list.size(); k++) {
                Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO, list.get(k));
                handler.sendMessage(msg);
                if(!loopFlag) {
                    break;
                }
            }
        }
    }

    /** se detiene el proceso de inventario **/
    private void stopInventory() {
        loopFlag = false;
        boolean result = mContext.uhf.stopInventory();
        if(mContext.isScanning) {
            ConnectionStatus connectionStatus = mContext.uhf.getConnectStatus();
            Message msg = handler.obtainMessage(Constantes.FLAG_STOP);
            if (result || connectionStatus == ConnectionStatus.DISCONNECTED) {
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
                //getUHFInfoEx();
            }
            mContext.isScanning = false;
            handler.sendMessage(msg);
        }
    }


}
