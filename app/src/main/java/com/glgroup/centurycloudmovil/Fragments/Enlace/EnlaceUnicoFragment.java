package com.glgroup.centurycloudmovil.Fragments.Enlace;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.Peticiones.NombreItem;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.Application.Enlace.Enlace;
import com.glgroup.centurycloudmovil.Models.Application.Enlace.ResEnlace;
import com.glgroup.centurycloudmovil.Models.Application.EpcGrabar.EpcGrabar;
import com.glgroup.centurycloudmovil.Models.Application.EpcGrabar.ResEpcGrabar;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.IdItem;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.ResIdItem;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.AppDatabase;
import com.glgroup.centurycloudmovil.Utils.Mensajes;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.ActivityEscanear;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.textfield.TextInputLayout;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnlaceUnicoFragment extends Fragment implements View.OnClickListener, MainActivity.IOnBackPressed{

    public EnlaceUnicoFragment() {
    }

    private String TAG = "UHFReadTagFragment";

    /** peticiones retrofit **/
    private Call<IdItem> reqIdItem;
    private  Call<Enlace> reqEnlace;
    private  Call<EpcGrabar> reqEpcGrabar;

    /** datos obtenidos retrofit **/
    private IdItem resIdItem;
    private Enlace resEnlace;
    private EpcGrabar resEpcGrabar;

    String nombre = "";

    /**
     * elementos para la lectura
     **/
    private boolean loopFlag = false;

    /**
     * conexto del fragment
     **/
    private MainActivity mContext;
    private boolean isExit = false;

    /**
     * codigo para acceso a la camara
     **/
    private static final int CODIGO_PERMISOS_CAMARA = 1, CODIGO_INTENT = 2;

    /** ventana de carga **/
    private AlertCarga alert, alert2;

    /**
     * elementos de la vista
     **/
    private View view;
    private ImageView btnRfid, btn_scansku;
    private EditText ed_contendor;
    private TextView txt_barcode, nombre_item, ed_rfid;
    public Button btn_write, btn_fijar;
    private CardView btn_clean, btn_enlace;
    private CheckBox chkSku;

    boolean isRuning = false;

    /** base de datos **/
    private AppDatabase bd;

    /** variables para consultas **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;
    private String area;


    /**
     * clase para la conexión de dispositivos
     **/
    private ConnectStatus mConnectStatus = new ConnectStatus();

    /**
     * hilo para recibir datos de lectura
     **/
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        /** error al detener la lectura **/
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                        Toast.makeText(mContext, "uhf_msg_inventory_stop_fail", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constantes.FLAG_START:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}

                    }

                    break;

                case Constantes.FLAG_UHFINFO:
                    /** se reciben los datos de las etiquetas **/
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    if (!TextUtils.isEmpty(info.getEPC())) {
                        if (ed_rfid.getText().toString().equals("")) {
                            ed_rfid.setText(info.getEPC());
                        }
                    }
                    break;

                case Constantes.FLAG_BARCODE:
                    String data = msg.obj.toString().replace("�","");
                    String result = obtenerNombre(getContext(), data.replace("�",""));
                    if (!result.equals("")) {

                        txt_barcode.setText(data);
                        nombre_item.setText(result);
                    } else {

                        Toasty.warning(getContext(), getString(R.string.codigo_barra_no_registrado), Toasty.LENGTH_LONG).show();
                    }

                    break;
            }
        }
    };

    /**
     * se controla si el dispositivo se encuentra conectado o no
     **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                  //  Toasty.success(getContext(), "Dispositivo conectado", Toasty.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
              //  Toasty.warning(getContext(), "Dispositivo desconectado", Toasty.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(TAG, "UHFReadTagFragment.onCreateView");
        Local.setData("enlace", getContext(), "idItem", "");
        view = inflater.inflate(R.layout.fragment_enlace_unico, container, false);
        init();


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isExit = true;
        mContext.removeConnectStatusNotice(mConnectStatus);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "UHFReadTagFragment.onPause");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "UHFReadTagFragment.onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            mContext.uhf.setPower(5);
        }
        mContext.addConnectStatusNotice(mConnectStatus);
        //  init();
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {
                Log.d(TAG, "  keycode =" + keycode + "   ,isExit=" + isExit);
                if (!isExit && mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    String nameDevice = Local.getData("dispositivo", getContext(), "nombre");
                    if (nameDevice.contains("R6")) {
                        if (chkSku.isChecked()) {
                            if (txt_barcode.getText().toString().equals("")) {
                                scan();
                            } else {
                                inventory();
                            }
                        } else {
                            inventory();
                        }
                    } else {
                        inventory();
                    }
                }
            }
        });
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }


    /**
     * inicio de componentes
     **/
    public void init() {
        /** inicializar la base de datos **/
        bd = Room.databaseBuilder(getContext(), AppDatabase.class, Constantes.BD_NAME)
                .allowMainThreadQueries()
                .build();

        alert = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alert.alertCarga();

        alert2 = new AlertCarga(getString(R.string.grabando_etiqueta), getActivity(), getLayoutInflater());
        alert2.alertCarga();

        isExit = false;
        btnRfid = view.findViewById(R.id.btnRfid);
        btnRfid.setOnClickListener(this);


        btn_scansku = view.findViewById(R.id.btn_scansku);
        btn_scansku.setOnClickListener(this);

        ed_rfid = view.findViewById(R.id.ed_rfid);
        txt_barcode = view.findViewById(R.id.txt_barcode);
        txt_barcode.setOnClickListener(this);

        nombre_item = view.findViewById(R.id.tv_nom_item);
        ed_contendor = view.findViewById(R.id.ed_contendor);

        btn_clean = view.findViewById(R.id.card_clean);
        btn_clean.setOnClickListener(this);

        chkSku = view.findViewById(R.id.chkSku);

        btn_enlace = view.findViewById(R.id.card_link);
        btn_enlace.setOnClickListener(this);

        btn_write = view.findViewById(R.id.btn_write);
        btn_write.setOnClickListener(this);

        btn_fijar = view.findViewById(R.id.btn_fijar);
        btn_fijar.setOnClickListener(this);

        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
        area = Local.getData("login", getContext(), "idArea");
    }


    @Override
    public void onClick(View view) {
        int permiso = 0;
        switch (view.getId()) {
            case R.id.btnRfid:
                if(!mContext.isScanning) {
                    /** se comprueba su el dispositivo está conectado **/
                    if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                        if (ed_rfid.getText().toString().equals("")) {
                            inventory();
                        }
                    } else {
                        Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.proceso_de_lectura), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.card_clean:
                limpiar();
                break;

            case R.id.btn_scansku:

                if (!Local.getData("permisos", getContext(), "camara").equals("true")) {
                    // Toast.makeText(getActivity(), "Habilitando permisos, vuelve a intentarlo", Toast.LENGTH_SHORT).show();
                    //  permisoSolicitadoDesdeBoton = true;
                    Local.setData("permisos", getContext(), "boton_camara", "true");
                    verificarYPedirPermisosDeCamara();
                    return;
                }
                verificarYPedirPermisosDeCamara();
                break;

            case R.id.card_link:
                /** se validan los permisos del boton **/
                if(bd.permisoDao().sp_Sel_Permiso(13) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(13).getPermiso(); }
                if(permiso == 1) {
                    /** PREPARENCE PARA LOS PROBLEMAS Y MÁS VALE QUE TEMAN **/

                    /** se valida si existe un item fijado para evitar solicitar leer un codigo de barras **/
                    if (Local.getData("enlace", getContext(), "fijarItem").equals("1") && !nombre_item.getText().toString().equals("")) {
                        if (!ed_rfid.getText().toString().equals("")) {
                            enlace();
                        } else {
                            Toasty.warning(getContext(), getString(R.string.necesario_rfid_antes_enlace), Toasty.LENGTH_LONG).show();
                        }
                    } else {
                        /** el usuario puede no usar codigo de barras, pero debe fijar un item en ese caso, nose cual es la finalidad de ésto **/
                        if (chkSku.isChecked()) {

                            /** si está marcado, se comprueba si el item leído es válido obteniendo el campo de descripción del producto **/
                            if (!txt_barcode.getText().toString().equals("")) {

                                /** si el codigo es correcto, se comprueba si existe un rfid leído para enlazar, caso contrario se solicita **/
                                if (!ed_rfid.getText().toString().equals("")) {
                                    enlace();
                                } else {
                                    Toasty.warning(getContext(), getString(R.string.necesario_rfid_antes_enlace), Toasty.LENGTH_LONG).show();
                                }
                            } else {
                                /** si está vació significa que no se ha leido un codigo de barras o éste era inválido **/
                                Toasty.warning(getContext(), getString(R.string.es_necesario_leer_un_sku_enlace), Toasty.LENGTH_LONG).show();
                            }
                        } else {
                            /** si no está marcado el chkSku, se valida si existe un item fijado en base al nombre de éste **/
                            if (!nombre_item.getText().toString().equals("")) {

                                /** si ha yn item válido, se comprueba la lectura de un RFID para en enlace, caso contrario se solicita **/
                                if (!ed_rfid.getText().toString().equals("")) {
                                    enlace();
                                } else {
                                    Toasty.warning(getContext(), getString(R.string.necesario_rfid_antes_enlace), Toasty.LENGTH_LONG).show();
                                }
                            } else {
                                Toasty.warning(getContext(), getString(R.string.debes_fijar_un_item), Toasty.LENGTH_LONG).show();
                            }
                        }
                    }
                }else{
                    Mensajes.errorPermiso(getContext());
                }
                break;

            case R.id.btn_write:
                /** se validan los permisos del boton **/
                if(bd.permisoDao().sp_Sel_Permiso(12) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(12).getPermiso(); }
                if(permiso == 1) {
                    /** se comprueba su el dispositivo está conectado **/
                    if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                        if (!mContext.isScanning) {
                            obtenerEpcGrabar(ed_rfid.getText().toString());
                        } else {
                            Toasty.warning(getContext(), getString(R.string.proceso_de_lectura), Toasty.LENGTH_LONG).show();
                        }
                    } else {
                        Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                    }

                }else{
                    Mensajes.errorPermiso(getContext());
                }
                break;

            case R.id.btn_fijar:
                /** se validan los permisos del boton **/
                if(bd.permisoDao().sp_Sel_Permiso(11) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(11).getPermiso(); }
                if(permiso == 1) {
                    Alertas.fijarItem(getContext(), getLayoutInflater(), nombre_item, 1);
                }else{
                    Mensajes.errorPermiso(getContext());
                }
                break;

            case R.id.txt_barcode:
                alertSolicitaSku(txt_barcode, nombre_item);
                break;
        }
    }

    /**
     * Enlace
     **/
    public void enlace() {
        alert.alertShow();
        String idItem = Local.getData("enlace", getContext(), "idItem");

        reqEnlace = ApiService.getApiService(getContext()).getenlace(usuario, cliente, proyecto, locacion, area, txt_barcode.getText().toString(), idItem, ed_rfid.getText().toString(), Constantes.ENLACE_UNICO, Constantes.ENLACE, Constantes.IMEI, ed_contendor.getText().toString(), token);
        reqEnlace.enqueue(new Callback<Enlace>() {
            @Override
            public void onResponse(Call<Enlace> call, Response<Enlace> response) {
                if (response.isSuccessful()) {
                    resEnlace = response.body();
                    for (ResEnlace data : resEnlace.getDatos().getResultado()) {
                        if (data.getIdMsj() > 0) {
                            Toasty.success(getContext(), data.getMensaje(), Toasty.LENGTH_LONG).show();

                            /** Si existe un item fijado, se limpia solo el campo rfid, caso contrario se limpia toda la vista **/
                            if (Local.getData("enlace", getContext(), "fijarItem").equals("1")) {
                                ed_rfid.setText("");
                            } else {
                                limpiar();
                            }
                        } else {
                            Toasty.error(getContext(), data.getMensaje(), Toasty.LENGTH_LONG).show();
                            ed_rfid.setText("");
                        }
                    }
                    alert.alerDismiss();
                }else{
                    alert.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<Enlace> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }

    /**
     * limpia campos
     **/
    public void limpiar() {
        ed_rfid.setText("");
        txt_barcode.setText("");
        nombre_item.setText("");
        Local.setData("enlace", getContext(), "idItem", "");
        Local.setData("enlace", getContext(), "fijarItem", "0");
    }


    /**sdk **/

    /**
     * lectura de etiquetas
     **/
    private void inventory() {
        if (mContext.uhf.getPower() != 5) {
            mContext.uhf.setPower(5);
        }
        UHFTAGInfo info = mContext.uhf.inventorySingleTag();
        if (info != null) {
            if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(1);}
            Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO);
            msg.obj = info;
            handler.sendMessage(msg);
        } else {
            Toasty.warning(getContext(), getString(R.string.no_se_detecto_etiqueta), Toasty.LENGTH_SHORT).show();
        }
        handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
    }

    /**
     * se accede a la vista para la lectura de codigo
     **/
    private void escanear() {
        Intent i = new Intent(getActivity(), ActivityEscanear.class);
        startActivityForResult(i, CODIGO_INTENT);
    }


    /**
     * se obtiene la lectura de codigo de barras
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CODIGO_INTENT) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    Local.setData("enlace", getContext(), "idItem", "");
                    String codigo = data.getStringExtra("codigo");
                    String result = obtenerNombre(getContext(), codigo);
                    if (!result.equals("")) {
                        txt_barcode.setText(codigo);
                        nombre_item.setText(result);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.codigo_barra_no_registrado), Toasty.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    /** ingresar sku manualmente **/
    public void alertSolicitaSku(final TextView txt_barcode,final TextView nombre_item ){
        final android.app.AlertDialog alertSku;
        final android.app.AlertDialog.Builder solicitaSku = new android.app.AlertDialog.Builder(getContext());


        View dialog = getLayoutInflater().inflate(R.layout.alert_ingresa_sku, null);

        final TextInputLayout codigo = dialog.findViewById(R.id.ed_sku);
        final Button ok = dialog.findViewById(R.id.btn_validar);
        final Button cancel = dialog.findViewById(R.id.btn_regresar);

        solicitaSku.setView(dialog);

        alertSku = solicitaSku.create();
        alertSku.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sku = codigo.getEditText().getText().toString().trim();
                if(sku.equals("")){
                    codigo.setErrorEnabled(true);
                    codigo.setError(getContext().getString(R.string.requerido));
                }else{
                    codigo.setErrorEnabled(false);
                    alertSku.dismiss();
                    String result = obtenerNombre(getContext(), sku);
                    if (!result.equals("")) {
                        txt_barcode.setText(sku);
                        nombre_item.setText(result);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.codigo_barra_no_registrado), Toasty.LENGTH_LONG).show();
                    }
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertSku.dismiss();
            }
        });
        alertSku.show();
    }


    public String obtenerNombre(final Context context, String sku) {

        reqIdItem = ApiService.getApiService(context).getiditem(usuario, cliente, proyecto, locacion, area, sku, token);
        reqIdItem.enqueue(new Callback<IdItem>() {
            @Override
            public void onResponse(Call<IdItem> call, Response<IdItem> response) {
                if (response.isSuccessful()) {
                    resIdItem = response.body();
                    for (ResIdItem data : resIdItem.getDatos().getResultado()) {
                        Log.d("item", data.getItem());
                        Log.d("item", String.valueOf(data.getIdItem()));
                        if (data.getIdItem() > 0) {
                            Local.setData("enlace", context, "idItem", String.valueOf(data.getIdItem()));
                            nombre = data.getItem();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<IdItem> call, Throwable t) {

            }
        });


        return nombre;
    }

    /**
     * permisos de cámara
     **/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CODIGO_PERMISOS_CAMARA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Escanear directamten solo si fue pedido desde el botón
                    if (Local.getData("permisos", getContext(), "boton_camara").equals("true")) {
                        escanear();
                    }
                    Local.setData("permisos", getContext(), "camara", "true");
                    //  permisoCamaraConcedido = true;
                } else {
                    Local.setData("permisos", getContext(), "camara", "false");
                    Toast.makeText(getActivity(), getString(R.string.error_permisos), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void verificarYPedirPermisosDeCamara() {
        int estadoDePermiso = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (estadoDePermiso == PackageManager.PERMISSION_GRANTED) {
            // En caso de que haya dado permisos ponemos la bandera en true
            // y llamar al método
            // permisoCamaraConcedido = true;
            Local.setData("permisos", getContext(), "camara", "true");
            escanear();
        } else {
            // Si no, pedimos permisos. Ahora mira onRequestPermissionsResult
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    CODIGO_PERMISOS_CAMARA);
        }
    }

    /**
     * lector de barras
     **/
    private synchronized void scan() {

        if (!isRuning) {
            isRuning = true;
            new ScanThread().start();
        }
    }

    class ScanThread extends Thread {
        public void run() {
            String data = null;
            byte[] temp = mContext.uhf.scanBarcodeToBytes();
            if (temp != null) {
                try {
                    data = new String(temp, "utf8");
                } catch (Exception e) {

                }
                if (data != null && !data.isEmpty()) {
                    if (data != null && !data.isEmpty()) {
                        Message msg = handler.obtainMessage(Constantes.FLAG_BARCODE, data);
                        handler.sendMessage(msg);

                    }

                }
            }
            isRuning = false;
        }
    }

    /**
     * grabar etiquetas
     **/
    public void grabarSkuRfid() {

        boolean r = false;
        //todo finalizar el proceso de grabado
        String strData = Local.getData("enlace", getContext(), "epcGrabar");
        //strData = "1234567890987658";
        r = mContext.uhf.writeData(Constantes.ACCESS_PASSWORD, mContext.uhf.Bank_EPC, 2, 4, strData);
        if (r) {
            Toast.makeText(getContext(), getString(R.string.grabado_correcto), Toast.LENGTH_SHORT).show();
            Local.setData("enlace", getContext(), "epcGrabar", "");
            ed_rfid.setText(strData);
            alert2.alerDismiss();
        } else {
            alert2.alerDismiss();
            Toast.makeText(getContext(), getString(R.string.error_grabar), Toast.LENGTH_SHORT).show();
        }
    }

    public void obtenerEpcGrabar(String epcLeido){
        alert2.alertShow();
        String idItem = Local.getData("enlace", getContext(), "idItem");
        reqEpcGrabar = ApiService.getApiService(getContext()).getepcgrabar(usuario,cliente, proyecto, locacion, area, idItem, epcLeido, token);
        reqEpcGrabar.enqueue(new Callback<EpcGrabar>() {
            @Override
            public void onResponse(Call<EpcGrabar> call, Response<EpcGrabar> response) {
                if(response.isSuccessful()){
                    resEpcGrabar = response.body();
                    for(ResEpcGrabar data : resEpcGrabar.getDatos().getResultado()){
                        Local.setData("enlace", getContext(), "epcGrabar", data.getEpc());
                    }

                }
                readDataTag();
            }

            @Override
            public void onFailure(Call<EpcGrabar> call, Throwable t) {
                alert2.alerDismiss();
            }
        });
    }


    /**
     * bloqueo de etiquetas
     **/
    public void readDataTag() {

        String entity = "";

        /** Se intenta leer con la clave propia **/
        entity = mContext.uhf.readData(Constantes.ACCESS_PASSWORD,
                mContext.uhf.Bank_RESERVED,
                Integer.parseInt("0"),
                Integer.parseInt("4"));

        if (entity != null) {

            /** se obtienen los datos con la clave propia **/
            System.out.println(entity);
            bloquearMemoria();
        } else {

            /** Se intenta leer con la clave estandar **/
            entity = mContext.uhf.readData("00000000",
                    mContext.uhf.Bank_RESERVED,
                    Integer.parseInt("0"),
                    Integer.parseInt("4"));
            if (entity != null) {
                /** se obtienen los datos con la clave estandar **/
                System.out.println(entity);
                grabarMemoria();
            } else {
                alert2.alerDismiss();
                /** no es posible obtener datos **/
                Toasty.error(getContext(), getString(R.string.etiqueta_bloqueda), Toasty.LENGTH_LONG).show();
                System.out.println("bloqueada sin acceso");
            }
        }
    }


    /**
     * se cambia el valor para APwd y KPwd
     **/
    public void grabarMemoria() {
        boolean r;
        String clavesBloqueo = Constantes.KILL_PASSWORD + Constantes.ACCESS_PASSWORD;
        r = mContext.uhf.writeData("00000000", mContext.uhf.Bank_RESERVED, Integer.parseInt("0"), Integer.parseInt("4"), clavesBloqueo);
        System.out.println(r);
        if (r) {
            /** se grabaron los datos correctamente **/
            bloquearMemoria();
        } else {
            alert2.alerDismiss();
            Toast.makeText(getContext(), getString(R.string.error_al_grabar_intente), Toast.LENGTH_LONG).show();
            /** NO fue posible grabar datos, se enlaza de todas formas? **/
        }
    }


    /**
     * se bloquea el EPC, APwd y KPwd
     **/
    public void bloquearMemoria() {
        /** el codigo de bloques '0A82A0' es utilizado para epc, apwd y kpwd, el código varía dependiendo de la memoria a grabar **/
        if (mContext.uhf.lockMem(Constantes.ACCESS_PASSWORD, "0A82A0")) {
            /** bloqueado correctamente **/
            grabarSkuRfid();
        } else {
            alert2.alerDismiss();
            Toast.makeText(getContext(), getString(R.string.error_al_grabar_intente), Toast.LENGTH_LONG).show();
            /** Error al bloquear los datos **/
        }
    }
}
