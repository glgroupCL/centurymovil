package com.glgroup.centurycloudmovil.Fragments.Desenlace;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.Peticiones.NombreItem;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Fragments.Enlace.EnlaceUnicoFragment;
import com.glgroup.centurycloudmovil.Models.Application.Desenlace.NombreRfid;
import com.glgroup.centurycloudmovil.Models.Application.Desenlace.ResNombreRfid;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DesenlaceUnicoFragment extends Fragment implements View.OnClickListener, MainActivity.IOnBackPressed{


    public DesenlaceUnicoFragment() {    }

    /** peticiones retrofit **/
    private Call<NombreRfid> reqNombreRfid;
    private Call<NombreRfid> reqDesenlace;

    /** respuestas retrofit **/
    private NombreRfid resNombreRfid;
    private NombreRfid resDesenlace;


    /** elementos de la vista **/
    private CardView card_clean, card_link;
    private ImageView btnRfid;
    private EditText ed_rfid;
    private TextView result;

    /** variables de datos **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;


    /**
     * elementos para la lectura
     **/
    private boolean loopFlag = false;


    /**
     * conexto del fragment
     **/
    private MainActivity mContext;
    private boolean isExit = false;
    boolean isRuning = false;

    /** ventana de carga **/
    private AlertCarga alert;

    /**
     * clase para la conexión de dispositivos
     **/
    private ConnectStatus mConnectStatus = new ConnectStatus();

    /**
     * hilo para recibir datos de lectura
     **/
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        /** error al detener la lectura **/
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                        Toast.makeText(mContext, "uhf_msg_inventory_stop_fail", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constantes.FLAG_START:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                    }

                    break;

                case Constantes.FLAG_UHFINFO:
                    /** se reciben los datos de las etiquetas **/
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    if (!TextUtils.isEmpty(info.getEPC())) {
                        ed_rfid.setText(info.getEPC());
                        obtenerItemRfid(info.getEPC());
                    }

                    break;
            }
        }
    };


    /**
     * se controla si el dispositivo se encuentra conectado o no
     **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                  //  Toasty.success(getContext(), "Dispositivo conectado", Toasty.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
              //  Toasty.warning(getContext(), "Dispositivo desconectado", Toasty.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }

    /** vista **/
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_desenlace_unico, container, false);
        init();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            mContext.uhf.setPower(5);
        }
        mContext.addConnectStatusNotice(mConnectStatus);
        //  init();
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {

                if (!isExit && mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {

                }
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }

    /** inicio de los componentes **/
    public void init(){
        alert = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alert.alertCarga();

        card_clean = view.findViewById(R.id.card_clean);
        card_clean.setOnClickListener(this);

        card_link = view.findViewById(R.id.card_link);
        card_link.setOnClickListener(this);

        btnRfid = view.findViewById(R.id.btnRfid);
        btnRfid.setOnClickListener(this);

        ed_rfid = view.findViewById(R.id.ed_rfid);
        result = view.findViewById(R.id.result);
        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRfid:
                if(!mContext.isScanning) {
                    /** se comprueba su el dispositivo está conectado **/
                    if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                        if (ed_rfid.getText().toString().equals("")) {
                            inventory();
                        }
                    } else {
                        Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.proceso_de_lectura), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.card_clean:
                limpiar();
                break;

            case R.id.card_link:
                desenlace();
                break;
        }

    }

    public void limpiar(){
        result.setText("");
        ed_rfid.setText("");
        card_link.setVisibility(View.INVISIBLE);
    }

    /**
     * lectura de etiquetas
     **/
    private void inventory() {
        if (mContext.uhf.getPower() != 5) {
            mContext.uhf.setPower(5);
        }
        UHFTAGInfo info = mContext.uhf.inventorySingleTag();
        if (info != null) {
            if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(1);}
            Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO);
            msg.obj = info;
            handler.sendMessage(msg);
        } else {
            Toast.makeText(getContext(), getString(R.string.no_se_detecto_etiqueta), Toast.LENGTH_SHORT).show();
        }
        handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
    }

    /** se obtiene el nombre del item asociado al rfid **/
    public void obtenerItemRfid(String rfid){
        alert.alertShow();
        reqNombreRfid = ApiService.getApiService(getContext()).getitemrfid(usuario,cliente,proyecto,locacion,rfid,"1","1", Constantes.IMEI, token);
        reqNombreRfid.enqueue(new Callback<NombreRfid>() {
            @Override
            public void onResponse(Call<NombreRfid> call, Response<NombreRfid> response) {
                if(response.isSuccessful()){
                    resNombreRfid = response.body();
                    int id = 0;
                    result.setText("");
                    for(ResNombreRfid data : resNombreRfid.getDatos().getResultado()){
                        id = data.getIdItem();
                        result.append("ID: "+data.getIdItem()+"\n");
                        result.append("RFID: "+data.getRfid()+"\n");
                        result.append(data.getDescripcion());
                        if(id > 0){

                            card_link.setVisibility(View.VISIBLE);
                        }else{
                            ed_rfid.setText("");
                        }
                    }
                }
                alert.alerDismiss();
            }

            @Override
            public void onFailure(Call<NombreRfid> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }

    /** desenlazar rfid **/
    public void desenlace(){
        alert.alertShow();
        reqDesenlace = ApiService.getApiService(getContext()).getdesenlace(usuario,cliente,proyecto,locacion,ed_rfid.getText().toString(),Constantes.DESENLACE_UNICO,Constantes.DESENLACE,Constantes.IMEI,token);
        reqDesenlace.enqueue(new Callback<NombreRfid>() {
            @Override
            public void onResponse(Call<NombreRfid> call, Response<NombreRfid> response) {
                if(response.isSuccessful()){
                    resDesenlace = response.body();
                    alert.alerDismiss();
                    for(ResNombreRfid data : resDesenlace.getDatos().getResultado()){
                        if(data.getIdItem() > 0){
                            Toasty.success(getContext(), data.getDescripcion(), Toasty.LENGTH_LONG).show();
                        }else{
                            Toasty.error(getContext(), data.getDescripcion(), Toasty.LENGTH_LONG).show();
                        }
                    }
                    limpiar();
                }else{
                    alert.alerDismiss();
                }

            }

            @Override
            public void onFailure(Call<NombreRfid> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }

}