package com.glgroup.centurycloudmovil.Fragments.Inventario;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.glgroup.centurycloudmovil.Controllers.Adapters.AdapterInventario;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Fragments.Inventario.InventarioFragment;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.Inventarios;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ResInventarios;
import com.glgroup.centurycloudmovil.Models.ListaInventario;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaInventariosFragment extends Fragment implements View.OnClickListener, MainActivity.IOnBackPressed{

    public ListaInventariosFragment() {
        // Required empty public constructor
    }

    /** Peticiones retrofit **/
    private Call<Inventarios> reqInventarios;

    /** datos obtenidos retrofit **/
    private Inventarios resInventarios;

    /** vista del fragment **/
    private View view;

    /** elementos de la vista **/
    private ListView listaInventarios;
    private FloatingActionButton addInventario;

    /** lista de inventarios **/
    ArrayList<String> inventarios = new ArrayList<String>();

    /** mensaje al no existir inventarios pendientes **/
    private LinearLayout linInfoDocument;

    /** lista con detalle del inventario **/
    private ArrayList<ListaInventario> listAlert = new ArrayList<>();
    private AdapterInventario adapterAlert;

    /** token **/
    private String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lista_inventarios, container, false);

        init();
        consultaInventarios();
        return view;
    }

    public void init(){
        listaInventarios = view.findViewById(R.id.list_inventario);
        addInventario = view.findViewById(R.id.add_inventario);
        addInventario.setOnClickListener(this);

        linInfoDocument = view.findViewById(R.id.linInfoDocument);

        token = Local.getData("login", getContext(), "token");
        //token = Constantes.TOKEN;

    }

    /** permite controlar la accion del botón de retroceso **/
    @Override
    public boolean onBackPressed() {
        if (true) {
            MainActivity mContext = (MainActivity) getActivity();
            mContext.getSupportActionBar().setTitle("");
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new DefaultFragment())
                    .commit();

            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_inventario:
                agregaInventario();
                break;
        }
    }

    /** alerta para indicar que se creará un nuevo inventario **/
    public void agregaInventario(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getContext());
        dialogo1.setTitle(getString(R.string.importante));
        dialogo1.setMessage(getString(R.string.se_creara_nuevo_inventario));
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton(getString(R.string.confirmar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                Local.setData("inventario", getContext(),"idInventario","0");
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new InventarioFragment())
                        .commit();
            }
        });
        dialogo1.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

            }
        });
        dialogo1.show();
    }


    /** obtiene inventarios **/
    public void consultaInventarios(){
        String usuario = Local.getData("login", getContext(), "idUsuario");
        String cliente = Local.getData("login", getContext(), "idCLiente");
        String proyecto = Local.getData("login", getContext(), "idProyecto");
        String locacion = Local.getData("login", getContext(), "idLocacion");
        String area = Local.getData("login", getContext(), "idArea");

        inventarios.clear();
        reqInventarios = ApiService.getApiService(getContext()).getinventarios(usuario,cliente,proyecto,locacion,area,"1","866502031287673", token);
        reqInventarios.enqueue(new Callback<Inventarios>() {
            @Override
            public void onResponse(Call<Inventarios> call, Response<Inventarios> response) {
                if(response.isSuccessful()){
                  resInventarios = response.body();
                  for(ResInventarios data : resInventarios.getDatos().getResultado()){
                      if(data.getIdInventario() < 0){
                          linInfoDocument.setVisibility(View.VISIBLE);
                          break;
                      }
                      Log.d("inventarios", data.getDescripcion());
                      inventarios.add(data.getDescripcion());

                  }
                  if(inventarios.size() > 0) {
                      muestraInventarios();
                  }
                }
            }

            @Override
            public void onFailure(Call<Inventarios> call, Throwable t) {

            }
        });
    }

    /** se listan los inventarios obtenidos **/
    public void muestraInventarios(){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1, inventarios);
        listaInventarios.setAdapter(adapter);

        listaInventarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int item = i;
                String valor = (String)listaInventarios.getItemAtPosition(i);
                muestraDetalleInventario(valor);
            }
        });
    }

    /** se muestra un detalle del inventario seleccionado **/
    public void muestraDetalleInventario(String descripcion){
        listAlert.clear();
        for(ResInventarios data : resInventarios.getDatos().getResultado()){
            if(data.getDescripcion().equals(descripcion)){
                listAlert.add(new ListaInventario(0, "Inventario", data.getDescripcion()));
                listAlert.add(new ListaInventario(0, "Creado", data.getFechaCreacion()));
                listAlert.add(new ListaInventario(0, "Usuario", data.getUsuarioInventario()));
                listAlert.add(new ListaInventario(0, "Proyecto", data.getProyecto()));
                listAlert.add(new ListaInventario(0, "Locacion", data.getLocacion()));
                listAlert.add(new ListaInventario(0, "Area", data.getArea()));
                Local.setData("inventario", getContext(), "idInventario", String.valueOf(data.getIdInventario()));
            }
        }
        if(listAlert.size() > 0){
            adapterAlert = new AdapterInventario(listAlert, getActivity());
            View viewAlert = getLayoutInflater().inflate(R.layout.list_detalle_inventario, null);
            ListView lv = (ListView) viewAlert.findViewById(R.id.list_alert_builder);
            lv.setAdapter(adapterAlert);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(getString(R.string.informacion));
            builder.setView(viewAlert);

            builder.setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.content_frame, new InventarioFragment())
                                    .commit();
                        }
                    })
                    .setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            alert.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        }

    }



}
