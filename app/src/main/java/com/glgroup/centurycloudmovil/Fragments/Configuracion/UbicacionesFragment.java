package com.glgroup.centurycloudmovil.Fragments.Configuracion;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas.Area;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas.ResArea;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes.Cliente;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes.ResCliente;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones.Locacion;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones.ResLocacion;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos.Proyecto;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos.ResProyecto;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.glgroup.centurycloudmovil.ui.UbicacionesActivity;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UbicacionesFragment extends Fragment implements MainActivity.IOnBackPressed{

    public UbicacionesFragment() {
        // Required empty public constructor
    }


    /** actividad donde se encuentra el lector **/
    private MainActivity mContext;

    /** peticiones retrofit **/
    private Call<Cliente> reqCliente;
    private Call<Proyecto> reqProyecto;
    private Call<Locacion> reqLocacion;
    private Call<Area> reqArea;

    /** datos obtenidos retrofit **/
    private Cliente resCliente;
    private Proyecto resProyecto;
    private Locacion resLocacion;
    private Area resArea;


    /** spinner de datos **/
    private Spinner spinnerCliente, spinnerProyecto, spinnerLocacion, spinnerArea;

    /** Llave para peticiones **/
    private String token;

    /** variables al cambiar los datos **/
    private String idCliente, idProyectoSel, idLocacionSel, idAreaSel;
    private String nomCliente, nomProyecto, nomLocacion, nomArea;
    private String urlImg;

    /** botones de la vista **/
    private Button btnIngresar, btn_regresar;

    /** ventana de carga **/
    private AlertCarga alert;



    Button cancelar;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ubicaciones, container, false);

        alert = new AlertCarga(getString(R.string.obteniendo_ubicaciones), getActivity(), getLayoutInflater());
        alert.alertCarga();
        alert.alertShow();

        init();
        String idusuario = Local.getData("login", getContext(), "idUsuario");
        consultaCliente(idusuario);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
    }

    /** permite controlar la accion del botón de retroceso **/
    @Override
    public boolean onBackPressed() {

        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();
        return true;

    }

    /** inicio de componentes **/
    public void init(){
        spinnerCliente = view.findViewById(R.id.spinnerCliente);
        spinnerProyecto = view.findViewById(R.id.spinnerProyecto);
        spinnerLocacion = view.findViewById(R.id.spinnerLocacion);
        spinnerArea = view.findViewById(R.id.spinnerArea);

        btnIngresar = view.findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gruardaConfiguracion();
                mContext.cargaMarquee();
                mContext.cargarImagen();
                mContext.getSupportActionBar().setTitle("");
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new DefaultFragment())
                        .commit();
            }
        });

        btn_regresar = view.findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.getSupportActionBar().setTitle("");
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new DefaultFragment())
                        .commit();
            }
        });

        token = Local.getData("login", getContext(), "token");
        //token = Constantes.TOKEN;
    }

    /**
     * Realiza una petición a la base de datos para obtener los clientes asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idUsuario - id obtenido en el login
     */
    public void consultaCliente(final String idUsuario){

        final ArrayList<String> listaCliente = new ArrayList<>();
        reqCliente = ApiService.getApiService(getContext()).getCliente(idUsuario, token);
        reqCliente.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if(response.isSuccessful()){
                    resCliente = response.body();
                    for(ResCliente cliente : resCliente.getDatos().getResCliente()){
                        listaCliente.add(cliente.getDescripcion());
                    }
                    cargaCliente(listaCliente, idUsuario);
                }else{
                    alert.alerDismiss();
                }
            }
            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                alert.alerDismiss();
                //  Toast.makeText(getApplicationContext(), (CharSequence) call,Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * Se rellena el spinner corriespondiente con los clientes asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idUsuario id obtenido en el login
     * @param listClientes  lista corresponde a los datos obtenidos en "consultaClientes"
     */
    public void cargaCliente(ArrayList<String> listClientes, final String idUsuario){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(getContext(), R.layout.item_spinner, listClientes);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCliente.setAdapter(adaptadorCliente);
        spinnerCliente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idClienteSel = 0;
                for (ResCliente data : resCliente.getDatos().getResCliente()) {
                    if(data.getDescripcion().equals(item)){
                        idClienteSel = data.getIdCliente();
                        nomCliente = item;
                        idCliente = String.valueOf(idClienteSel);
                        urlImg = data.getFotoCliente();
                        break;
                    }
                }
                alert.alertShow();
                if(idClienteSel != 0){
                    consultaProyecto(idClienteSel, idUsuario);
                }else {
                    Toasty.warning(getContext(),getString(R.string.no_clientes), Toasty.LENGTH_LONG).show();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //  Toast.makeText(getApplicationContext(),"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Realiza una petición a la base de datos para obtener los proyectos asociados al usuario en relación al cliente seleccionado
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idCliente se obtiene en base al cliente seleccionado
     * @param idUsuario obtenido en el login
     */
    public void consultaProyecto(final int idCliente, final String idUsuario) {
        final ArrayList<String> listaProyectos = new ArrayList<>();
        reqProyecto = ApiService.getApiService(getContext()).getProyecto(idUsuario, String.valueOf(idCliente), token);
        reqProyecto.enqueue(new Callback<Proyecto>() {
            @Override
            public void onResponse(Call<Proyecto> call, Response<Proyecto> response) {
                if(response.isSuccessful()){
                    resProyecto = response.body();
                    boolean validaProyecto = true;
                    for(ResProyecto data : resProyecto.getDatos().getResultado()){
                        if(data.getIdProyecto() > 0) {
                            listaProyectos.add(data.getDescripcion());
                        }else{
                            validaProyecto = false;
                            break;
                        }
                    }
                    if(validaProyecto){
                        cargaProyectos(listaProyectos, idCliente, idUsuario);
                    }else{
                        alert.alerDismiss();
                        Toasty.warning(getContext(),getString(R.string.no_proyectos), Toasty.LENGTH_LONG).show();
                    }
                }else {
                    alert.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<Proyecto> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }


    /**
     * Se rellena el spinner corriespondiente con los proyectos asociados al usuario
     * @author: Erick Oyarce
     * @version:14/07/2020
     * @param listProyectos corresponde a los datos obtenidos en "consultaProyecto"
     */
    public void cargaProyectos(ArrayList<String> listProyectos, final int idCliente, final String idUsuario){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listProyectos);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProyecto.setAdapter(adaptadorCliente);
        spinnerProyecto.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

            @Override public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idProyecto = 0;
                for (ResProyecto data : resProyecto.getDatos().getResultado()) {
                    if(data.getDescripcion().equals(item)){
                        idProyecto = data.getIdProyecto();
                        nomProyecto = item;
                        idProyectoSel = String.valueOf(idProyecto);
                        break;
                    }
                }
                if(idProyecto != 0){
                    consultaLocacion(idCliente,idUsuario,idProyecto);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //   Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Realiza una petición a la base de datos para obtener las loccaciones asociados al usuario en relación al proyecto seleccionado
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idCliente se obtiene en base al cliente seleccionado
     * @param idUsuario obtenido en el login
     * @param idProyecto obtenido en base al proyecto seleccionado
     */
    public void consultaLocacion(final int idCliente, final String idUsuario, final int idProyecto){
        reqLocacion = ApiService.getApiService(getContext()).getLocacion(idUsuario, String.valueOf(idCliente), String.valueOf(idProyecto), token);
        reqLocacion.enqueue(new Callback<Locacion>() {
            @Override
            public void onResponse(Call<Locacion> call, Response<Locacion> response) {
                if(response.isSuccessful()){
                    resLocacion = response.body();
                    boolean locacionValida = true;
                    ArrayList<String> listLocacion = new ArrayList<>();
                    for (ResLocacion data : resLocacion.getDatos().getResultado()) {
                        if(data.getIdLocacion() > 0){
                            listLocacion.add(data.getDescripcion());
                        }else {
                            locacionValida = false;
                            break;
                        }
                    }
                    if (locacionValida){
                        cargaLocaciones(listLocacion, idCliente, idUsuario, idProyecto);
                    }else {
                        alert.alerDismiss();
                        Toasty.warning(getContext(),getString(R.string.no_locaciones), Toasty.LENGTH_LONG).show();
                    }
                }else {
                    alert.alerDismiss();
                }
            }
            @Override
            public void onFailure(Call<Locacion> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }


    /**
     * Se rellena el spinner corriespondiente con las locaciones asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param listLocacion corresponde a los datos obtenidos en "consultaLocacion"
     */
    public void cargaLocaciones(ArrayList<String> listLocacion, final int idCliente, final String idUsuario, final int idProyecto){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listLocacion);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocacion.setAdapter(adaptadorCliente);
        spinnerLocacion.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

            @Override public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idLocacion = 0;
                for (ResLocacion data : resLocacion.getDatos().getResultado()) {
                    if(data.getDescripcion().equals(item)){
                        idLocacion = data.getIdLocacion();
                        nomLocacion = item;
                        idLocacionSel =  String.valueOf(idLocacion);
                        break;
                    }
                }
                if(idLocacion != 0) {
                    consultaArea(idCliente, idUsuario,idProyecto,idLocacion);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //   Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Realiza una petición a la base de datos para obtener las areas asociados al usuario en relación a la locacion seleccionado
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idCliente se obtiene en base al cliente seleccionado
     * @param idUsuario obtenido en el login
     * @param idProyecto obtenido en base al proyecto seleccionado
     * @param  idLocacion obtenido en base a la locacion seleccionada
     */
    public void consultaArea(int idCliente, String idUsuario, int idProyecto, int idLocacion){
        reqArea = ApiService.getApiService(getContext()).getArea(idUsuario, String.valueOf(idCliente), String.valueOf(idProyecto), String.valueOf(idLocacion), token);
        reqArea.enqueue(new Callback<Area>() {
            @Override
            public void onResponse(Call<Area> call, Response<Area> response) {
                if(response.isSuccessful()){
                    resArea = response.body();
                    boolean areaValida = true;
                    ArrayList<String> listArea = new ArrayList<>();
                    for(ResArea data : resArea.getDatos().getResultado()){
                        if(data.getIdArea() > 0){
                            listArea.add(data.getDescripcion());
                        }else {
                            areaValida = false;
                            break;
                        }
                    }
                    alert.alerDismiss();
                    if (areaValida){
                        cargaAreas(listArea);
                    }else {
                        Toasty.warning(getContext(),getString(R.string.no_areas), Toasty.LENGTH_LONG).show();
                    }
                }else {
                    alert.alerDismiss();
                }
            }
            @Override
            public void onFailure(Call<Area> call, Throwable t) {
                alert.alerDismiss();
                Log.d("Ubicaciones", String.valueOf(call));
                Log.d("Ubicaciones", String.valueOf(t));
            }
        });
    }

    /**
     * Se rellena el spinner corriespondiente con las Areas asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param listAreas corresponde a los datos obtenidos en "consultaAreas"
     */
    public void cargaAreas(ArrayList<String> listAreas){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listAreas);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerArea.setAdapter(adaptadorCliente);
        spinnerArea.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

            @Override public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idArea = 0;
                for(ResArea data : resArea.getDatos().getResultado()){
                    if(data.getDescripcion().equals(item)){
                        idArea = data.getIdArea();
                        nomArea = item;
                        idAreaSel = String.valueOf(idArea);
                        break;
                    }
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //   Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /** Al guardar los datos se aplican los cambios **/
    public void gruardaConfiguracion(){

        Local.setData("login", getContext(), "nomCliente", nomCliente);
        Local.setData("login", getContext(), "idCLiente", idCliente);

        Local.setData("login", getContext(), "nomProyecto", nomProyecto);
        Local.setData("login", getContext(), "idProyecto",idProyectoSel);

        Local.setData("login", getContext(), "nomLocacion", nomLocacion);
        Local.setData("login", getContext(), "idLocacion",idLocacionSel);

        Local.setData("login", getContext(), "nomArea", nomArea);
        Local.setData("login", getContext(), "idArea", idAreaSel);

        Local.setData("login", getContext(), "imgCliente", urlImg);
    }

}
