package com.glgroup.centurycloudmovil.Fragments.Enlace;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.AppDatabase;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.Application.Enlace.Enlace;
import com.glgroup.centurycloudmovil.Models.Application.Enlace.ResEnlace;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.IdItem;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.ResIdItem;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Filtro;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Mensajes;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.ActivityEscanear;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.textfield.TextInputLayout;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnlaceMasivoFragment extends Fragment implements View.OnClickListener, MainActivity.IOnBackPressed{

    public EnlaceMasivoFragment() {
        // Required empty public constructor
    }
    /** vista **/
    private View view;

    /** peticion retrofit **/
    private Call<Enlace> reqEnlace;
    private Call<IdItem> reqIdItem;
    /** respuesta peticion **/
    private Enlace resEnlace;
    private IdItem resIdItem;

    String nombre ="";



    /** elementos de la vista **/
    private EditText   ed_contendor;
    private ImageView btn_scansku, btn_iniciar, btn_detener;
    private Button btn_fijar;
    private CardView  btn_link, btn_clean;
    private TextView txt_count, txt_barcode, tv_nom_item;
    private CheckBox chkSku;

    /** actividad donde se encuentra el lector **/
    private MainActivity mContext;

    /** variable de lectura **/
    boolean isRuning = false;
    private boolean loopFlag = false;

    /** lista de etiquetas leidas **/
    private Map<String, String> etiquetasLeidas = new HashMap<String, String>();
    private ArrayList<String> listItems = new ArrayList<String>();

    /** adaptador para la lista de etiquetas leidas **/
    private ArrayAdapter<String> adapterListEPC;

    /** lista de la vista **/
    private ListView list_epc;

    /** clase para la conexión de dispositivos **/
    private ConnectStatus mConnectStatus = new ConnectStatus();

    /** base de datos **/
    private AppDatabase bd;

    /** ventana de carga **/
    private AlertCarga alert;

    /** se controla si el dispositivo se encuentra conectado o no **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Toast.makeText(getContext(),"Conectado en fragment", Toast.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
                //Toast.makeText(getContext(),"Desconectado en fragment", Toast.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }


    /**
     * hilo para recibir datos de lectura
     **/
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        /** error al detener la lectura **/
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                        Toast.makeText(mContext, "uhf_msg_inventory_stop_fail", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constantes.FLAG_START:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                    }

                    break;

                case Constantes.FLAG_UHFINFO:
                    /** se reciben los datos de las etiquetas **/
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    if (!TextUtils.isEmpty(info.getEPC())) {
                        agregarEtiquetas(info.getEPC());
                    }

                    break;

                case Constantes.FLAG_BARCODE:
                    String data = msg.obj.toString().replace("�","");
                    String result = obtenerNombre(getContext(), data.replace("�",""));
                    if (!result.equals("")) {

                        txt_barcode.setText(data);
                        tv_nom_item.setText(result);
                    } else {

                        Toasty.warning(getContext(), getString(R.string.codigo_barra_no_registrado), Toasty.LENGTH_LONG).show();
                    }

                    break;
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_enlace_masivo, container, false);
        init();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            mContext.uhf.setPower(30);
        }
        mContext.addConnectStatusNotice(mConnectStatus);

        /** acción recibida desde el lector **/
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {
                if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    String nameDevice = Local.getData("dispositivo", getContext(), "nombre");
                    if (nameDevice.contains("R6")) {
                        if (txt_barcode.getText().toString().equals("")) {
                            scan();
                        } else {
                            lectura();
                        }
                    }else{
                        lectura();
                    }
                }
            }
        });

    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }



    /** inicio de los elementos **/
    public void init(){

        /** inicializar la base de datos **/
        bd = Room.databaseBuilder(getContext(), AppDatabase.class, Constantes.BD_NAME)
                .allowMainThreadQueries()
                .build();

        alert = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alert.alertCarga();

        txt_barcode = view.findViewById(R.id.txt_barcode);
        txt_barcode.setOnClickListener(this);

        tv_nom_item = view.findViewById(R.id.tv_nom_item);
        ed_contendor = view.findViewById(R.id.ed_contendor);
        txt_count = view.findViewById(R.id.txt_count);

        chkSku = view.findViewById(R.id.chkSku);

        btn_scansku = view.findViewById(R.id.btn_scansku);
        btn_scansku.setOnClickListener(this);

        btn_iniciar = view.findViewById(R.id.btn_iniciar);
        btn_iniciar.setOnClickListener(this);

        btn_detener = view.findViewById(R.id.btn_detener);
        btn_detener.setOnClickListener(this);

        btn_fijar = view. findViewById(R.id.btn_fijar);
        btn_fijar.setOnClickListener(this);

        btn_link = view.findViewById(R.id.card_link);
        btn_link.setOnClickListener(this);

        btn_clean = view.findViewById(R.id.card_clean);
        btn_clean.setOnClickListener(this);

        list_epc = view.findViewById(R.id.list_epc);


        adapterListEPC = new ArrayAdapter<String>(getContext(),
                R.layout.item_documentos, listItems);

        list_epc.setAdapter(adapterListEPC);

    }

    @Override
    public void onClick(View v) {
        int permiso = 0;
        switch (v.getId()){
            case R.id.btn_scansku:
                if (!Local.getData("permisos", getContext(), "camara").equals("true")) {
                    // Toast.makeText(getActivity(), "Habilitando permisos, vuelve a intentarlo", Toast.LENGTH_SHORT).show();
                    //  permisoSolicitadoDesdeBoton = true;
                    Local.setData("permisos", getContext(), "boton_camara", "true");
                    verificarYPedirPermisosDeCamara();
                    return;
                }
                verificarYPedirPermisosDeCamara();
                break;

            case R.id.card_clean:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                limpiar();
                break;

            case R.id.btn_iniciar:
                /** se comprueba su el dispositivo está conectado **/
                if(mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if (!loopFlag) {
                        detener();
                        startThread();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.btn_detener:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                break;

            case R.id.btn_fijar:
                /** se validan los permisos del boton **/
                if(bd.permisoDao().sp_Sel_Permiso(16) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(16).getPermiso(); }
                if(permiso == 1) {
                    Alertas.fijarItem(getContext(), getLayoutInflater(), tv_nom_item, 2);
                }else{
                    Mensajes.errorPermiso(getContext());
                }
                break;

            case R.id.card_link:
                /** se validan los permisos del boton **/
                if(bd.permisoDao().sp_Sel_Permiso(17) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(17).getPermiso(); }
                if(permiso == 1) {
                    if (Local.getData("enlaceMasivo", getContext(), "fijarItem").equals("1") && !tv_nom_item.getText().toString().equals("")) {
                        if (etiquetasLeidas.size() > 0) {
                            iniciar();
                            stopInventory();
                            enlace();
                        } else {
                            Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                        }
                    } else {
                        /** el usuario puede no usar codigo de barras, pero debe fijar un item en ese caso, nose cual es la finalidad de ésto **/
                        if (chkSku.isChecked()) {

                            /** si está marcado, se comprueba si el item leído es válido obteniendo el campo de descripción del producto **/
                            if (!txt_barcode.getText().toString().equals("")) {

                                /** si el codigo es correcto, se comprueba si existe un rfid leído para enlazar, caso contrario se solicita **/
                                if (etiquetasLeidas.size() > 0) {
                                    iniciar();
                                    stopInventory();
                                    enlace();
                                } else {
                                    Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                                }
                            } else {
                                /** si está vació significa que no se ha leido un codigo de barras o éste era inválido **/
                                Toasty.warning(getContext(), getString(R.string.es_necesario_leer_un_sku_enlace), Toasty.LENGTH_LONG).show();
                            }
                        } else {
                            /** si no está marcado el chkSku, se valida si existe un item fijado en base al nombre de éste **/
                            if (!tv_nom_item.getText().toString().equals("")) {

                                /** si ha un item válido, se comprueba la lectura de un RFID para en enlace, caso contrario se solicita **/
                                if (etiquetasLeidas.size() > 0) {
                                    iniciar();
                                    stopInventory();
                                    enlace();
                                } else {
                                    Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                                }
                            } else {
                                Toasty.warning(getContext(), getString(R.string.debes_fijar_un_item), Toasty.LENGTH_LONG).show();
                            }
                        }
                    }
                }else{
                    Mensajes.errorPermiso(getContext());
                }
                break;

            case R.id.txt_barcode:
                alertSolicitaSku(txt_barcode, tv_nom_item);
                break;
        }
    }

    public void limpiar(){
        etiquetasLeidas.clear();
        txt_barcode.setText("");
        tv_nom_item.setText("");
        ed_contendor.setText("");
        listItems.clear();
        txt_count.setText("0");
        adapterListEPC.notifyDataSetChanged();
        Local.setData("enlaceMasivo", getContext(), "idItem", "");
        Local.setData("enlaceMasivo", getContext(), "fijarItem", "0");
    }


    /** se define si detener o comenzar la lectura **/
    public void lectura(){
        if (!loopFlag) {
            if(!mContext.isScanning) {
                detener();
                startThread();
            }else{
                Toasty.warning(getContext(), getString(R.string.proceso_de_lectura), Toasty.LENGTH_LONG).show();
            }
        }else{
            iniciar();
            stopInventory();
        }
    }

    /** oculta y muestra botones **/
    public void iniciar(){
        btn_iniciar.setVisibility(View.VISIBLE);
        btn_detener.setVisibility(View.GONE);
    }

    /** oculta y muestra botones **/
    public void detener(){
        btn_iniciar.setVisibility(View.GONE);
        btn_detener.setVisibility(View.VISIBLE);
    }


    /** SDK **/
    public void startThread() {
        if(mContext.uhf.getPower() != 30){
            mContext.uhf.setPower(30);
        }
        if (isRuning) {
            return;
        }
        isRuning = true;
        // cbFilter.setChecked(false);
        new TagThread().start();
    }

    /** proceso de lectura rfid **/
    class TagThread extends Thread {
        public void run() {
            Message msg = handler.obtainMessage(Constantes.FLAG_START);
            if (mContext.uhf.startInventoryTag()) {
                loopFlag = true;
                mContext.isScanning = true;
                //mStrTime = System.currentTimeMillis();
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            handler.sendMessage(msg);
            isRuning = false;//执行完成设置成false
            while (loopFlag) {
                getUHFInfo();
                handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
            }
            stopInventory();
        }
    }

    /** obtiene información de la etiqueta y la envía al handler **/
    private void getUHFInfo() {
        List<UHFTAGInfo> list = mContext.uhf.readTagFromBufferList_EpcTidUser();
        if (list != null && !list.isEmpty()) {
            for (int k = 0; k < list.size(); k++) {
                Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO, list.get(k));
                handler.sendMessage(msg);
                if(!loopFlag) {
                    break;
                }
            }
        }
    }

    /** se detiene el proceso de inventario **/
    private void stopInventory() {
        loopFlag = false;
        boolean result = mContext.uhf.stopInventory();
        if(mContext.isScanning) {
            ConnectionStatus connectionStatus = mContext.uhf.getConnectStatus();
            Message msg = handler.obtainMessage(Constantes.FLAG_STOP);
            if (result || connectionStatus == ConnectionStatus.DISCONNECTED) {
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
                //getUHFInfoEx();
            }
            mContext.isScanning = false;
            handler.sendMessage(msg);
        }
    }

    /** agregar las etiquetas a la lista **/
    public void agregarEtiquetas(String epc){
        if(!etiquetasLeidas.containsKey(epc)) {
            etiquetasLeidas.put(epc,epc);
            if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(1);}
            listItems.add(epc);
            adapterListEPC.notifyDataSetChanged();
            txt_count.setText(String.valueOf(etiquetasLeidas.size()));
        }
    }

    /**
     * lector de barras
     **/
    private synchronized void scan() {

        if (!isRuning) {
            isRuning = true;
            new ScanThread().start();
        }
    }

    class ScanThread extends Thread {
        public void run() {
            String data = null;
            byte[] temp = mContext.uhf.scanBarcodeToBytes();
            if (temp != null) {
                try {
                    data = new String(temp, "utf8");
                } catch (Exception e) {

                }
                data = new String(temp);
                if (data != null && !data.isEmpty()) {
                    Message msg = handler.obtainMessage(Constantes.FLAG_BARCODE, data);

                    handler.sendMessage(msg);

                }
            }
            isRuning = false;

        }
    }


    /** permisos para usar la camara como lector de barras **/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constantes.CODIGO_PERMISOS_CAMARA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Escanear directamten solo si fue pedido desde el botón
                    if (Local.getData("permisos", getContext(), "boton_camara").equals("true")) {
                        escanear();
                    }
                    Local.setData("permisos", getContext(), "camara", "true");
                    //  permisoCamaraConcedido = true;
                } else {
                    Local.setData("permisos", getContext(), "camara", "false");
                    Toast.makeText(getActivity(), getString(R.string.error_permisos), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void verificarYPedirPermisosDeCamara() {
        int estadoDePermiso = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (estadoDePermiso == PackageManager.PERMISSION_GRANTED) {
            // En caso de que haya dado permisos ponemos la bandera en true
            // y llamar al método
            // permisoCamaraConcedido = true;
            Local.setData("permisos", getContext(), "camara", "true");
            escanear();
        } else {
            // Si no, pedimos permisos. Ahora mira onRequestPermissionsResult
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    Constantes.CODIGO_PERMISOS_CAMARA);
        }
    }

    /**
     * se accede a la vista para la lectura de codigo
     **/
    private void escanear() {
        Intent i = new Intent(getActivity(), ActivityEscanear.class);
        startActivityForResult(i, Constantes.CODIGO_INTENT);
    }

    /**
     * se obtiene la lectura de codigo de barras
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constantes.CODIGO_INTENT) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    String codigo = data.getStringExtra("codigo");
                    //txt_barcode.setText(codigo);
                    String result = obtenerNombre(getContext(), codigo);
                    if (!result.equals("")) {

                        txt_barcode.setText(codigo);
                        tv_nom_item.setText(result);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.codigo_barra_no_registrado), Toasty.LENGTH_LONG).show();
                    }
                    //obtenerNombre(codigo);
                }
            }
        }
    }



    /**
     * Enlace
     **/
    public void enlace() {
        alert.alertShow();
        String tags = Filtro.Filtro(etiquetasLeidas);

        String idItem = Local.getData("enlaceMasivo", getContext(), "idItem");
        String token = Local.getData("login", getContext(), "token");
        String usuario = Local.getData("login", getContext(), "idUsuario");
        String cliente = Local.getData("login", getContext(), "idCLiente");
        String proyecto = Local.getData("login", getContext(), "idProyecto");
        String locacion = Local.getData("login", getContext(), "idLocacion");
        String area = Local.getData("login", getContext(), "idArea");

        reqEnlace = ApiService.getApiService(getContext()).getenlace(usuario, cliente, proyecto, locacion, area, txt_barcode.getText().toString(), idItem,tags, Constantes.ENLACE_MASIVO, Constantes.ENLACE, Constantes.IMEI, ed_contendor.getText().toString(), token);
        reqEnlace.enqueue(new Callback<Enlace>() {
            @Override
            public void onResponse(Call<Enlace> call, Response<Enlace> response) {
                if (response.isSuccessful()) {
                    resEnlace = response.body();
                    alert.alerDismiss();
                    for (ResEnlace data : resEnlace.getDatos().getResultado()) {
                        if (data.getIdMsj() > 0) {
                            Toasty.success(getContext(), data.getMensaje(), Toasty.LENGTH_LONG).show();

                            /** Si existe un item fijado, se limpia solo el campo rfid, caso contrario se limpia toda la vista **/
                            if (Local.getData("enlace", getContext(), "fijarItem").equals("1")) {
                                listItems.clear();
                                etiquetasLeidas.clear();
                                ed_contendor.setText("0");
                                adapterListEPC.notifyDataSetChanged();
                            } else {
                                limpiar();
                            }
                        } else {
                            listItems.clear();
                            etiquetasLeidas.clear();
                            adapterListEPC.notifyDataSetChanged();
                            ed_contendor.setText("0");
                            Toasty.error(getContext(), data.getMensaje(), Toasty.LENGTH_LONG).show();
                            //ed_rfid.setText("");
                        }
                    }
                }else{
                    alert.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<Enlace> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }


    /** ingresar sku manualmente **/
    public void alertSolicitaSku(final TextView txt_barcode,final TextView nombre_item ){
        final android.app.AlertDialog alertSku;
        final android.app.AlertDialog.Builder solicitaSku = new android.app.AlertDialog.Builder(getContext());


        View dialog = getLayoutInflater().inflate(R.layout.alert_ingresa_sku, null);

        final TextInputLayout codigo = dialog.findViewById(R.id.ed_sku);
        final Button ok = dialog.findViewById(R.id.btn_validar);
        final Button cancel = dialog.findViewById(R.id.btn_regresar);

        solicitaSku.setView(dialog);

        alertSku = solicitaSku.create();
        alertSku.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sku = codigo.getEditText().getText().toString().trim();
                if(sku.equals("")){
                    codigo.setErrorEnabled(true);
                    codigo.setError(getContext().getString(R.string.requerido));
                }else{
                    codigo.setErrorEnabled(false);
                    alertSku.dismiss();
                    String result = obtenerNombre(getContext(), sku);
                    if (!result.equals("")) {
                        txt_barcode.setText(sku);
                        nombre_item.setText(result);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.codigo_barra_no_registrado), Toasty.LENGTH_LONG).show();
                    }
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertSku.dismiss();
            }
        });
        alertSku.show();
    }

    public String obtenerNombre(final Context context, String sku) {
        String token = Local.getData("login", context, "token");
        String usuario = Local.getData("login", context, "idUsuario");
        String cliente = Local.getData("login", context, "idCLiente");
        String proyecto = Local.getData("login", context, "idProyecto");
        String locacion = Local.getData("login", context, "idLocacion");
        String area = Local.getData("login", context, "idArea");

        reqIdItem = ApiService.getApiService(context).getiditem(usuario, cliente, proyecto, locacion, area, sku, token);
        reqIdItem.enqueue(new Callback<IdItem>() {
            @Override
            public void onResponse(Call<IdItem> call, Response<IdItem> response) {
                if (response.isSuccessful()) {
                    resIdItem = response.body();
                    for (ResIdItem data : resIdItem.getDatos().getResultado()) {
                        Log.d("item", data.getItem());
                        Log.d("item", String.valueOf(data.getIdItem()));
                        if (data.getIdItem() > 0) {
                            Local.setData("enlace", context, "idItem", String.valueOf(data.getIdItem()));
                            nombre = data.getItem();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<IdItem> call, Throwable t) {

            }
        });


        return nombre;
    }

}
