package com.glgroup.centurycloudmovil.Fragments.Desenlace;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glgroup.centurycloudmovil.Controllers.Adapters.viewPagerAdapter;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Fragments.Enlace.EnlaceMasivoFragment;
import com.glgroup.centurycloudmovil.Fragments.Enlace.EnlaceUnicoFragment;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class DesenlaceFragment extends Fragment implements MainActivity.IOnBackPressed{

    public DesenlaceFragment() {
        // Required empty public constructor
    }

    MainActivity mContext;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_desenlace, container, false);
        Alertas.alertEnlace(getContext(),getLayoutInflater(),mContext);
        //cargaVistas();
        return view;
    }

    public void cargaVistas(){
        mContext = (MainActivity) getActivity();
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        ViewPager viewPager = view.findViewById(R.id.view_pager);

        viewPagerAdapter viewPagerAdapter = new viewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPagerAdapter.addFragment(new DesenlaceUnicoFragment(), getString(R.string.unico));
        viewPagerAdapter.addFragment(new DesenlaceMasivoFragment(), getString(R.string.masivo));

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }

}
