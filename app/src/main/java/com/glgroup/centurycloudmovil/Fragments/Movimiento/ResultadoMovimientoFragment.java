package com.glgroup.centurycloudmovil.Fragments.Movimiento;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Controllers.Adapters.AdaptadorResulMovimiento;
import com.glgroup.centurycloudmovil.Controllers.Adapters.ResultInventario;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ResCierreInventario;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ResListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.CierreMovimiento;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.ListaRfidLeidosMov;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.ResCierreMovimiento;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.ResListaRfidLeidosMov;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.protobuf.Api;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ResultadoMovimientoFragment extends Fragment implements MainActivity.IOnBackPressed {

    /** vista fragment **/
    private View view;

    /** elementos de la vista **/
    private RecyclerView resultadoLectura;
    private Button btn_enviar;
    private TextView sub_title;

    /** peticion retrofit **/
    private Call<ListaRfidLeidosMov> reqListaLeidosMov;
    private Call<CierreMovimiento> reqCierreMovimiento;

    /** respuesta retrofit **/
    private ListaRfidLeidosMov resListaLeidosMov;
    private CierreMovimiento resCierreMovimiento;

    /**adaptador **/
    private AdaptadorResulMovimiento resultMovimientoAdapter;
    private ArrayList<ResListaRfidLeidosMov> listItemMovimiento = new ArrayList<>();

    /** resultados mostrados en la vista **/
    LinearLayoutManager VerticalLayout;

    /**
     * variables de datos
     **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;
    private String area;
    private String etiquetas;
    private String movimiento;
    private String locacionOriDes;
    private String areaOriDes;

    /** contexto de la vista **/
    private MainActivity mContext;

    /** ventana de carga **/
    private AlertCarga alertProcesar, alertValidar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_resultado_movimiento, container, false);
        init();
        obtenerListaLeidos();
        return view;
    }
    /** inicio de componentes y variables **/
    public void init(){
        mContext = (MainActivity) getActivity();

        alertProcesar = new AlertCarga(getString(R.string.procesando_lectura), getActivity(), getLayoutInflater());
        alertProcesar.alertCarga();

        alertValidar = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alertValidar.alertCarga();

        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
        area = Local.getData("login", getContext(), "idArea");
        etiquetas = Local.getData("movimiento", getContext(), "etiquetas");
        movimiento = Local.getData("movimiento", getContext(), "tipo");
        locacionOriDes = Local.getData("movimiento", getContext(), "idLocacion");
        areaOriDes = Local.getData("movimiento", getContext(), "idArea");

        resultadoLectura = view.findViewById(R.id.resultadoLectura);

        sub_title = view.findViewById(R.id.sub_title);
        sub_title.setText(getString(R.string.items_leidos)+" "+Local.getData("movimiento", getContext(), "cantLeida"));

        btn_enviar = view.findViewById(R.id.btnEnd);
        btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cierreMovimiento();
            }
        });
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new LecturaMovimientoFragment())
                .commit();
        return true;
    }

    /** obtener datos lectura **/
    public void obtenerListaLeidos(){
        alertProcesar.alertShow();
        VerticalLayout = new LinearLayoutManager(getActivity(),  LinearLayoutManager.VERTICAL,false);
        resultadoLectura.setHasFixedSize(true);

        reqListaLeidosMov = ApiService.getApiService(getContext()).getlistaleidosmov(usuario,cliente,proyecto,locacion,area,movimiento,locacionOriDes,areaOriDes,etiquetas, Constantes.IMEI,token);
        reqListaLeidosMov.enqueue(new Callback<ListaRfidLeidosMov>() {
            @Override
            public void onResponse(Call<ListaRfidLeidosMov> call, Response<ListaRfidLeidosMov> response) {
                if(response.isSuccessful()){
                    resListaLeidosMov = response.body();
                    listItemMovimiento.clear();
                    int validar = 0;
                    for(ResListaRfidLeidosMov data : resListaLeidosMov.getDatos().getResultado()){
                        validar = data.getIdItem();
                        if(validar == -1){
                            break;
                        }
                        listItemMovimiento.add(data);
                    }
                    alertProcesar.alerDismiss();
                    if(validar >= 0){
                        resultMovimientoAdapter = new AdaptadorResulMovimiento(listItemMovimiento);
                        resultadoLectura.setLayoutManager(VerticalLayout);
                        resultadoLectura.setAdapter(resultMovimientoAdapter);
                    }else{

                    }
                }else{
                    alertProcesar.alerDismiss();
                }
            }
            @Override
            public void onFailure(Call<ListaRfidLeidosMov> call, Throwable t) {
                alertProcesar.alerDismiss();
            }
        });
    }

    /** cierre del movimiento **/
    public void cierreMovimiento(){
        alertValidar.alertShow();
        reqCierreMovimiento = ApiService.getApiService(getContext()).getcierremovimiento(usuario,cliente,proyecto,locacion,area,movimiento,locacionOriDes,areaOriDes,etiquetas,Constantes.IMEI,"0", token);
        reqCierreMovimiento.enqueue(new Callback<CierreMovimiento>() {
            @Override
            public void onResponse(Call<CierreMovimiento> call, Response<CierreMovimiento> response) {
                if(response.isSuccessful()){
                    resCierreMovimiento = response.body();
                    int validar = 0;
                    String mensaje = "";
                    for(ResCierreMovimiento data : resCierreMovimiento.getDatos().getResultado()){
                        validar = data.getIdMsj();
                        mensaje = data.getMensaje();
                    }
                    alertValidar.alerDismiss();
                    if(validar > 0){
                        Toasty.success(getContext(), mensaje,Toasty.LENGTH_LONG).show();
                        mContext.getSupportActionBar().setTitle("");
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_frame, new DefaultFragment())
                                .commit();
                    }else{
                        Alertas.errorCierre(mensaje,getContext());
                    }
                }else{
                    alertValidar.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<CierreMovimiento> call, Throwable t) {
                alertValidar.alerDismiss();
            }
        });
    }
}