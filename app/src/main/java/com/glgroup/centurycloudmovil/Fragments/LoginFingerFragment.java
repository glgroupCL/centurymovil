package com.glgroup.centurycloudmovil.Fragments;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.FingerLogin.FingerClass;
import com.glgroup.centurycloudmovil.Controllers.FingerLogin.FingerprintHandler;
import com.glgroup.centurycloudmovil.R;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFingerFragment extends Fragment implements View.OnClickListener{

    public LoginFingerFragment() {
        // Required empty public constructor
    }
    public static LoginFingerFragment newInstance() {
        return new LoginFingerFragment();
    }

    /** Variables para desbloqueo por huella digital **/
    KeyguardManager keyguardManager;
    FingerprintManager fingerprintManager;
    FingerClass fingerClass;

    /** elementos de la vista **/
    ImageView finger;
    Button logCred;


    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login_finger, container, false);

        init();

        return view;
    }

    public void init(){
        keyguardManager = (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
        fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);

        finger = view.findViewById(R.id.iconFinger);
        finger.setOnClickListener(this);

        logCred = view.findViewById(R.id.btnLogCred);
        logCred.setOnClickListener(this);

        /**se valida si el dispositivo cuenta con lector de huellas **/
        if (!fingerprintManager.isHardwareDetected()) {
            finger.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iconFinger:
                huella();
                break;

            case R.id.btnLogCred:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameContent, LoginCredentialFragment.newInstance())
                        .commitNow();
                break;
        }

    }
    public void huella() {
        // Check whether the device has a Fingerprint sensor.

        // Checks whether fingerprint permission is set on manifest
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
      //      Toast.makeText(getContext(), "1", Toast.LENGTH_SHORT).show();
            //  textView.setText("Fingerprint authentication permission not enabled");
        } else {
       //     Toast.makeText(getContext(), "2", Toast.LENGTH_SHORT).show();
            // Check whether at least one fingerprint is registered
            if (!fingerprintManager.hasEnrolledFingerprints()) {
          //      Toast.makeText(getContext(), "3", Toast.LENGTH_SHORT).show();
                //   textView.setText("Register at least one fingerprint in Settings");
            } else {
          //      Toast.makeText(getContext(), "4", Toast.LENGTH_SHORT).show();
                // Checks whether lock screen security is enabled or not
                if (!keyguardManager.isKeyguardSecure()) {
             //       Toast.makeText(getContext(), "5", Toast.LENGTH_SHORT).show();
                    //   textView.setText("Lock screen security not enabled in Settings");
                } else {
                //    Toast.makeText(getContext(), "6", Toast.LENGTH_SHORT).show();
                    fingerClass = new FingerClass();
                    fingerClass.generateKey();


                    if (fingerClass.cipherInit()) {
              //          Toast.makeText(getContext(), "7", Toast.LENGTH_SHORT).show();
                        FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(fingerClass.getCipher());
                        FingerprintHandler helper = new FingerprintHandler(getContext());
                        helper.startAuth(fingerprintManager, cryptoObject);
                    }else{
                 //       Toast.makeText(getContext(), "8", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

    }


}
