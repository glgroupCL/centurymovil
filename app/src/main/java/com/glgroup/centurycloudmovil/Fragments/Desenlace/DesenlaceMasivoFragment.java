package com.glgroup.centurycloudmovil.Fragments.Desenlace;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Adapters.AdaptadorDetalleDesenlace;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.Application.Desenlace.NombreRfid;
import com.glgroup.centurycloudmovil.Models.Application.Desenlace.ResNombreRfid;
import com.glgroup.centurycloudmovil.Models.Datos.ResListaBusquedaRfid;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Filtro;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DesenlaceMasivoFragment extends Fragment implements View.OnClickListener, MainActivity.IOnBackPressed{


    public DesenlaceMasivoFragment() {
    }
    /** elementos de la vista **/
    private TextView contador;
    private CardView leer, pausar, card_clean, card_unlink, card_detail;

    /** lista de etiquetas leidas **/
    private Map<String, String> etiquetasLeidas = new HashMap<String, String>();

    /** peticiones retrofit **/
    private Call<NombreRfid> reqDesenlace;
    private Call<NombreRfid> reqNombreRfid;

    /** respuestas retrofit **/
    private NombreRfid resDesenlace;
    private NombreRfid resNombreRfid;

    /** variables de datos **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;


    /** vista **/
    private View view;

    /** Contexto **/
    private MainActivity mContext;

    /** variable de lectura **/
    boolean isRuning = false;
    private boolean loopFlag = false;

    /** ventana de carga **/
    private AlertCarga alert;

    /** clase para la conexión de dispositivos **/
    private ConnectStatus mConnectStatus = new ConnectStatus();


    /** se controla si el dispositivo se encuentra conectado o no **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Toast.makeText(getContext(),"Conectado en fragment", Toast.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
                //Toast.makeText(getContext(),"Desconectado en fragment", Toast.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }


    /**
     * hilo para recibir datos de lectura
     **/
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        /** error al detener la lectura **/
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                        Toast.makeText(mContext, "uhf_msg_inventory_stop_fail", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constantes.FLAG_START:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                    }

                    break;

                case Constantes.FLAG_UHFINFO:
                    /** se reciben los datos de las etiquetas **/
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    if (!TextUtils.isEmpty(info.getEPC())) {
                        agregarEtiquetas(info.getEPC());
                    }

                    break;
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_desenlace_masivo, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        init();
        mContext.addConnectStatusNotice(mConnectStatus);

        /** acción recibida desde el lector **/
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {
                if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    lectura();
                }
            }
        });
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }

    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }

    public void init(){

        alert = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alert.alertCarga();

        contador = view.findViewById(R.id.lbl_contador);

        leer = view.findViewById(R.id.leer);
        leer.setOnClickListener(this);

        pausar = view.findViewById(R.id.pausar);
        pausar.setOnClickListener(this);

        card_clean = view.findViewById(R.id.card_clean);
        card_clean.setOnClickListener(this);

        card_unlink = view.findViewById(R.id.card_unlink);
        card_unlink.setOnClickListener(this);

        card_detail = view.findViewById(R.id.card_detail);
        card_detail.setOnClickListener(this);

        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.leer:
                /** se comprueba su el dispositivo está conectado **/
                if(mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if (!loopFlag) {
                        detener();
                        startThread();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.pausar:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                break;

            case R.id.card_clean:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                limpiar();
                break;

            case R.id.card_unlink:
                if(etiquetasLeidas.size() > 0){
                    if (loopFlag) {
                        iniciar();
                        stopInventory();
                    }
                    String tags = Filtro.Filtro(etiquetasLeidas);
                    desenlace(tags);
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.card_detail:
                if(etiquetasLeidas.size() > 0){
                    if (loopFlag) {
                        iniciar();
                        stopInventory();
                    }
                    String tags = Filtro.Filtro(etiquetasLeidas);
                    detalle(tags);
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                }
                break;
        }
    }

    /** limpia campos **/
    public void limpiar(){
        etiquetasLeidas.clear();
        contador.setText("0");
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }


    /** se define si detener o comenzar la lectura **/
    public void lectura(){
        if (!loopFlag) {
            if(!mContext.isScanning) {
                detener();
                startThread();
            }else{
                Toasty.warning(getContext(), getString(R.string.proceso_de_lectura), Toasty.LENGTH_LONG).show();
            }
        }else{
            iniciar();
            stopInventory();
        }
    }

    public void iniciar(){
        leer.setVisibility(View.VISIBLE);
        pausar.setVisibility(View.GONE);
    }

    public void detener(){
        leer.setVisibility(View.GONE);
        pausar.setVisibility(View.VISIBLE);
    }


    /** SDK **/
    public void startThread() {
        if(mContext.uhf.getPower() != 15){
            mContext.uhf.setPower(15);
        }
        if (isRuning) {
            return;
        }
        isRuning = true;
        // cbFilter.setChecked(false);
        new TagThread().start();
    }

    /** proceso de lectura rfid **/
    class TagThread extends Thread {
        public void run() {
            Message msg = handler.obtainMessage(Constantes.FLAG_START);
            if (mContext.uhf.startInventoryTag()) {
                loopFlag = true;
                mContext.isScanning = true;
                //mStrTime = System.currentTimeMillis();
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            handler.sendMessage(msg);
            isRuning = false;//执行完成设置成false
            while (loopFlag) {
                getUHFInfo();
                handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
            }
            stopInventory();
        }
    }

    /** obtiene información de la etiqueta y la envía al handler **/
    private void getUHFInfo() {
        List<UHFTAGInfo> list = mContext.uhf.readTagFromBufferList_EpcTidUser();
        if (list != null && !list.isEmpty()) {
            for (int k = 0; k < list.size(); k++) {
                Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO, list.get(k));
                handler.sendMessage(msg);
                if(!loopFlag) {
                    break;
                }
            }
        }
    }

    /** se detiene el proceso de inventario **/
    private void stopInventory() {
        loopFlag = false;
        boolean result = mContext.uhf.stopInventory();
        if(mContext.isScanning) {
            ConnectionStatus connectionStatus = mContext.uhf.getConnectStatus();
            Message msg = handler.obtainMessage(Constantes.FLAG_STOP);
            if (result || connectionStatus == ConnectionStatus.DISCONNECTED) {
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
                //getUHFInfoEx();
            }
            mContext.isScanning = false;
            handler.sendMessage(msg);
        }
    }

    /** agregar las etiquetas a la lista **/
    public void agregarEtiquetas(String epc){
        if(!etiquetasLeidas.containsKey(epc)) {
            etiquetasLeidas.put(epc,epc);
            if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(1);}
            contador.setText(String.valueOf(etiquetasLeidas.size()));
        }
    }

    /** desenlace **/
    public void desenlace(String tags){
        alert.alertShow();
        reqDesenlace = ApiService.getApiService(getContext()).getdesenlace(usuario,cliente,proyecto,locacion,tags,Constantes.DESENLACE_MASIVO,Constantes.DESENLACE,Constantes.IMEI,token);
        reqDesenlace.enqueue(new Callback<NombreRfid>() {
            @Override
            public void onResponse(Call<NombreRfid> call, Response<NombreRfid> response) {
                if(response.isSuccessful()){
                    resDesenlace = response.body();
                    int valido = 0;
                    ArrayList<String> items = new ArrayList<String>();
                    for(ResNombreRfid data : resDesenlace.getDatos().getResultado()){
                        valido = data.getIdItem();
                        if(valido == -2 || valido > 0) {
                            items.add(data.getRfid()+"\n"+data.getDescripcion());
                        }
                    }
                    alert.alerDismiss();
                    verDesenlace(items);
                    //limpiar();
                }
            }

            @Override
            public void onFailure(Call<NombreRfid> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }


    /** muestra el detalle del desenlace **/
    public void verDesenlace(ArrayList<String> items){
        /** constructores de alerta **/
        AlertDialog alertResultado;
        AlertDialog.Builder resultado = new AlertDialog.Builder(getContext());

        /** vista del alert **/
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_ver_desenlace, null);
        ListView listView = view.findViewById(R.id.listaItems);

        /** se asigna un adaptador a la lista, con el resultado de los items desenlazados **/
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);

        /** se asigna la vista al alert **/
        resultado.setView(view);
        resultado.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                limpiar();
            }
        });

        /** se acrea el alert **/
        alertResultado = resultado.create();
        alertResultado.setCancelable(false);
        alertResultado.show();
    }


    /** detalles de etiquetas **/
    public void detalle(String tags){
        alert.alertShow();
        reqNombreRfid = ApiService.getApiService(getContext()).getitemrfid(usuario,cliente,proyecto,locacion,tags,"2","1", Constantes.IMEI, token);
        reqNombreRfid.enqueue(new Callback<NombreRfid>() {
            @Override
            public void onResponse(Call<NombreRfid> call, Response<NombreRfid> response) {
                if(response.isSuccessful()){
                    resNombreRfid = response.body();
                    int validar = 0;
                    ArrayList<ResListaBusquedaRfid> listaRFID = new ArrayList<>();
                    for(ResNombreRfid data : resNombreRfid.getDatos().getResultado()){
                        validar = data.getIdItem();
                        if(validar == -2 || validar > 0){
                            listaRFID.add(new ResListaBusquedaRfid(data.getRfid(), data.getDescripcion()));
                        }
                    }
                    alert.alerDismiss();
                    if(validar == -2 || validar > 0 ){
                        verDetalle(listaRFID);
                    }
                }
            }

            @Override
            public void onFailure(Call<NombreRfid> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }

    /** muestra el detalle de la lectur **/
    public void verDetalle(final ArrayList<ResListaBusquedaRfid> listaRFID){
        /** constructores de alerta **/
        final AlertDialog alertResultado;
        AlertDialog.Builder resultado = new AlertDialog.Builder(getContext());

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());

        /** vista del alert **/
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_ver_detalles, null);

        RecyclerView recyclerView = view.findViewById(R.id.listaItems);
        final Button regresar = view.findViewById(R.id.btnRegresar);
        final Button btn_desenlace = view.findViewById(R.id.btn_unlink);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        /**adaptador **/
        final AdaptadorDetalleDesenlace mAdapter = new AdaptadorDetalleDesenlace(listaRFID);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new AdaptadorDetalleDesenlace.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {}
            @Override
            public void onDeleteClick(int position) {
                /** es posible remover de la lista los rfid que no necesitan desenlace **/
                listaRFID.remove(position);
                mAdapter.notifyItemRemoved(position);
            }
        });

        /** se asigna un adaptador a la lista, con el resultado de los items desenlazados **/

        /** se asigna la vista al alert **/
        resultado.setView(view);
        alertResultado = resultado.create();
        alertResultado.setCancelable(false);

        btn_desenlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listaRFID.size() > 0) {
                    /** se obtienen los rfid restantes de la lista y se envían para el desenlace **/
                    Map<String, String> etiquetasAux = new HashMap<String, String>();
                    for (int i = 0; i < listaRFID.size(); i++) {
                        etiquetasAux.put(listaRFID.get(i).getRfid(), listaRFID.get(i).getRfid());
                    }
                    Log.d("lista", String.valueOf(etiquetasAux));
                    String tags = Filtro.Filtro(etiquetasAux);
                    desenlace(tags);
                    alertResultado.dismiss();
                }else {
                    Toasty.warning(getContext(), getString(R.string.lista_items_vacia), Toasty.LENGTH_LONG).show();
                    alertResultado.dismiss();
                }
            }
        });
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertResultado.dismiss();
            }
        });

        /** se acrea el alert **/
        alertResultado.show();
    }


}