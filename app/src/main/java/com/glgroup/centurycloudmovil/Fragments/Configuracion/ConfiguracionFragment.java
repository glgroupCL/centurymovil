package com.glgroup.centurycloudmovil.Fragments.Configuracion;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.rscja.deviceapi.interfaces.ConnectionStatus;

import java.util.Locale;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfiguracionFragment extends Fragment implements MainActivity.IOnBackPressed{

    public ConfiguracionFragment() {
        // Required empty public constructor
    }

    /** actividad donde se encuentra el lector **/
    private MainActivity mContext;


    LinearLayout ubicaciones, linPermiso;
    Switch switchHuella, switchTheme, switchSonido;
    RelativeLayout relIdioma;
    TextView textIdioma;
    ImageView img_sonido;

    /** cambio de idioma **/
    private Locale locale;
    private Configuration config = new Configuration();

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_configuracion, container, false);

        init();


        selectLogin();


        return view;
    }

    public void init(){
        textIdioma = view.findViewById(R.id.textIdioma);

        String lang = Local.getData("lenguaje",getContext(),"lang");
        if(lang == null ){textIdioma.setText(getString(R.string.str_es));
        }else {
            if(lang.equals("es")){textIdioma.setText(getString(R.string.str_es));}
            if(lang.equals("en")){textIdioma.setText(getString(R.string.str_en));}
            if(lang.equals("zh")){textIdioma.setText(getString(R.string.str_zh));}
        }

        img_sonido = view.findViewById(R.id.img_sonido);
        switchSonido = view.findViewById(R.id.switchSonido);
        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){
            switchSonido.setChecked(true);
            sonidoOn();
        }else{
            switchSonido.setChecked(false);
            sonidoOff();
        }
        switchSonido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switchSonido.isChecked()){
                    sonidoOn();
                }else{
                   sonidoOff();
                }
            }
        });


        ubicaciones = view.findViewById(R.id.linUbic);
        ubicaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                configuracion();
            }
        });

        relIdioma = view.findViewById(R.id.relIdioma);
        relIdioma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idioma();
            }
        });

        linPermiso = view.findViewById(R.id.linPermiso);
        linPermiso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mContext.checkLocationEnable() == 1){
                    Toasty.success(getContext(), "Permisos Habilitados", Toasty.LENGTH_LONG).show();
                }
            }
        });

        switchHuella = view.findViewById(R.id.switchHuella);
    }

    public void sonidoOn(){
        img_sonido.setImageResource(R.drawable.ic_sound);
        Local.setData("sonido",getContext(),"activado", "1");
    }

    public void sonidoOff(){
        img_sonido.setImageResource(R.drawable.ic_mute);
        Local.setData("sonido",getContext(),"activado", "0");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
    }

    public void configuracion() {
        mContext.getSupportActionBar().setTitle(getString(R.string.ubicaciones));
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new UbicacionesFragment())
                .commit();

    }

    public void idioma(){
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(getString(R.string.idioma));
        //obtiene los idiomas del array de string.xml
        String[] types = getResources().getStringArray(R.array.languages);
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch(which){
                    case 0:
                        locale = new Locale("en");
                        config.locale =locale;
                        Local.setData("lenguaje",getContext(),"lang","en");
                        break;
                    case 1:
                        locale = new Locale("es");
                        config.locale =locale;
                        Local.setData("lenguaje",getContext(),"lang","es");
                        break;
                    case 2:
                        locale = new Locale("zh");
                        config.locale =locale;
                        Local.setData("lenguaje",getContext(),"lang","zh");
                        break;

                }
                getResources().updateConfiguration(config, null);
                Intent refresh = new Intent(getActivity(), MainActivity.class);
                if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    mContext.mIsActiveDisconnect = true; // 主动断开为true
                    mContext.uhf.disconnect();
                }
                startActivity(refresh);
            }

        });

        b.show();
    }

    public void selectLogin(){
        if (Local.getData("acceso", getContext(), "finger").equals("1")) {
            switchHuella.setChecked(true);
        } else {
            switchHuella.setChecked(false);
        }
        switchHuella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (switchHuella.isChecked()) {
                    Local.setData("acceso", getContext(), "finger", "1");
                } else {
                    Local.setData("acceso", getContext(), "finger", "0");
                }
            }
        });

//        switchTheme = view.findViewById(R.id.switchTheme);
//        if (Local.getData("theme", getContext(), "tema").equals("1")) {
//            switchTheme.setChecked(true);
//        } else {
//            switchTheme.setChecked(false);
//        }
//        switchTheme.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (switchTheme.isChecked()) {
//                    Local.setData("theme", getContext(), "tema", "1");
//                } else {
//                    Local.setData("theme", getContext(), "tema", "0");
//                }
//                Intent intent = new Intent(getActivity(), MainActivity.class);
//                startActivity(intent);
//            }
//        });
    }

    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }
}
