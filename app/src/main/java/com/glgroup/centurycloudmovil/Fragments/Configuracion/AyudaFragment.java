package com.glgroup.centurycloudmovil.Fragments.Configuracion;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.aakira.expandablelayout.Utils;
import com.glgroup.centurycloudmovil.Controllers.Adapters.RecyclerViewAdapter;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Models.ItemModel;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AyudaFragment extends Fragment implements MainActivity.IOnBackPressed {

    public AyudaFragment() {
        // Required empty public constructor
    }

    /** actividad donde se encuentra el lector **/
    private MainActivity mContext;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_ayuda, container, false);
        ayudaFunciones();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
    }

    /** permite controlar la accion del botón de retroceso **/
    @Override
    public boolean onBackPressed() {
        if (true) {
            mContext.getSupportActionBar().setTitle("");
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new DefaultFragment())
                    .commit();
            return true;
        } else {
            return false;
        }
    }

    public void ayudaFunciones(){
        final RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),0));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        final List<ItemModel> data = new ArrayList<>();
        data.add(new ItemModel(
                getString(R.string.enlazar),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), getString(R.string.ayuda_enlace_unico)+"\n\n"+getString(R.string.ayuda_enlace_masivo)));

        data.add(new ItemModel(
                getString(R.string.desenlace),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), getString(R.string.ayuda_desenlace_unico)+"\n\n"+getString(R.string.ayuda_desenlace_masivo)));

        data.add(new ItemModel(
                getString(R.string.inventario),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), getString(R.string.ayuda_inventario_1)+"\n\n"+
                getString(R.string.ayuda_inventario_2)+"\n\n"+getString(R.string.ayuda_inventario_3)+"\n\n"+getString(R.string.ayuda_inventario_4)+"\n\n"+
                getString(R.string.ayuda_inventario_5)+"\n"));

        data.add(new ItemModel(
                getString(R.string.busqueda),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), "Ayuda de búsquedas"));

        data.add(new ItemModel(
                getString(R.string.buscar_dispositivo),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), getString(R.string.ayuda_buscar_dispositivo)+"\n"));

        data.add(new ItemModel(
                getString(R.string.establecer_conexion),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), getString(R.string.ayuda_establecer_conexion)+"\n"));

        data.add(new ItemModel(
                getString(R.string.desconectar_dispositivo),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), getString(R.string.ayuda_desconectar_dispositivo)+"\n"));

        data.add(new ItemModel(
                getString(R.string.no_detecta_dispositivo),
                R.color.Blanco,
                R.color.Blanco,
                Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR), getString(R.string.ayuda_no_se_detecta_dispositivo)+"\n\n"
                +getString(R.string.ayuda_no_se_detecta_dispositivo1)+"\n"+getString(R.string.ayuda_no_se_detecta_dispositivo2)+"\n"+getString(R.string.ayuda_no_se_detecta_dispositivo3)
                +"\n"+getString(R.string.ayuda_no_se_detecta_dispositivo4)+"\n"));

        recyclerView.setAdapter(new RecyclerViewAdapter(data));

    }

}
