package com.glgroup.centurycloudmovil.Fragments.Movimiento;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Adapters.AdaptadorResulMovimiento;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.Inventario.InventarioFragment;
import com.glgroup.centurycloudmovil.Fragments.Inventario.ListaInventariosFragment;
import com.glgroup.centurycloudmovil.Models.Application.LocacionesInOut.LocacionInOut;
import com.glgroup.centurycloudmovil.Models.Application.LocacionesInOut.ResLocacion;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.ListaRfidLeidosMov;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.ResListaRfidLeidosMov;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes.ResCliente;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Filtro;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LecturaMovimientoFragment extends Fragment implements MainActivity.IOnBackPressed, View.OnClickListener{
    /** vista del fragment **/
    private View view;

    /** peticiones retrofit **/
    private Call<LocacionInOut> reqLocaciones;
    private Call<ListaRfidLeidosMov> reqListaLeidosMov;

    /** respuesta retrofit **/
    private LocacionInOut resLocaciones;
    private ListaRfidLeidosMov resListaLeidosMov;

    /** areas disponibles **/
    private ArrayList<String> areas = new ArrayList<>();
    private String movimiento = "";

    /** elementos de la vista **/
    private CardView limpiar, leer, procesar;
    private TextView contador;
    int cont = 0;
    private ImageView imgLectura;
    private TextView textLectura, txt_movimiento, txt_area_orides;
    private Spinner spinnerAreasOriDes;

    /** variable de lectura **/
    boolean isRuning = false;
    private boolean loopFlag = false;
    private long mStrTime;

    /** lista de etiquetas leidas **/
    private Map<String, String> etiquetasLeidas = new HashMap<String, String>();


    /** actividad donde se encuentra el lector **/
    private MainActivity mContext;

    /**
     * variables de datos
     **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;
    private String area;
    private String locacionOriDes;

    private Timer mTimer = new Timer();
    private TimerTask mInventoryPerMinuteTask;


    /** clase para la conexión de dispositivos **/
    private ConnectStatus mConnectStatus = new ConnectStatus();

    /** ventana de carga **/
    private AlertCarga alertValidar;


    /** se controla si el dispositivo se encuentra conectado o no **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Toast.makeText(getContext(),"Conectado en fragment", Toast.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
                //Toast.makeText(getContext(),"Desconectado en fragment", Toast.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }


    @SuppressLint("HandlerLeak")
    private
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:

                    break;
                case Constantes.FLAG_START:

                    break;
                case Constantes.FLAG_UPDATE_TIME:
//                    float useTime = (System.currentTimeMillis() - mStrTime) / 1000.0F;
//                    tv_time.setText(NumberTool.getPointDouble(loopFlag ? 1 : 3, useTime) + "s");
                    break;
                case Constantes.FLAG_UHFINFO:
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    agregarEtiquetas(info.getEPC());
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lectura_movimiento, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        init();
        cargarAreas();
        mContext.addConnectStatusNotice(mConnectStatus);

        /** acción recibida desde el lector **/
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {
                if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if(loopFlag) {
                        iniciar();
                        stopInventory();
                    } else {
                        detener();
                        startThread();
                    }
                }
            }
        });
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        if(loopFlag){
            iniciar();
            stopInventory();
        }

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new MovimientoFragment())
                .commit();
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }

    /** inicialización de los componentes **/
    public void init(){

        alertValidar = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alertValidar.alertCarga();

        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
        area = Local.getData("login", getContext(), "idArea");
        locacionOriDes = Local.getData("movimiento", getContext(), "idLocacion");

        limpiar = view.findViewById(R.id.btnClear);
        limpiar.setOnClickListener(this);

        leer = view.findViewById(R.id.btnLectura);
        leer.setOnClickListener(this);

        procesar = view.findViewById(R.id.btnSend);
        procesar.setOnClickListener(this);

        contador = view.findViewById(R.id.lbl_contador);

        spinnerAreasOriDes = view.findViewById(R.id.spinnerAreasOriDes);

        imgLectura = view.findViewById(R.id.imgLectura);
        textLectura = view.findViewById(R.id.textLectura);

        txt_movimiento = view.findViewById(R.id.txt_movimiento);

        txt_area_orides = view.findViewById(R.id.txt_area_orides);
        if(Local.getData("movimiento", getContext(), "tipo").equals("1")){
            txt_area_orides.setText(getString(R.string.area_origen));
            txt_movimiento.setText(getString(R.string.origen)+": "+Local.getData("locEntrada", getContext(), "descripcion"));
            movimiento = "1";
        }else{
            txt_area_orides.setText(getString(R.string.area_destino));
            txt_movimiento.setText(getString(R.string.destino)+": "+Local.getData("locSalida", getContext(), "descripcion"));
            movimiento = "2";
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btClear:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                etiquetasLeidas.clear();
                contador.setText("0");
                break;

            case R.id.btnLectura:
                /** se comprueba su el dispositivo está conectado **/
                if(mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if (loopFlag) {
                        iniciar();
                        stopInventory();
                    } else {
                        detener();
                        startThread();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.btnSend:
                if(!contador.getText().toString().equals("0")){
                    if (loopFlag) {
                        iniciar();
                        stopInventory();
                    }
                    obtenerListaLeidos(Filtro.Filtro(etiquetasLeidas));
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                }

                break;
        }
    }

    /** cambiar color de botones **/
    public void iniciar(){
        leer.setCardBackgroundColor(Color.parseColor("#003D70"));
        imgLectura.setImageResource(R.drawable.ic_play);
        textLectura.setText(getString(R.string.iniciar));
    }

    public void detener(){
        leer.setCardBackgroundColor(Color.parseColor("#FFB71C1C"));
        imgLectura.setImageResource(R.drawable.ic_pause);
        textLectura.setText(getString(R.string.detener));
    }

    /** agregar las etiquetas a la lista **/
    public void agregarEtiquetas(String epc){
        if(!etiquetasLeidas.containsKey(epc)) {
            etiquetasLeidas.put(epc,epc);
            if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(1);}
            contador.setText(String.valueOf(etiquetasLeidas.size()));
        }
    }

    /** SDK **/
    public void startThread() {
        if(mContext.uhf.getPower() != 30){
            mContext.uhf.setPower(30);
        }
        if (isRuning) {
            return;
        }
        isRuning = true;
        // cbFilter.setChecked(false);
        new TagThread().start();
    }

    class TagThread extends Thread {
        public void run() {
            Message msg = handler.obtainMessage(Constantes.FLAG_START);
            if (mContext.uhf.startInventoryTag()) {
                loopFlag = true;
                mContext.isScanning = true;
                mStrTime = System.currentTimeMillis();
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            handler.sendMessage(msg);
            isRuning = false;//执行完成设置成false
            while (loopFlag) {
                getUHFInfo();
                handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
            }
            stopInventory();
        }
    }

    /** obtiene información de la etiqueta y la envía al handler **/
    private void getUHFInfo() {
        List<UHFTAGInfo> list = mContext.uhf.readTagFromBufferList_EpcTidUser();
        if (list != null && !list.isEmpty()) {
            for (int k = 0; k < list.size(); k++) {
                Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO, list.get(k));
                handler.sendMessage(msg);
                if(!loopFlag) {
                    break;
                }
            }
        }
    }

    /** se detiene el proceso de inventario **/
    private void stopInventory() {
        loopFlag = false;
        cancelInventoryTask();
        boolean result = mContext.uhf.stopInventory();
        if(mContext.isScanning) {
            ConnectionStatus connectionStatus = mContext.uhf.getConnectStatus();
            Message msg = handler.obtainMessage(Constantes.FLAG_STOP);
            if (result || connectionStatus == ConnectionStatus.DISCONNECTED) {
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
                //getUHFInfoEx();
            }
            mContext.isScanning = false;
            handler.sendMessage(msg);
        }
    }

    /** la verdad nosé que hace **/
    private void cancelInventoryTask() {
        if(mInventoryPerMinuteTask != null) {
            mInventoryPerMinuteTask.cancel();
            mInventoryPerMinuteTask = null;
        }
    }

    /** cargar areas origen - destino **/
    public void cargarAreas(){

        reqLocaciones = ApiService.getApiService(getContext()).getlocacionesinout(usuario,cliente,proyecto,locacion,"19", movimiento, token);
        reqLocaciones.enqueue(new Callback<LocacionInOut>() {
            @Override
            public void onResponse(Call<LocacionInOut> call, Response<LocacionInOut> response) {
                if(response.isSuccessful()){
                    resLocaciones = response.body();
                    areas.clear();
                    int validar = 0;
                    for(ResLocacion data : resLocaciones.getDatos().getResultado()){
                        validar = data.getIdOpcion();
                        areas.add(data.getOpcion());
                    }
                    if(validar > 0){
                        mostrarAreas();
                    }else{
                        Toasty.error(getContext(), getString(R.string.no_areas), Toasty.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LocacionInOut> call, Throwable t) {

            }
        });
    }

    /** mostrar areas origen - destino **/
    public void mostrarAreas(){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, areas);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAreasOriDes.setAdapter(adaptadorCliente);
        spinnerAreasOriDes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                for(ResLocacion data : resLocaciones.getDatos().getResultado()){
                    if(data.getOpcion().equals(item)){
                        Local.setData("movimiento", getContext(), "idArea", String.valueOf(data.getIdOpcion()));
                    }
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //  Toast.makeText(getApplicationContext(),"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /** obtener datos lectura **/
    public void obtenerListaLeidos(final String etiquetas){
        alertValidar.alertShow();
        reqListaLeidosMov = ApiService.getApiService(getContext()).getlistaleidosmov(usuario,cliente,proyecto,locacion,area,movimiento,locacionOriDes,Local.getData("movimiento", getContext(), "idArea"),etiquetas, Constantes.IMEI,token);
        reqListaLeidosMov.enqueue(new Callback<ListaRfidLeidosMov>() {
            @Override
            public void onResponse(Call<ListaRfidLeidosMov> call, Response<ListaRfidLeidosMov> response) {
                if(response.isSuccessful()){
                    resListaLeidosMov = response.body();
                    int validar = 0;
                    String mensaje = "";
                    for(ResListaRfidLeidosMov data : resListaLeidosMov.getDatos().getResultado()){
                        validar = data.getIdItem();
                        mensaje = data.getDescripcion();
                        if(validar == -1){
                            break;
                        }

                    }
                    alertValidar.alerDismiss();
                    if(validar >= 0){
                        Local.setData("movimiento", getContext(), "etiquetas", etiquetas);
                        Local.setData("movimiento", getContext(), "cantLeida", contador.getText().toString());
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_frame, new ResultadoMovimientoFragment())
                                .commit();
                    }else{
                        Alertas.errorCierre(mensaje, getContext());
                    }
                }else{
                    alertValidar.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<ListaRfidLeidosMov> call, Throwable t) {
                alertValidar.alerDismiss();
            }
        });
    }

}