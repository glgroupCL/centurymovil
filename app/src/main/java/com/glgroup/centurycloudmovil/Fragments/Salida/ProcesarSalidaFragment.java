package com.glgroup.centurycloudmovil.Fragments.Salida;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Controllers.Adapters.AdaptadorObsLecturaRfid;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.EnvioListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ListaRfidDoc;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ResEnvioListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ResListaRfidDoc;
import com.glgroup.centurycloudmovil.Models.Application.PermisoUsuario.Permiso;
import com.glgroup.centurycloudmovil.Models.Application.PermisoUsuario.ResPermiso;
import com.glgroup.centurycloudmovil.Models.Datos.ObsLecturaRfid;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Security.Hash;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProcesarSalidaFragment extends Fragment implements View.OnClickListener, MainActivity.IOnBackPressed{

    /**
     * vista y contexto
     **/
    private MainActivity mContext;
    private View view;

    /**
     * peticion retrofit
     **/
    private Call<ListaRfidDoc> reqListaRfidDoc;
    private Call<Permiso> reqPermiso;
    private Call<EnvioListaRfid> reqEnvioListaRfid;

    /**
     * respuesta retrofit
     **/
    private ListaRfidDoc resListarfidDoc;
    private Permiso resPermiso;
    private EnvioListaRfid resEnvioListaRfid;

    /**
     * variables de datos
     **/
    private String token;
    private String usuario;
    private String cliente;
    private String proyecto;
    private String locacion;
    private String area;
    private String numeroDocumento, idProveedor, idDocumento, tipoDocumento;
    private String etiquetas;

    /** elementos de la vista **/
    private TextView txt_title, txt_descripcion_doc, txt_efectividad, txt_valor_numero, txt_lectura;
    private CardView card_clean, card_obs, card_send;

    /** adaptador observacion lectura **/
    private AdaptadorObsLecturaRfid mAdapterObs;
    private ArrayList<ObsLecturaRfid> listaDatos = new ArrayList<>();


    /** ventana de carga **/
    private AlertCarga alert, alertValidar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_procesar_salida, container, false);
        init();
        obtenerResutado();
        return view;
    }

    public void init() {
        alert = new AlertCarga(getString(R.string.procesando_lectura), getActivity(), getLayoutInflater());
        alert.alertCarga();

        alertValidar = new AlertCarga(getString(R.string.validando_datos), getActivity(), getLayoutInflater());
        alertValidar.alertCarga();


        Local.setData("docSalida",getContext(),"id_usuario_envio", Local.getData("login", getContext(), "idUsuario"));
        token = Local.getData("login", getContext(), "token");
        usuario = Local.getData("login", getContext(), "idUsuario");
        cliente = Local.getData("login", getContext(), "idCLiente");
        proyecto = Local.getData("login", getContext(), "idProyecto");
        locacion = Local.getData("login", getContext(), "idLocacion");
        area = Local.getData("login", getContext(), "idArea");
        numeroDocumento = Local.getData("docSalida", getContext(), "numero_documento");
        idProveedor = Local.getData("docSalida", getContext(), "id_proveedor");
        idDocumento = Local.getData("docSalida", getContext(), "id_documento");
        tipoDocumento = Local.getData("docSalida", getContext(), "tipo_documento");
        etiquetas = Local.getData("docSalida", getContext(), "etiquetas");

        txt_title = view.findViewById(R.id.txt_title);

        txt_descripcion_doc = view.findViewById(R.id.descripcion_documento);
        txt_descripcion_doc.setText(Local.getData("docSalida", getContext(), "descripcion"));

        txt_efectividad = view.findViewById(R.id.efectividad);
        txt_valor_numero = view.findViewById(R.id.valor_numero);
        txt_lectura = view.findViewById(R.id.txt_lectura);

        card_clean = view.findViewById(R.id.card_clean);
        card_clean.setOnClickListener(this);

        card_obs = view.findViewById(R.id.card_obs);
        card_obs.setOnClickListener(this);

        card_send = view.findViewById(R.id.card_send);
        card_send.setOnClickListener(this);
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new LecturaSalidaFragment())
                .commit();

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_clean:
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new LecturaSalidaFragment())
                        .commit();
                break;

            case R.id.card_obs:
                /** si el valor es 1, está autorizado, sino debe solicitar credenciales **/
                if(Local.getData("btnSalida", getContext(), "btnObservaciones").equals("1")){
                    verObsLecturaRfid();
                }else{
                    solicitarCredenciales();
                }
                break;

            case R.id.card_send:
                /** si el valor es 1, está autorizado, sino debe solicitar credenciales **/
                if(Local.getData("btnSalida", getContext(), "btnEnviarLectura").equals("1")){
                    alertobservacion();
                }else{
                    solicitarCredenciales();
                }
                break;
        }
    }

    /** se obtiene el resultado de la lectura de salida **/
    public void obtenerResutado() {
        alert.alertShow();
        reqListaRfidDoc = ApiService.getApiService(getContext()).getlistarfidinout(usuario, cliente, proyecto, locacion, area, idDocumento, tipoDocumento, numeroDocumento, idProveedor, etiquetas, token);
        reqListaRfidDoc.enqueue(new Callback<ListaRfidDoc>() {
            @Override
            public void onResponse(Call<ListaRfidDoc> call, Response<ListaRfidDoc> response) {
                if(response.isSuccessful()){
                    listaDatos.clear();
                    resListarfidDoc = response.body();
                    for(ResListaRfidDoc data : resListarfidDoc.getDatos().getResultado()){

                        /** permisos para los botones de la vista **/
                        if(data.getIdSegmento() == 0){
                            /** observaciones **/
                            if(data.getValor1() == 2){
                                Local.setData("btnSalida", getContext(), "btnObservaciones", (data.getIntValor1() == 1)? "1":"0");
                            }
                            /** detalles de lectura **/
                            if(data.getValor1() == 3){
                                Local.setData("btnSalida", getContext(), "btnDetalle", (data.getIntValor1() == 1)? "1":"0");
                            }
                            /** enviar la lectura **/
                            if(data.getValor1() == 4){
                                Local.setData("btnSalida", getContext(), "btnEnviarLectura", (data.getIntValor1() == 1)? "1":"0");
                            }
                        }

                        /** mensajes de la vista **/
                        if(data.getIdSegmento() == 1){

                            /** % de efectividad, si valor1 == 1, el color de texto es rojo, caso contrario es verde **/
                            if(data.getValor1() == 1){
                                txt_efectividad.setTextColor(Color.parseColor("#FF0000"));
                            }else{
                                txt_efectividad.setTextColor(Color.parseColor("#148B0B"));
                            }
                            txt_efectividad.setText(data.getTxtValor1());

                            /** valor bajo el % de efectividad, si valor1 == 1, el color de texto es rojo, caso contrario es verde **/
                            if(data.getValor2() == 1){
                                txt_valor_numero.setTextColor(Color.parseColor("#FF0000"));
                            }else{
                                txt_valor_numero.setTextColor(Color.parseColor("#148B0B"));
                            }
                            Local.setData("docSalida", getContext(),"colorIndicador", String.valueOf(data.getValor2()));
                            txt_valor_numero.setText(data.getTxtValor2());

                            /** mensaje sobre la lectura **/
                            txt_lectura.setText(data.getTxtValor3());
                        }
                        if(data.getIdSegmento() == 2){
                            listaDatos.add(new ObsLecturaRfid(data.getTxtValor1(), data.getStrValor1(), String.valueOf(data.getValor1())));
                        }
                    }
                    alert.alerDismiss();
                }else{
                    alert.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<ListaRfidDoc> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }

    /** muestra las observaciones de la lectura rfid **/
    public void verObsLecturaRfid(){
        /** constructores de alerta **/
        final AlertDialog alertObsLectura;
        AlertDialog.Builder obsLectura = new AlertDialog.Builder(getContext());

        /** vista del alert **/
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_obs_lectura_rfid, null);

        ListView listaItems = view.findViewById(R.id.listaItems);
        mAdapterObs = new AdaptadorObsLecturaRfid(getContext(), listaDatos);
        listaItems.setAdapter(mAdapterObs);

        Button volver = view.findViewById(R.id.btn_regresar);
        Button detalle = view.findViewById(R.id.btn_detalle);

        obsLectura.setView(view);
        alertObsLectura = obsLectura.create();
        alertObsLectura.setCancelable(true);

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertObsLectura.dismiss();
            }
        });

        detalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertObsLectura.dismiss();
                if(Local.getData("btnSalida", getContext(), "btnDetalle").equals("1")) {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_frame, new DetalleLecturaSalidaFragment())
                            .commit();
                }else{
                    solicitarCredenciales();
                }
            }
        });

        alertObsLectura.show();
    }
    /** solicitar permisos administrador **/
    public void solicitarCredenciales(){
        /** constructores de alerta **/
        final AlertDialog alertCredencial;
        AlertDialog.Builder credenciales = new AlertDialog.Builder(getContext());

        /** vista del alert **/
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_login_admin, null);

        final TextInputLayout usuario = view.findViewById(R.id.edUser);
        final TextInputLayout clave = view.findViewById(R.id.edPass);

        Button volver = view.findViewById(R.id.btn_regresar);
        final Button validar = view.findViewById(R.id.btn_validar);

        credenciales.setView(view);
        alertCredencial = credenciales.create();
        alertCredencial.setCancelable(true);

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCredencial.dismiss();
            }
        });

        validar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean valido = true;
                if(usuario.getEditText().getText().toString().equals("")){
                    usuario.setErrorEnabled(true);
                    usuario.setError(getString(R.string.requerido));
                    valido = false;
                }else{
                    usuario.setError(null);
                }
                if(clave.getEditText().getText().toString().equals("")){
                    clave.setErrorEnabled(true);
                    clave.setError(getString(R.string.requerido));
                    valido = false;
                }else{
                    clave.setError(null);
                }
                if(valido){
                    alertCredencial.dismiss();
                    validarPermiso(usuario.getEditText().getText().toString(), Hash.md5(clave.getEditText().getText().toString()));
                }
            }
        });

        alertCredencial.show();
    }

    /** validar permisos administrador **/
    public void validarPermiso(String usuario, String clave){
        alertValidar.alertShow();
        reqPermiso = ApiService.getApiService(getContext()).getusuarioadmin(usuario, clave, Constantes.IMEI,"0", "1", token);
        reqPermiso.enqueue(new Callback<Permiso>() {
            @Override
            public void onResponse(Call<Permiso> call, Response<Permiso> response) {
                if(response.isSuccessful()){
                    resPermiso = response.body();
                    int validar = 0;
                    String mensaje = "";
                    for(ResPermiso data : resPermiso.getDatos().getResultado()){
                        validar = data.getIdPermiso();
                        mensaje = data.getDescripcion();
                        if(validar > 0){
                            /** observaciones **/
                            if(validar == 2){
                                Local.setData("btnSalida", getContext(), "btnObservaciones", String.valueOf(data.getPermiso()));
                            }
                            /** Detalle **/
                            if(validar == 3){
                                Local.setData("btnSalida", getContext(), "btnDetalle", String.valueOf(data.getPermiso()));
                            }
                            /** Enviar **/
                            if(validar == 4){
                                Local.setData("btnSalida", getContext(), "btnEnviarLectura", String.valueOf(data.getPermiso()));
                            }
                            Local.setData("docSalida",getContext(),"id_usuario_envio", String.valueOf(data.getIdUsuario()));

                        }
                    }
                    alertValidar.alerDismiss();
                    if(validar > 0){
                        Toasty.success(getContext(), getString(R.string.usuario_valido), Toasty.LENGTH_SHORT).show();
                    }else{
                        Toasty.warning(getContext(), mensaje, Toasty.LENGTH_SHORT).show();
                    }
                }else{
                    alertValidar.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<Permiso> call, Throwable t) {
                alertValidar.alerDismiss();
            }
        });
    }

    /** Alert para solicitar observaciones al cerrar la lectura **/
    public void alertobservacion(){
        final android.app.AlertDialog alertObservacion;
        final android.app.AlertDialog.Builder finalizar = new android.app.AlertDialog.Builder(getContext());


        View dialog = getLayoutInflater().inflate(R.layout.alert_observacion, null);

        final Button ok = dialog.findViewById(R.id.btnCerrar);
        final TextInputLayout observacion = dialog.findViewById(R.id.edObservacion);

        finalizar.setView(dialog);

        alertObservacion = finalizar.create();
        alertObservacion.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertObservacion.dismiss();
                enviarLectura(observacion.getEditText().getText().toString());
            }
        });
        alertObservacion.show();
    }

    /**envío de lectura **/
    public void enviarLectura(String observacion){
        alert.alertShow();
        reqEnvioListaRfid = ApiService.getApiService(getContext()).getenviorfidinout(usuario,cliente,proyecto,locacion,area,idDocumento,tipoDocumento,numeroDocumento,idProveedor,etiquetas,observacion,"0", Constantes.IMEI,token);
        reqEnvioListaRfid.enqueue(new Callback<EnvioListaRfid>() {
            @Override
            public void onResponse(Call<EnvioListaRfid> call, Response<EnvioListaRfid> response) {
                if(response.isSuccessful()){
                    resEnvioListaRfid = response.body();
                    int validar = 0;
                    String mensaje = "";
                    ResEnvioListaRfid content = null;
                    for(ResEnvioListaRfid data : resEnvioListaRfid.getDatos().getResultado()){
                        validar = data.getIdMsg();
                        content = data;
                    }
                    alert.alerDismiss();
                    if(validar > 0){
                        if(content != null && content.getIdMsg() == 1){
                            Alertas.alertCorrecto(getString(R.string.aviso), (content.getMsg1()==null ? "":content.getMsg1()), getContext(), getLayoutInflater(), mContext);
                        }else{
                            Alertas.alertIncorrecto((content.getMsg1()==null ? "":content.getMsg1())+"\n"+(content.getMsg2()==null ? "":content.getMsg2()), (content.getMsg3()==null ? "":content.getMsg3()), getContext(), getLayoutInflater(), mContext);
                        }
                    }else{
                        Alertas.errorCierre(content.getMsg1(), getContext());
                    }
                }else{
                    alert.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<EnvioListaRfid> call, Throwable t) {
                alert.alerDismiss();
            }
        });

    }

//    public void alertCorrecto(String tit, String msg){
//        final android.app.AlertDialog alertCorrecto;
//        final android.app.AlertDialog.Builder correcto = new android.app.AlertDialog.Builder(getContext());
//
//
//        View dialog = getLayoutInflater().inflate(R.layout.alert_cierrecorrecto_doc, null);
//
//        final Button ok = dialog.findViewById(R.id.btn_send);
//        final TextView titulo = dialog.findViewById(R.id.txt_titulo);
//        final TextView mensaje = dialog.findViewById(R.id.mensaje);
//
//        titulo.setText(tit);
//        mensaje.setText(msg);
//
//        correcto.setView(dialog);
//
//        alertCorrecto = correcto.create();
//        alertCorrecto.setCancelable(true);
//
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertCorrecto.dismiss();
//                getActivity().getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.content_frame, new DefaultFragment())
//                        .commit();
//            }
//        });
//        alertCorrecto.show();
//    }

//    public void alertIncorrecto(String tit, String msg){
//        final android.app.AlertDialog alertIncorrecto;
//        final android.app.AlertDialog.Builder incorrecto = new android.app.AlertDialog.Builder(getContext());
//
//
//        View dialog = getLayoutInflater().inflate(R.layout.alert_cierreincorrecto_doc, null);
//
//        final Button ok = dialog.findViewById(R.id.btn_send);
//        final Button volver = dialog.findViewById(R.id.btn_regresar);
//        final TextView titulo = dialog.findViewById(R.id.txt_titulo);
//        final TextView mensaje = dialog.findViewById(R.id.mensaje);
//
//        titulo.setText(tit);
//        mensaje.setText(msg);
//
//        incorrecto.setView(dialog);
//
//        alertIncorrecto = incorrecto.create();
//        alertIncorrecto.setCancelable(true);
//
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertIncorrecto.dismiss();
//                getActivity().getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.content_frame, new DefaultFragment())
//                        .commit();
//            }
//        });
//
//        volver.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertIncorrecto.dismiss();
//            }
//        });
//
//        alertIncorrecto.show();
//
//    }

}