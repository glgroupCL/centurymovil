package com.glgroup.centurycloudmovil.Fragments.Entrada;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Filtro;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class LecturaEntradaFragment extends Fragment implements MainActivity.IOnBackPressed, View.OnClickListener{

    /** vista y conexto del fragment **/
    private MainActivity mContext;
    private View view;

    /** elementos de la vista **/
    private TextView descripcion_documento;
    private CardView leer, pausar, card_clean, card_obs, card_process;
    private TextView contador;

    /** lista de etiquetas leidas **/
    private Map<String, String> etiquetasLeidas = new HashMap<String, String>();

    /** variable de lectura **/
    boolean isRuning = false;
    private boolean loopFlag = false;

    /** clase para la conexión de dispositivos **/
    private ConnectStatus mConnectStatus = new ConnectStatus();


    /** se controla si el dispositivo se encuentra conectado o no **/
    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(ConnectionStatus connectionStatus) {
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Toast.makeText(getContext(),"Conectado en fragment", Toast.LENGTH_LONG).show();
                    //TODO mostrar o cambiar botones de lectura?
                }

            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                loopFlag = false;
                mContext.isScanning = false;
                //Toast.makeText(getContext(),"Desconectado en fragment", Toast.LENGTH_LONG).show();
                //TODO ocultar o cambiar botones de lectura?
            }
        }
    }


    /**
     * hilo para recibir datos de lectura
     **/
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        /** error al detener la lectura **/
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                        Toast.makeText(mContext, "uhf_msg_inventory_stop_fail", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constantes.FLAG_START:
                    if (msg.arg1 == Constantes.FLAG_SUCCESS) {
                        /** nose que pasa aquí, pero si funciona no lo toques **/
                    } else {
                        if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(2);}
                    }

                    break;

                case Constantes.FLAG_UHFINFO:
                    /** se reciben los datos de las etiquetas **/
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    if (!TextUtils.isEmpty(info.getEPC())) {
                        agregarEtiquetas(info.getEPC());
                    }

                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lectura_entrada, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        init();
        mContext.addConnectStatusNotice(mConnectStatus);

        /** acción recibida desde el lector **/
        mContext.uhf.setKeyEventCallback(new KeyEventCallback() {
            @Override
            public void onKeyDown(int keycode) {
                if (mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    lectura();
                }
            }
        });
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        if(loopFlag) {
            iniciar();
            stopInventory();
        }
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DocumentosEntradaFragment())
                .commit();

        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loopFlag) {
            iniciar();
            stopInventory();
        }
    }

    /** inicio de los elementos **/
    public void init(){
        descripcion_documento = view.findViewById(R.id.descripcion_documento);
        descripcion_documento.setText(Local.getData("docEntrada", getContext(), "descripcion"));

        leer = view.findViewById(R.id.leer);
        leer.setOnClickListener(this);

        pausar = view.findViewById(R.id.pausar);
        pausar.setOnClickListener(this);

        card_clean = view.findViewById(R.id.card_clean);
        card_clean.setOnClickListener(this);

        card_obs = view.findViewById(R.id.card_obs);
        card_obs.setOnClickListener(this);

        card_process = view.findViewById(R.id.card_process);
        card_process.setOnClickListener(this);

        contador = view.findViewById(R.id.lbl_contador);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.leer:
                /** se comprueba su el dispositivo está conectado **/
                if(mContext.uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if (!loopFlag) {
                        detener();
                        startThread();
                    }
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.pausar:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                break;


            case R.id.card_clean:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                limpiar();
                break;

            case R.id.card_obs:
                Alertas.verObservación(Local.getData("docEntrada", getContext(), "observacion"),getContext());
                break;

            case R.id.card_process:
                if (loopFlag) {
                    iniciar();
                    stopInventory();
                }
                if(etiquetasLeidas.size() > 0) {
                    Local.setData("docEntrada", getContext(), "etiquetas", Filtro.Filtro(etiquetasLeidas));
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_frame, new ProcesarEntradaFragment())
                            .commit();
                }else{
                    Toasty.warning(getContext(), getString(R.string.no_etiquetas), Toasty.LENGTH_LONG).show();
                }
                break;
        }

    }

    /** se define si detener o comenzar la lectura **/
    public void lectura(){
        if (!loopFlag) {
            if(!mContext.isScanning) {
                detener();
                startThread();
            }else{
                Toasty.warning(getContext(), getString(R.string.proceso_de_lectura), Toasty.LENGTH_LONG).show();
            }
        }else{
            iniciar();
            stopInventory();
        }
    }

    public void iniciar(){
        leer.setVisibility(View.VISIBLE);
        pausar.setVisibility(View.GONE);
    }

    public void detener(){
        leer.setVisibility(View.GONE);
        pausar.setVisibility(View.VISIBLE);
    }

    /** limpiar elementos **/
    public void limpiar(){
        contador.setText("0");
        etiquetasLeidas.clear();
    }

    /** SDK **/
    public void startThread() {
        if(mContext.uhf.getPower() != 15){
            mContext.uhf.setPower(15);
        }
        if (isRuning) {
            return;
        }
        isRuning = true;
        // cbFilter.setChecked(false);
        new TagThread().start();
    }

    /** proceso de lectura rfid **/
    class TagThread extends Thread {
        public void run() {
            Message msg = handler.obtainMessage(Constantes.FLAG_START);
            if (mContext.uhf.startInventoryTag()) {
                loopFlag = true;
                mContext.isScanning = true;
                //mStrTime = System.currentTimeMillis();
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            handler.sendMessage(msg);
            isRuning = false;//执行完成设置成false
            while (loopFlag) {
                getUHFInfo();
                handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
            }
            stopInventory();
        }
    }

    /** obtiene información de la etiqueta y la envía al handler **/
    private void getUHFInfo() {
        List<UHFTAGInfo> list = mContext.uhf.readTagFromBufferList_EpcTidUser();
        if (list != null && !list.isEmpty()) {
            for (int k = 0; k < list.size(); k++) {
                Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO, list.get(k));
                handler.sendMessage(msg);
                if(!loopFlag) {
                    break;
                }
            }
        }
    }

    /** se detiene el proceso de inventario **/
    private void stopInventory() {
        loopFlag = false;
        boolean result = mContext.uhf.stopInventory();
        if(mContext.isScanning) {
            ConnectionStatus connectionStatus = mContext.uhf.getConnectStatus();
            Message msg = handler.obtainMessage(Constantes.FLAG_STOP);
            if (result || connectionStatus == ConnectionStatus.DISCONNECTED) {
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
                //getUHFInfoEx();
            }
            mContext.isScanning = false;
            handler.sendMessage(msg);
        }
    }

    /** agregar las etiquetas a la lista **/
    public void agregarEtiquetas(String epc){
        if(!etiquetasLeidas.containsKey(epc)) {
            etiquetasLeidas.put(epc,epc);
            if(Local.getData("sonido",getContext(),"activado").equals("1") || Local.getData("sonido",getContext(),"activado").equals("")){Utils.playSound(1);}
            contador.setText(String.valueOf(etiquetasLeidas.size()));
        }
    }

}