package com.glgroup.centurycloudmovil.Fragments.Enlace;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glgroup.centurycloudmovil.Controllers.Adapters.viewPagerAdapter;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnlaceFragment extends Fragment implements MainActivity.IOnBackPressed{

    public EnlaceFragment() {
        // Required empty public constructor
    }


    MainActivity mContext;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_enlace, container, false);
       // cargaVistas();
        mContext = (MainActivity) getActivity();
        Alertas.alertEnlace(getContext(),getLayoutInflater(),mContext);
        return view;
    }

    /** permite controlar la acción del boton atrás **/
    @Override
    public boolean onBackPressed() {
        mContext.getSupportActionBar().setTitle("");
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new DefaultFragment())
                .commit();

        return true;
    }

    public void cargaVistas(){


        mContext = (MainActivity) getActivity();

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        ViewPager viewPager = view.findViewById(R.id.view_pager);

        viewPagerAdapter viewPagerAdapter = new viewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPagerAdapter.addFragment(new EnlaceUnicoFragment(), getString(R.string.unico));
        viewPagerAdapter.addFragment(new EnlaceMasivoFragment(), getString(R.string.masivo));

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int currentPage;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                Log.i("PAGER", String.valueOf(currentPage));//*** Aquí se obtiene el indice!
//                if(currentPage != Integer.valueOf(Local.getData("pager", getContext(), "pagerEnlace"))) {
//                    Local.setData("pager", getContext(), "pagerEnlace", String.valueOf(currentPage));
//                    if(mContext.isScanning) {
//                        mContext.uhf.stopInventory();
//                    }
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            public final int getCurrentPage() {
                return currentPage;
            }

        });
    }
}
