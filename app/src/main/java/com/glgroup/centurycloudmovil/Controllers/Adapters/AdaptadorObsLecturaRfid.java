package com.glgroup.centurycloudmovil.Controllers.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Models.Datos.ObsLecturaRfid;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;

import java.util.ArrayList;

public class AdaptadorObsLecturaRfid extends BaseAdapter {

    private Context context;
    private ArrayList<ObsLecturaRfid> listItems;

    public AdaptadorObsLecturaRfid(Context context, ArrayList<ObsLecturaRfid> listItems) {
        this.context = context;
        this.listItems = listItems;
    }
    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        //se obtiene el valor de la lista en la posición i, y si transorma a un elemento de la clase datoskpi
        ObsLecturaRfid item = (ObsLecturaRfid) getItem(i);

        //se crea una vista en base a un layout personalizado
        view = LayoutInflater.from(context).inflate(R.layout.lista_obs_lectura_rfid, null);

        //se obtienen los elementos del layout
        TextView dato = view.findViewById(R.id.dato);
        TextView valor = view.findViewById(R.id.valor);

        //se valida el id del campo para determinar el color del texto
        if(item.getId().equals("3")){
            //se obtiene el valor del indicador
            String indicador = Local.getData("docSalida", context,"colorIndicador");

            if(indicador.equals("1")){
                valor.setTextColor(Color.parseColor("#FF0000"));
            }else{
                valor.setTextColor(Color.parseColor("#148B0B"));
            }

        }
        //se asigna el valor correspondiente a los elementos de la vista
        dato.setText(item.getDato());
        valor.setText(item.getValor());

        return view;
    }
}
