package com.glgroup.centurycloudmovil.Controllers.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Models.Datos.DetalleLecturaRfid;
import com.glgroup.centurycloudmovil.R;

import java.util.ArrayList;

public class AdaptadorDetalleLecturaRfid extends BaseAdapter {

    private Context context;
    private ArrayList<DetalleLecturaRfid> listItems = new ArrayList<>();

    public AdaptadorDetalleLecturaRfid(Context context, ArrayList<DetalleLecturaRfid> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        //se obtienen los datos de la lista y se transforman a la estructura de la clase
        DetalleLecturaRfid item = (DetalleLecturaRfid) getItem(i);

        //se genera una vista en base a un layout personalizado
        view = LayoutInflater.from(context).inflate(R.layout.lista_detalle_lectura_rfid, null);

        //se obtienen los elementos de la vista del layout
        TextView valItem = view.findViewById(R.id.item);
        TextView cant_doc = view.findViewById(R.id.cant_dcto);
        TextView cant_read = view.findViewById(R.id.cant_read);
        TextView dif = view.findViewById(R.id.dif);

        //se asignan valores de la lista a los elementos de la vista
        valItem.setText(item.getItem());
        cant_doc.setText(item.getCant_dcto());
        cant_read.setText(item.getCant_read());
        dif.setText(item.getDiferencia());

        return view;
    }
}
