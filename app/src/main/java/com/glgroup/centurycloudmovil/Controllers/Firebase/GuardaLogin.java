package com.glgroup.centurycloudmovil.Controllers.Firebase;

import androidx.annotation.NonNull;

import com.glgroup.centurycloudmovil.Utils.Constantes;

import com.glgroup.centurycloudmovil.Models.Firebase.Login;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class GuardaLogin {

    /** almacena datos del login **/
    private Constantes constant;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference loginReference;


    public void almacena(String usuario, final String fecha){
        constant = new Constantes();

        loginReference = db.collection(constant.COLLECTION_LOGIN);

        final Login data = new Login(usuario.toLowerCase(), fecha);
        loginReference.whereEqualTo("usuario", usuario.toLowerCase())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        String idDoc = "";
                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            idDoc = documentSnapshot.getId();
                        }
                        if(idDoc.equals("")){
                            loginReference.add(data);
                        }else {
                            loginReference.document(idDoc)
                                    .update("fecha", fecha);
                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

    }


}
