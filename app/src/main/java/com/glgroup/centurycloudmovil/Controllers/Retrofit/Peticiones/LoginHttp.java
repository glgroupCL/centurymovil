package com.glgroup.centurycloudmovil.Controllers.Retrofit.Peticiones;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;

import com.glgroup.centurycloudmovil.Controllers.Firebase.GuardaLogin;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.Login;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Mensajes;
import com.glgroup.centurycloudmovil.Utils.Security.Hash;
import com.glgroup.centurycloudmovil.Utils.Security.JWTUtils;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.glgroup.centurycloudmovil.ui.UbicacionesActivity;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginHttp {


    public void logIn(final String user, String pass, String imei, final Activity context, LayoutInflater layoutInflater){

        /** ventana de carga **/
        final AlertCarga alert = new AlertCarga(context.getString(R.string.validando_datos),context, layoutInflater);
        alert.alertCarga();
        alert.alertShow();

        /** Consulta retrofit **/
        final Call<Login> reqLogin;
        final Login[] resLogin = new Login[1];

        /** constantes **/
        final Constantes constantes = new Constantes();

        final Mensajes mensajes = new Mensajes(context);
        reqLogin = ApiService.getApiService(context).getLogin(user.trim(), Hash.md5(pass.trim()), imei);
        reqLogin.enqueue(new Callback<com.glgroup.centurycloudmovil.Models.Application.Login>() {
            @Override
            public void onResponse(Call<com.glgroup.centurycloudmovil.Models.Application.Login> call, Response<com.glgroup.centurycloudmovil.Models.Application.Login> response) {
                if(response.isSuccessful()){

                    resLogin[0] = response.body();
                    if(resLogin[0].getOk() > 0){
                        if(JWTUtils.descifraToken(resLogin[0].getToken(), context) > 0){


                            /** guarda el ultimo inicio de sesion del usuario en firebase **/
                            GuardaLogin guardaLogin = new GuardaLogin();
                            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                            guardaLogin.almacena(user, date);

                            alert.alerDismiss();
                           validarEntrada(context);

                        }
                    }else{
                        alert.alerDismiss();
                        mensajes.Login(resLogin[0].getOk());
                    }
                }else{
                    /** error al ingresar datos inválidos **/
                    alert.alerDismiss();
                    if (response.code() == 400) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Log.d(constantes.LOGIN_CREDENTIAL, String.valueOf(jObjError));
                            mensajes.Login(jObjError.getInt("ok"));
                        } catch (Exception e) {
                            Log.d(constantes.LOGIN_CREDENTIAL, String.valueOf(e));
                            mensajes.Login(-5);
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                alert.alerDismiss();
               // Log.d(constantes.LOGIN_CREDENTIAL, String.valueOf(call));
                Log.d(constantes.LOGIN_CREDENTIAL, String.valueOf(t));
                if(String.valueOf(t).contains("Failed to connect to")){
                    Toasty.error(context,context.getString(R.string.error_conexion),Toasty.LENGTH_LONG).show();
                }
         //       mensajes.Login(-5);
            }
        });
    }

    public void validarEntrada(Context context){
        String cliente = Local.getData("login", context, "idCLiente");
        String proyecto = Local.getData("login", context, "idProyecto");
        String locacion = Local.getData("login", context, "idLocacion");
        String area = Local.getData("login", context, "idArea");
        if(cliente.equals("") || proyecto.equals("") || locacion.equals("") || area.equals("")){
            Intent myIntent = new Intent(context, UbicacionesActivity.class);
            context.startActivity(myIntent);
        }else{
            Intent myIntent = new Intent(context, MainActivity.class);
            context.startActivity(myIntent);
        }
    }
}
