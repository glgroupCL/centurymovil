package com.glgroup.centurycloudmovil.Controllers.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.glgroup.centurycloudmovil.Models.ListaInventario;
import com.glgroup.centurycloudmovil.R;

import java.util.ArrayList;


public class AdapterInventario extends BaseAdapter {

    private ArrayList<ListaInventario> listItem;
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(int position);

    }

    public AdapterInventario(ArrayList<ListaInventario> listItem, Context context) {
        this.listItem = listItem;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int i) {
        return listItem.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ListaInventario item = (ListaInventario) getItem(i);

        view = LayoutInflater.from(context).inflate(R.layout.list_double, null);
        TextView titulo = (TextView) view.findViewById(R.id.txt_titulo);
        TextView sub = (TextView) view.findViewById(R.id.txt_sub);

        titulo.setText(item.getTitulo());
        sub.setText(item.getSubtitulo());

        return view;
    }
}
