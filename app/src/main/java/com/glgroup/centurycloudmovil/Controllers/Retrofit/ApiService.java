package com.glgroup.centurycloudmovil.Controllers.Retrofit;

import android.content.Context;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.Interface.ApiServices;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {

    private static ApiServices API_SERVICE;

    /** se reinicia la conexión de firebase **/
    public static void reiniciarApiService(){
        API_SERVICE = null;
    }

    public static ApiServices getApiService(Context context) {
        // Creamos un interceptor y le indicamos el log level a usar
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        String baseUrl = Local.getData("conexion", context, "url")+":"+Local.getData("conexion", context, "puerto");
        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //tiempos de espera
        httpClient.connectTimeout(15, TimeUnit.SECONDS);
        httpClient.readTimeout(15, TimeUnit.SECONDS);
        httpClient.writeTimeout(15, TimeUnit.SECONDS);

        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build()) // <-- usamos el log level
                    .build();
            API_SERVICE = retrofit.create(ApiServices.class);
        }

        return API_SERVICE;
    }

}
