package com.glgroup.centurycloudmovil.Controllers.ROOMSQLite;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.Entity.PermisoBD;
import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.Interface.PermisoDao;

/** ejemplo
 * @Database(entities = {Permiso.class, otraclase.class, otra.class}, version = 1)
 */
@Database(entities = {PermisoBD.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    /** permisos de la tabla existentes en la interface **/
    @SuppressWarnings("WeakerAccess")
    public abstract PermisoDao permisoDao();


    /** manejador de base de datos **/
    private static AppDatabase sInstance;
}
