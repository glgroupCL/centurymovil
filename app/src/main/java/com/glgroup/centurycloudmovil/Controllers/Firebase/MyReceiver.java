package com.glgroup.centurycloudmovil.Controllers.Firebase;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;

import androidx.core.content.FileProvider;

import com.glgroup.centurycloudmovil.BuildConfig;

import java.io.File;

//clase que se conecta a firebase para realizar actualizaciones de la aplicación
//comprobar que existe el archivo google-service.json asociado al proyecto
public class MyReceiver extends BroadcastReceiver {

    DownloadManager myDownloadManager;
    long tamaño;
    IntentFilter myIntentFilter;

    Context contextApp;
    String name = "";

    private Context myContext;
    private Activity myActivity;

    public MyReceiver(Activity activity) {
        this.myContext = activity;
        this.myActivity =  activity;

        myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("El Evento_action", intent.getAction());
        String action = intent.getAction();

        if(DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)){
            intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);

            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(tamaño);

            Cursor cursor = myDownloadManager.query(query);
            if(cursor.moveToFirst()){
                int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if(DownloadManager.STATUS_SUCCESSFUL == cursor.getInt(columnIndex)){
                    String uriString = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    File file = new File(uriString);
                    System.out.println(file);

                    Intent pantallaInstall = new Intent(Intent.ACTION_VIEW);
                    pantallaInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        Uri photoURI = FileProvider.getUriForFile(myActivity,
                                //BuildConfig.APPLICATION_ID + ".provider", new File(file.toString().replace("file:/", "")));
                                BuildConfig.APPLICATION_ID + ".provider", file);
                        pantallaInstall.setDataAndType(photoURI, "application/vnd.android.package-archive");

                   // pantallaInstall.setDataAndType(Uri.parse(uriString), "application/vnd.android.package-archive");
                    myActivity.startActivity(pantallaInstall);

                    Log.e("Mensaje Descarga: ", "Se descargo sin problemas");
                }
            }
        }

    }

    public void Descargar(String urlUpdate, Context context){
       // String url = "https://firebasestorage.googleapis.com/v0/b/descargar-5f53c.appspot.com/o/app-debug.apk?alt=media&token=42777046-cc39-4976-9c42-166f5ba32b39";
        String url = urlUpdate;
        DownloadManager.Request myRequest;

        myDownloadManager = (DownloadManager) myContext.getSystemService(Context.DOWNLOAD_SERVICE);

        myRequest = new DownloadManager.Request(Uri.parse(url));
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(url);
        name = URLUtil.guessFileName(url, null, fileExtension);

        //crear carpeta
     //   File myFile = new File(context.	MediaStore.Downloads("Century"), "apk");
        File myFile = new File(context.getExternalFilesDir("Century"),"apk");
        boolean isCreate = myFile.exists();

        if(isCreate == false){
            isCreate = myFile.mkdirs();
        }

      //  myRequest.setDestinationInExternalPublicDir("/apk", name);
        myRequest.setDestinationInExternalFilesDir(context,"Century/apk",name);

        String h = myRequest.setDestinationInExternalFilesDir(context,"Century/apk",name).toString();

        Log.e("ruta_apk",h);
        System.out.println(h);

        tamaño = myDownloadManager.enqueue(myRequest);

    }

    public void registrar(MyReceiver objMyreceiver){
        myContext.registerReceiver(objMyreceiver, myIntentFilter);

    }

    public void borrarRegistro(MyReceiver objMyreceiver){
        myContext.unregisterReceiver(objMyreceiver);

    }
}
