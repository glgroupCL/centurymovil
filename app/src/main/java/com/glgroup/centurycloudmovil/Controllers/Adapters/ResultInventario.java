package com.glgroup.centurycloudmovil.Controllers.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ResListaRfid;
import com.glgroup.centurycloudmovil.R;

import java.util.ArrayList;

public class ResultInventario extends RecyclerView.Adapter<ResultInventario.exViewHolder> {

    private ArrayList<ResListaRfid> listaRFID;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public static class exViewHolder extends RecyclerView.ViewHolder {

        public TextView producto;
        public TextView cantidad;

        public exViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            producto = itemView.findViewById(R.id.producto);
            cantidad = itemView.findViewById(R.id.cantidad);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public ResultInventario(ArrayList<ResListaRfid> exampleList) {
        listaRFID = exampleList;
    }

    @Override
    public exViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_result_inventario, parent, false);
        exViewHolder evh = new exViewHolder(v, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(exViewHolder holder, int position) {
        ResListaRfid currentItem = listaRFID.get(position);

        holder.producto.setText(currentItem.getDescripcion());
        holder.cantidad.setText(String.valueOf(currentItem.getCantidad()));
    }

    @Override
    public int getItemCount() {
        return listaRFID.size();
    }
}
