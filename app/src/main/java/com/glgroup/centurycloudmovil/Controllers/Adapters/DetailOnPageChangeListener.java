package com.glgroup.centurycloudmovil.Controllers.Adapters;

import androidx.viewpager.widget.ViewPager;

public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

    private int currentPage;

    @Override
    public void onPageSelected(int position) {
        currentPage = position;  //*** Aquí se obtiene el indice!
    }

    public final int getCurrentPage() {
        return currentPage;
    }
}