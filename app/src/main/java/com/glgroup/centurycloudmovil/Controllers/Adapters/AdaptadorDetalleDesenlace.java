package com.glgroup.centurycloudmovil.Controllers.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.glgroup.centurycloudmovil.Models.Datos.ResListaBusquedaRfid;
import com.glgroup.centurycloudmovil.R;

import java.util.ArrayList;

public class AdaptadorDetalleDesenlace extends RecyclerView.Adapter<AdaptadorDetalleDesenlace.exViewHolder> {

    private ArrayList<ResListaBusquedaRfid> listaRFID;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public AdaptadorDetalleDesenlace(ArrayList<ResListaBusquedaRfid> exampleList) {
        listaRFID = exampleList;
    }

    public static class exViewHolder extends RecyclerView.ViewHolder {
        public TextView rfid;
        public TextView descripcion;
        public ImageView mDeleteImage;

        public exViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            rfid = itemView.findViewById(R.id.rfid);
            descripcion = itemView.findViewById(R.id.descripcion);
            mDeleteImage = itemView.findViewById(R.id.delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            mDeleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });

        }
    }

    @Override
    public exViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_ver_detalles, parent, false);
        exViewHolder evh = new exViewHolder(v, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(exViewHolder holder, int position) {
        ResListaBusquedaRfid currentItem = listaRFID.get(position);

        holder.rfid.setText(currentItem.getRfid());
        holder.descripcion.setText(currentItem.getDescripcion());
    }

    @Override
    public int getItemCount() {
        return listaRFID.size();
    }
}
