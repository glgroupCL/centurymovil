package com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.Interface;

/** DAO = DATA ACCESS OBJECT **/

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.Entity.PermisoBD;

import java.util.List;

@Dao
public interface PermisoDao {

    /** metodos CRUD para la tabla "pemiso" **/

    /** Seleccionar cantidad de datos en la tabla**/
    @Query("SELECT COUNT(*) FROM " + PermisoBD.TABLE_NAME)
    int sp_Sel_Count();

    /** seleccionar todos los datos **/
    @Query("SELECT * FROM " + PermisoBD.TABLE_NAME)
    List<PermisoBD> sp_Sel_All();

    /** actualizar permisos **/
    @Query("UPDATE "+ PermisoBD.TABLE_NAME + " SET permiso = :permiso WHERE id_boton = :idBoton")
    void sp_Upd_Permiso(int permiso, int idBoton);

    /** insertar varios permisos **/
    @Insert
    void sp_Ins_Permisos(PermisoBD... permisos);

    /** eliminar un permiso **/
    @Query("DELETE FROM "+ PermisoBD.TABLE_NAME + " WHERE " + PermisoBD.COLUMN_ID + " = :id")
    int sp_Del_Permiso(int id);

    /** insertar solo 1 permiso **/
    @Insert
    long sp_Ins_Permiso(PermisoBD permiso);

    /** obtener permiso en base a un id de boton **/
    @Query("SELECT * FROM "+PermisoBD.TABLE_NAME+" WHERE id_boton = :id LIMIT 1")
    PermisoBD sp_Sel_Permiso(int id);

    /** se obtiene la descripción de un permiso en base al id del boton **/
    @Query("SELECT descripcion FROM "+PermisoBD.TABLE_NAME+" WHERE id_boton = :id LIMIT 1")
    String sp_Sel_Descripcion(int id);

}
