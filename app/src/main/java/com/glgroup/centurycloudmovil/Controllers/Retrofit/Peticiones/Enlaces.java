package com.glgroup.centurycloudmovil.Controllers.Retrofit.Peticiones;

import android.content.Context;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.Enlace.Enlace;
import com.glgroup.centurycloudmovil.Models.Application.Enlace.ResEnlace;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;

public class Enlaces {

    public static int Enlace (Context context, String sku, String idItem, String rfid, String tipo, String contenedor){
        String token = Local.getData("login", context, "token");
        String usuario = Local.getData("login", context, "idUsuario");
        String cliente = Local.getData("login", context, "idCLiente");
        String proyecto = Local.getData("login", context, "idProyecto");
        String locacion = Local.getData("login", context, "idLocacion");
        String area = Local.getData("login", context, "idArea");


        final int[] result = {0};
        Call<Enlace> reqEnlace;
        final Enlace[] resEnlace = new Enlace[1];

        reqEnlace = ApiService.getApiService(context).getenlace(usuario,cliente,proyecto,locacion,area,sku,idItem,rfid,tipo,"1", Constantes.IMEI,contenedor,token);
        reqEnlace.enqueue(new Callback<Enlace>() {
            @Override
            public void onResponse(Call<Enlace> call, Response<Enlace> response) {
                if(response.isSuccessful()){
                    resEnlace[0] = response.body();
                    for(ResEnlace data : resEnlace[0].getDatos().getResultado()){
                        result[0] = data.getIdMsj();
                    }

                }
            }

            @Override
            public void onFailure(Call<Enlace> call, Throwable t) {

            }
        });

        return result[0];
    }
}
