package com.glgroup.centurycloudmovil.Controllers.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.glgroup.centurycloudmovil.Models.Application.Busqueda.ResListaBusquedaRfid;
import com.glgroup.centurycloudmovil.R;

import java.util.ArrayList;

public class AdaptadorListaRfidBusqueda extends RecyclerView.Adapter<AdaptadorListaRfidBusqueda.exViewHolder> {

    private ArrayList<ResListaBusquedaRfid> listaRFID;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public AdaptadorListaRfidBusqueda(ArrayList<ResListaBusquedaRfid> exampleList) {
        listaRFID = exampleList;
    }

    public static class exViewHolder extends RecyclerView.ViewHolder {
        public TextView rfid;
        public TextView descripcion;

        public exViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            rfid = itemView.findViewById(R.id.rfid);
            descripcion = itemView.findViewById(R.id.descripcion);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }

    @Override
    public exViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_rfid_busqueda, parent, false);
        exViewHolder evh = new exViewHolder(v, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(exViewHolder holder, int position) {
        ResListaBusquedaRfid currentItem = listaRFID.get(position);

        holder.rfid.setText(currentItem.getCodigoItem());
        holder.descripcion.setText(currentItem.getDescripcion());
    }

    @Override
    public int getItemCount() {
        return listaRFID.size();
    }
}
