package com.glgroup.centurycloudmovil.Controllers.Retrofit.Peticiones;

import android.content.Context;
import android.util.Log;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.IdItem;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.ResIdItem;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NombreItem {



    public static String obtenerNombre(final Context context, String sku) {
        String token = Local.getData("login", context, "token");
        String usuario = Local.getData("login", context, "idUsuario");
        String cliente = Local.getData("login", context, "idCLiente");
        String proyecto = Local.getData("login", context, "idProyecto");
        String locacion = Local.getData("login", context, "idLocacion");
        String area = Local.getData("login", context, "idArea");
        //token = Constantes.TOKEN;
        final Call<IdItem> reqIdItem;
        final IdItem[] resIdItem = new IdItem[1];

        final String[] nombre = {""};

        reqIdItem = ApiService.getApiService(context).getiditem(usuario, cliente, proyecto, locacion, area, sku, token);
        reqIdItem.enqueue(new Callback<IdItem>() {
            @Override
            public void onResponse(Call<IdItem> call, Response<IdItem> response) {
                if (response.isSuccessful()) {
                    resIdItem[0] = response.body();
                    for (ResIdItem data : resIdItem[0].getDatos().getResultado()) {
                        Log.d("item", data.getItem());
                        Log.d("item", String.valueOf(data.getIdItem()));
                        if (data.getIdItem() > 0) {
                            Local.setData("enlace", context, "idItem", String.valueOf(data.getIdItem()));
                            nombre[0] = data.getItem();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<IdItem> call, Throwable t) {

            }
        });

        Log.d("item", nombre[0]);
        return nombre[0];
    }
}
