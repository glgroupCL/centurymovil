package com.glgroup.centurycloudmovil.Controllers.Retrofit.Interface;


import com.glgroup.centurycloudmovil.Models.Application.Busqueda.ListaBusquedaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Desenlace.NombreRfid;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.Documento;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.EnvioListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.Documentos.ListaRfidDoc;
import com.glgroup.centurycloudmovil.Models.Application.Enlace.Enlace;
import com.glgroup.centurycloudmovil.Models.Application.EpcGrabar.EpcGrabar;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.CierreInventario;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.Inventarios;
import com.glgroup.centurycloudmovil.Models.Application.Inventarios.ListaRfid;
import com.glgroup.centurycloudmovil.Models.Application.ListaItem.ListaItems;
import com.glgroup.centurycloudmovil.Models.Application.LocacionesInOut.LocacionInOut;
import com.glgroup.centurycloudmovil.Models.Application.Login;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.CierreMovimiento;
import com.glgroup.centurycloudmovil.Models.Application.Movimiento.ListaRfidLeidosMov;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.IdItem;
import com.glgroup.centurycloudmovil.Models.Application.PermisoUsuario.Permiso;
import com.glgroup.centurycloudmovil.Models.Application.PermisosBotones.PermisoBtn;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas.Area;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes.Cliente;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones.Locacion;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos.Proyecto;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiServices {

    @FormUrlEncoded
    @POST("/Autentica")
    Call<Login> getLogin(
            @Field("correo") String Usuario,
            @Field("contrasena") String Contrasena,
            @Field("imei") String Imei);


    /** Se obtienen los permisos de los botones **/
    @FormUrlEncoded
    @POST("/SeleccionarPermisos")
    Call<PermisoBtn> getpermisos(
            @Field("id_usuario") String idUsuario,
            @Field("id_tipo") String idTipo,
            @Field("id_vista") String idVista,
            @Field("token") String token
    );


    /** Se listan las ubicaciones disponibles para el usuario **/
    @FormUrlEncoded
    @POST("/ListaCliente")
    Call<Cliente> getCliente(
            @Field("id_usuario") String idUsuario,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("/ListaProyectos")
    Call<Proyecto> getProyecto(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("/ListaLocaciones")
    Call<Locacion> getLocacion(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("/ListaAreas")
    Call<Area> getArea(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("token") String token
    );


    /** se obtiene la lista de inventarios disponibles para el usuario **/
    @FormUrlEncoded
    @POST("/InventarioArea")
    Call<Inventarios> getinventarios(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("tipo_ingreso") String tipo_ingreso,
            @Field("imei_mac") String imeiMac,
            @Field("token") String token
    );

    /** entrega el resultado de la lectura realizada en el inventario **/
    @FormUrlEncoded
    @POST("/ListaRFIDLeidos")
    Call<ListaRfid> getlistarfid(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("id_inventario") String idInventario,
            @Field("rfid") String lectura,
            @Field("tipo_ingreso") String tipo_ingreso,
            @Field("imei_mac") String imeiMac,
            @Field("token") String token
    );

    /** cierre del inventario realizado **/
    @FormUrlEncoded
    @POST("/ListaRFIDInventario")
    Call<CierreInventario> getcierreinventario(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("id_inventario") String idInventario,
            @Field("rfid") String lectura,
            @Field("tipo_ingreso") String tipo_ingreso,
            @Field("observaciones") String observaciones,
            @Field("imei_mac") String imeiMac,
            @Field("token") String token
    );

    /** enlaza un item con una etiqueta rfid **/
    @FormUrlEncoded
    @POST("/Enlazar")
    Call<Enlace> getenlace(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("sku") String sku,
            @Field("id_item") String idItem,
            @Field("rfid") String rfid,
            @Field("tipo") String tipo,
            @Field("tipo_ingreso") String tipoIngreso,
            @Field("imei_mac") String imeiMac,
            @Field("contenedor") String contenedor,
            @Field("token") String token
    );


    /** Obtener Epc para grabar en etiqueta **/
    @FormUrlEncoded
    @POST("/EpcItemGrabar")
    Call<EpcGrabar> getepcgrabar(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("id_item") String idItem,
            @Field("epc_leido") String epcLeido,
            @Field("token") String token
    );


    /** obtener el id de un item por el sku **/
    @FormUrlEncoded
    @POST("/BuscarNombreMaestra")
    Call<IdItem> getiditem(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("sku") String sku,
            @Field("token") String token
    );

    /** obtener el id de un item por el sku **/
    @FormUrlEncoded
    @POST("/ListaItemMaestra")
    Call<ListaItems> getlistaitems(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("nombre_item") String nombreItem,
            @Field("id_tipolista") String idTipoLista,
            @Field("token") String token
    );


    /** obtener el id y nombre de un item por el RFID **/
    @FormUrlEncoded
    @POST("/NombreItemRfid")
    Call<NombreRfid> getitemrfid(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("rfid") String rfid,
            @Field("tipo") String tipo,
            @Field("tipo_ingreso") String tipoIngreso,
            @Field("imei_mac") String imeiMac,
            @Field("token") String token

    );

    /** Desenlace de items **/
    @FormUrlEncoded
    @POST("/DesenlazarRfid")
    Call<NombreRfid> getdesenlace(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("rfid") String rfid,
            @Field("tipo") String tipo,
            @Field("tipo_ingreso") String tipoIngreso,
            @Field("imei_mac") String imeiMac,
            @Field("token") String token

    );

    /** Obtener documentos **/
    @FormUrlEncoded
    @POST("/ListaDocumentosInOut")
    Call<Documento> getdocumentos(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("id_tipo") String idTipo,
            @Field("token") String token

    );

    /** Resultados de la lectura entrada - salida **/
    @FormUrlEncoded
    @POST("/ListaRfidDocInOut")
    Call<ListaRfidDoc> getlistarfidinout(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("id_documento") String idDocumento,
            @Field("tipo_documento") String tipoDocumento,
            @Field("numero_documento") String numeroDocumento,
            @Field("id_proveedor") String idProveedor,
            @Field("lectura_rfid") String lecturaRfid,
            @Field("token") String token

    );

    /** Permisos de usuario entrada - salida **/
    @FormUrlEncoded
    @POST("/PermisosUsuarioAdmin")
    Call<Permiso> getusuarioadmin(
            @Field("id_usuario") String idUsuario,
            @Field("clave") String clave,
            @Field("imei_mac") String imeiMac,
            @Field("tipoRfid") String tipoRfid,
            @Field("tipoMovimiento") String tipoMovimiento,
            @Field("token") String token

    );

    /** Envío de lectura entrada - salida **/
    @FormUrlEncoded
    @POST("/ListaRfidDocInOutEnviar")
    Call<EnvioListaRfid> getenviorfidinout(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("id_documento") String idDocumento,
            @Field("tipo_documento") String tipoDocumento,
            @Field("numero_documento") String numeroDocumento,
            @Field("id_proveedor") String idProveedor,
            @Field("lectura_rfid") String lecturaRfid,
            @Field("observacion") String observacion,
            @Field("tipo_lector") String tipoLector,
            @Field("imei_mac") String imeiMac,
            @Field("token") String token

    );

    /** Locaciones de entrada - salida **/
    @FormUrlEncoded
    @POST("/FiltroGenerico")
    Call<LocacionInOut> getlocacionesinout(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("tipo_filtro") String tipoFiltro,
            @Field("segmento") String segmento,
            @Field("token") String token

    );

    /** Resultado de lectura movimiento entrada - salida **/
    @FormUrlEncoded
    @POST("/ListaRFIDLeidosMovimiento")
    Call<ListaRfidLeidosMov> getlistaleidosmov(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("movimiento") String movimiento,
            @Field("locacion_orides") String locacionOriDes,
            @Field("area_orides") String areaOriDes,
            @Field("rfid") String rfid,
            @Field("imei_mac") String imeiMac,
            @Field("token") String token

    );

    /** Cierre movimiento entrada - salida **/
    @FormUrlEncoded
    @POST("/ListaRfidMovimiento")
    Call<CierreMovimiento> getcierremovimiento(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("id_area") String idArea,
            @Field("movimiento") String movimiento,
            @Field("locacionOriDes") String locacionOriDes,
            @Field("areaOriDes") String areaOriDes,
            @Field("rfid") String rfid,
            @Field("imei_mac") String imeiMac,
            @Field("par_id_lector_RFID") String lector,
            @Field("token") String token

    );

    /** lista de rfid asociados a un SKU para realizar la búsqueda **/
    @FormUrlEncoded
    @POST("/ListaRFID")
    Call<ListaBusquedaRfid> getlistabusqueda(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("item") String item,
            @Field("token") String token

    );

    /** lista de rfid CENTURY asociados a un SKU para realizar la búsqueda **/
    @FormUrlEncoded
    @POST("/BusquedaEpcCentury")
    Call<ListaBusquedaRfid> getlistabusquedacentury(
            @Field("id_usuario") String idUsuario,
            @Field("id_cliente") String idCliente,
            @Field("id_proyecto") String idProyecto,
            @Field("id_locacion") String idLocacion,
            @Field("item") String item,
            @Field("token") String token

    );


}
