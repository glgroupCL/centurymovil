package com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.Entity;

import android.provider.BaseColumns;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = PermisoBD.TABLE_NAME)
public class PermisoBD {

    /** nombre de la tabla **/
    public static final String TABLE_NAME = "permiso";

    public static final String COLUMN_NAME = "name";

    public static final String COLUMN_ID= BaseColumns._ID;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo( index = true, name = COLUMN_ID)
    private int id;

    @ColumnInfo(name = "id_boton")
    private int idBoton;

    @ColumnInfo(name = "descripcion")
    private String descripcion;

    @ColumnInfo(name = "permiso")
    private int permiso;


    public PermisoBD(int idBoton, String descripcion, int permiso) {
        this.idBoton = idBoton;
        this.descripcion = descripcion;
        this.permiso = permiso;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdBoton() {
        return idBoton;
    }

    public void setIdBoton(int idBoton) {
        this.idBoton = idBoton;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPermiso() {
        return permiso;
    }

    public void setPermiso(int permiso) {
        this.permiso = permiso;
    }
}
