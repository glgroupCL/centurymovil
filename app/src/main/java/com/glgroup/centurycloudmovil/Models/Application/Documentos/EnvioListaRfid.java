package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

public class EnvioListaRfid {

    @SerializedName("datos")
    private DataEnvioListaRfid datos;

    public EnvioListaRfid(DataEnvioListaRfid datos) {
        this.datos = datos;
    }

    public DataEnvioListaRfid getDatos() {
        return datos;
    }

    public void setDatos(DataEnvioListaRfid datos) {
        this.datos = datos;
    }
}
