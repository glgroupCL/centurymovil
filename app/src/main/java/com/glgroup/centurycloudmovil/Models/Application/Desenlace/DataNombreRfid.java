package com.glgroup.centurycloudmovil.Models.Application.Desenlace;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataNombreRfid {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResNombreRfid> resultado;

    public DataNombreRfid(int exito, String mensaje, List<ResNombreRfid> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResNombreRfid> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResNombreRfid> resultado) {
        this.resultado = resultado;
    }
}
