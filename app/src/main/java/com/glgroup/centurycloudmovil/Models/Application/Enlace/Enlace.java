package com.glgroup.centurycloudmovil.Models.Application.Enlace;

import com.google.gson.annotations.SerializedName;

public class Enlace {

    @SerializedName("datos")
    private DataEnlace datos;

    public Enlace(DataEnlace datos) {
        this.datos = datos;
    }

    public DataEnlace getDatos() {
        return datos;
    }

    public void setDatos(DataEnlace datos) {
        this.datos = datos;
    }
}
