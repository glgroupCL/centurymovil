package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes;

import com.google.gson.annotations.SerializedName;

public class Cliente {

    @SerializedName("datos")
    private DataCliente datos;

    public Cliente() {
    }

    public Cliente(DataCliente datos) {
        this.datos = datos;
    }

    public DataCliente getDatos() {
        return datos;
    }

    public void setDatos(DataCliente datos) {
        this.datos = datos;
    }
}
