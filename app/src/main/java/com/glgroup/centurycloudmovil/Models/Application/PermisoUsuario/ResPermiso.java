package com.glgroup.centurycloudmovil.Models.Application.PermisoUsuario;

import com.google.gson.annotations.SerializedName;

public class ResPermiso {

    @SerializedName("id_permiso")
    private int idPermiso;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("permiso")
    private int permiso;

    @SerializedName("id_usuario")
    private int idUsuario;

    public ResPermiso(int idPermiso, String descripcion, int permiso, int idUsuario) {
        this.idPermiso = idPermiso;
        this.descripcion = descripcion;
        this.permiso = permiso;
        this.idUsuario = idUsuario;
    }

    public int getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(int idPermiso) {
        this.idPermiso = idPermiso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPermiso() {
        return permiso;
    }

    public void setPermiso(int permiso) {
        this.permiso = permiso;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
