package com.glgroup.centurycloudmovil.Models.Application.EpcGrabar;

import com.google.gson.annotations.SerializedName;

public class EpcGrabar {

    @SerializedName("datos")
    private DataEpcGrabar datos;

    public EpcGrabar(DataEpcGrabar datos) {
        this.datos = datos;
    }

    public DataEpcGrabar getDatos() {
        return datos;
    }

    public void setDatos(DataEpcGrabar datos) {
        this.datos = datos;
    }
}
