package com.glgroup.centurycloudmovil.Models.Application.PermisoUsuario;

import com.google.gson.annotations.SerializedName;

public class Permiso {

    @SerializedName("datos")
    private DataPermiso datos;

    public Permiso(DataPermiso datos) {
        this.datos = datos;
    }

    public DataPermiso getDatos() {
        return datos;
    }

    public void setDatos(DataPermiso datos) {
        this.datos = datos;
    }
}
