package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

public class Inventarios {

    @SerializedName("datos")
    private DataInventarios datos;

    public Inventarios(DataInventarios datos) {
        this.datos = datos;
    }

    public DataInventarios getDatos() {
        return datos;
    }

    public void setDatos(DataInventarios datos) {
        this.datos = datos;
    }
}
