package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

public class ResListaRfid {

    @SerializedName("secuencial")
    private int secuencial;

    @SerializedName("id_inventario")
    private int idInventario;

    @SerializedName("id_item")
    private int idItem;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("cantidad")
    private int cantidad;

    @SerializedName("link_imagen")
    private String linkImagen;

    public ResListaRfid(int secuencial, int idInventario, int idItem, String descripcion, int cantidad, String linkImagen) {
        this.secuencial = secuencial;
        this.idInventario = idInventario;
        this.idItem = idItem;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.linkImagen = linkImagen;
    }

    public int getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(int secuencial) {
        this.secuencial = secuencial;
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getLinkImagen() {
        return linkImagen;
    }

    public void setLinkImagen(String linkImagen) {
        this.linkImagen = linkImagen;
    }
}
