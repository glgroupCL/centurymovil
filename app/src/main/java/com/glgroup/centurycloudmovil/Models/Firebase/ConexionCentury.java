package com.glgroup.centurycloudmovil.Models.Firebase;

public class ConexionCentury {

    String Url;
    String Puerto;

    public ConexionCentury() {
    }


    public ConexionCentury(String url, String puerto) {
        Url = url;
        Puerto = puerto;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getPuerto() {
        return Puerto;
    }

    public void setPuerto(String puerto) {
        Puerto = puerto;
    }
}
