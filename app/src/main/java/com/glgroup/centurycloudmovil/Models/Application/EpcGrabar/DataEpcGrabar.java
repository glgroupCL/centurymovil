package com.glgroup.centurycloudmovil.Models.Application.EpcGrabar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataEpcGrabar {

    @SerializedName("resultado")
    private List<ResEpcGrabar> resultado;

    public DataEpcGrabar(List<ResEpcGrabar> resultado) {
        this.resultado = resultado;
    }

    public List<ResEpcGrabar> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResEpcGrabar> resultado) {
        this.resultado = resultado;
    }
}
