package com.glgroup.centurycloudmovil.Models.Application.LocacionesInOut;

import com.google.gson.annotations.SerializedName;

public class LocacionInOut {

    @SerializedName("datos")
    private DataLocacion datos;

    public LocacionInOut(DataLocacion datos) {
        this.datos = datos;
    }

    public DataLocacion getDatos() {
        return datos;
    }

    public void setDatos(DataLocacion datos) {
        this.datos = datos;
    }
}
