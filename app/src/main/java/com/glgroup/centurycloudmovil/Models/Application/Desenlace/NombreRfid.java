package com.glgroup.centurycloudmovil.Models.Application.Desenlace;

import com.google.gson.annotations.SerializedName;

public class NombreRfid {

    @SerializedName("datos")
    private DataNombreRfid datos;

    public NombreRfid(DataNombreRfid datos) {
        this.datos = datos;
    }

    public DataNombreRfid getDatos() {
        return datos;
    }

    public void setDatos(DataNombreRfid datos) {
        this.datos = datos;
    }
}
