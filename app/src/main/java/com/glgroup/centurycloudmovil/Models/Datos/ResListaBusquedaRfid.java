package com.glgroup.centurycloudmovil.Models.Datos;

public class ResListaBusquedaRfid {

    /** detalles del desenlace **/
    private String rfid;
    private String descripcion;

    public ResListaBusquedaRfid(String rfid, String descripcion) {
        this.rfid = rfid;
        this.descripcion = descripcion;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
