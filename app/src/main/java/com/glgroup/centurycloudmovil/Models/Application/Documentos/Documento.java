package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

public class Documento {

    @SerializedName("datos")
    private DataDocumento datos;

    public Documento(DataDocumento datos) {
        this.datos = datos;
    }

    public DataDocumento getDatos() {
        return datos;
    }

    public void setDatos(DataDocumento datos) {
        this.datos = datos;
    }
}
