package com.glgroup.centurycloudmovil.Models.Application.NombreItem;

import com.google.gson.annotations.SerializedName;

public class IdItem {

    @SerializedName("datos")
    private DataIdItem datos;

    public IdItem(DataIdItem datos) {
        this.datos = datos;
    }

    public DataIdItem getDatos() {
        return datos;
    }

    public void setDatos(DataIdItem datos) {
        this.datos = datos;
    }
}
