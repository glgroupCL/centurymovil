package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones;

import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos.DataProyecto;
import com.google.gson.annotations.SerializedName;

public class Locacion {

    @SerializedName("datos")
    private DataLocacion datos;

    public Locacion(DataLocacion datos) {
        this.datos = datos;
    }

    public DataLocacion getDatos() {
        return datos;
    }

    public void setDatos(DataLocacion datos) {
        this.datos = datos;
    }
}
