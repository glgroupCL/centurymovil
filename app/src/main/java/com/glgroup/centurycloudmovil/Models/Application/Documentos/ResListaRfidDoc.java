package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

public class ResListaRfidDoc {

    @SerializedName("secuencial")
    private int secuencial;

    @SerializedName("id_segmento")
    private int idSegmento;

    @SerializedName("segmento")
    private String segmento;

    @SerializedName("Valor_1")
    private int valor1;

    @SerializedName("txt_Valor_1")
    private String txtValor1;

    @SerializedName("int_Valor_1")
    private int intValor1;

    @SerializedName("str_Valor_1")
    private String strValor1;

    @SerializedName("Valor_2")
    private int valor2;

    @SerializedName("txt_Valor_2")
    private String txtValor2;

    @SerializedName("int_Valor_2")
    private int intValor2;

    @SerializedName("str_Valor_2")
    private String strValor2;

    @SerializedName("Valor_3")
    private int valor3;

    @SerializedName("txt_Valor_3")
    private String txtValor3;

    @SerializedName("int_Valor_3")
    private int intValor3;

    @SerializedName("str_Valor_3")
    private String strValor3;

    public ResListaRfidDoc(int secuencial, int idSegmento, String segmento, int valor1, String txtValor1, int intValor1, String strValor1, int valor2, String txtValor2, int intValor2, String strValor2, int valor3, String txtValor3, int intValor3, String strValor3) {
        this.secuencial = secuencial;
        this.idSegmento = idSegmento;
        this.segmento = segmento;
        this.valor1 = valor1;
        this.txtValor1 = txtValor1;
        this.intValor1 = intValor1;
        this.strValor1 = strValor1;
        this.valor2 = valor2;
        this.txtValor2 = txtValor2;
        this.intValor2 = intValor2;
        this.strValor2 = strValor2;
        this.valor3 = valor3;
        this.txtValor3 = txtValor3;
        this.intValor3 = intValor3;
        this.strValor3 = strValor3;
    }

    public int getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(int secuencial) {
        this.secuencial = secuencial;
    }

    public int getIdSegmento() {
        return idSegmento;
    }

    public void setIdSegmento(int idSegmento) {
        this.idSegmento = idSegmento;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public int getValor1() {
        return valor1;
    }

    public void setValor1(int valor1) {
        this.valor1 = valor1;
    }

    public String getTxtValor1() {
        return txtValor1;
    }

    public void setTxtValor1(String txtValor1) {
        this.txtValor1 = txtValor1;
    }

    public int getIntValor1() {
        return intValor1;
    }

    public void setIntValor1(int intValor1) {
        this.intValor1 = intValor1;
    }

    public String getStrValor1() {
        return strValor1;
    }

    public void setStrValor1(String strValor1) {
        this.strValor1 = strValor1;
    }

    public int getValor2() {
        return valor2;
    }

    public void setValor2(int valor2) {
        this.valor2 = valor2;
    }

    public String getTxtValor2() {
        return txtValor2;
    }

    public void setTxtValor2(String txtValor2) {
        this.txtValor2 = txtValor2;
    }

    public int getIntValor2() {
        return intValor2;
    }

    public void setIntValor2(int intValor2) {
        this.intValor2 = intValor2;
    }

    public String getStrValor2() {
        return strValor2;
    }

    public void setStrValor2(String strValor2) {
        this.strValor2 = strValor2;
    }

    public int getValor3() {
        return valor3;
    }

    public void setValor3(int valor3) {
        this.valor3 = valor3;
    }

    public String getTxtValor3() {
        return txtValor3;
    }

    public void setTxtValor3(String txtValor3) {
        this.txtValor3 = txtValor3;
    }

    public int getIntValor3() {
        return intValor3;
    }

    public void setIntValor3(int intValor3) {
        this.intValor3 = intValor3;
    }

    public String getStrValor3() {
        return strValor3;
    }

    public void setStrValor3(String strValor3) {
        this.strValor3 = strValor3;
    }
}
