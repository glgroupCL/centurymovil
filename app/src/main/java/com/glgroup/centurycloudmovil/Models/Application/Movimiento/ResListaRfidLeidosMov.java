package com.glgroup.centurycloudmovil.Models.Application.Movimiento;

import com.google.gson.annotations.SerializedName;

public class ResListaRfidLeidosMov {

    @SerializedName("secuencial")
    private int secuencial;

    @SerializedName("id_item")
    private int idItem;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("cantidad")
    private String cantidad;

    public ResListaRfidLeidosMov(int secuencial, int idItem, String descripcion, String cantidad) {
        this.secuencial = secuencial;
        this.idItem = idItem;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
    }

    public int getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(int secuencial) {
        this.secuencial = secuencial;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
