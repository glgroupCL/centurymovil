package com.glgroup.centurycloudmovil.Models.Application.NombreItem;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataIdItem {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResIdItem> resultado;

    public DataIdItem(int exito, String mensaje, List<ResIdItem> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResIdItem> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResIdItem> resultado) {
        this.resultado = resultado;
    }
}
