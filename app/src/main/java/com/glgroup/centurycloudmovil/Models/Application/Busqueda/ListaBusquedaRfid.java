package com.glgroup.centurycloudmovil.Models.Application.Busqueda;

import com.google.gson.annotations.SerializedName;

public class ListaBusquedaRfid {

    @SerializedName("datos")
    private DataListaBusquedaRfid datos;

    public ListaBusquedaRfid(DataListaBusquedaRfid datos) {
        this.datos = datos;
    }

    public DataListaBusquedaRfid getDatos() {
        return datos;
    }

    public void setDatos(DataListaBusquedaRfid datos) {
        this.datos = datos;
    }
}
