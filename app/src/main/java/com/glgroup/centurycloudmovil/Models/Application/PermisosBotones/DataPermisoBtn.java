package com.glgroup.centurycloudmovil.Models.Application.PermisosBotones;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPermisoBtn {

    @SerializedName("extio")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResPermisoBtn> resultado;

    public DataPermisoBtn(int exito, String mensaje, List<ResPermisoBtn> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResPermisoBtn> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResPermisoBtn> resultado) {
        this.resultado = resultado;
    }
}
