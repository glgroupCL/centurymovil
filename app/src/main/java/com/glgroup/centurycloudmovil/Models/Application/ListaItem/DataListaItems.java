package com.glgroup.centurycloudmovil.Models.Application.ListaItem;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataListaItems {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResListaItems> resultado;

    public DataListaItems(int exito, String mensaje, List<ResListaItems> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResListaItems> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResListaItems> resultado) {
        this.resultado = resultado;
    }
}
