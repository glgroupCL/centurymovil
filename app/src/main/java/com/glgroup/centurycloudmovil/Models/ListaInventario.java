package com.glgroup.centurycloudmovil.Models;

public class ListaInventario {

    private int id;
    private String titulo;
    private String subtitulo;

    public ListaInventario(int id, String titulo, String subtitulo) {
        this.id = id;
        this.titulo = titulo;
        this.subtitulo = subtitulo;
    }

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    @Override
    public String toString() {
        return "ListaInventario{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", subtitulo='" + subtitulo + '\'' +
                '}';
    }
}
