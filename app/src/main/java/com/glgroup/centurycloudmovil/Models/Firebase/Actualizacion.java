package com.glgroup.centurycloudmovil.Models.Firebase;

public class Actualizacion {

    private String version;
    private String url;

    public Actualizacion() {
    }

    public Actualizacion(String version, String url) {
        this.version = version;
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
