package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataArea {
    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResArea> resultado;

    public DataArea(int exito, String mensaje, List<ResArea> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResArea> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResArea> resultado) {
        this.resultado = resultado;
    }
}
