package com.glgroup.centurycloudmovil.Models.Application.Movimiento;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataCierreMovimiento {

    @SerializedName("resultado")
    private List<ResCierreMovimiento> resultado;

    public DataCierreMovimiento(List<ResCierreMovimiento> resultado) {
        this.resultado = resultado;
    }

    public List<ResCierreMovimiento> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResCierreMovimiento> resultado) {
        this.resultado = resultado;
    }
}
