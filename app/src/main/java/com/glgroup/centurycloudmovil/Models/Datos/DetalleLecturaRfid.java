package com.glgroup.centurycloudmovil.Models.Datos;

public class DetalleLecturaRfid {

    private String item;
    private String cant_dcto;
    private String cant_read;
    private String diferencia;

    public DetalleLecturaRfid(String item, String cant_dcto, String cant_read, String diferencia) {
        this.item = item;
        this.cant_dcto = cant_dcto;
        this.cant_read = cant_read;
        this.diferencia = diferencia;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getCant_dcto() {
        return cant_dcto;
    }

    public void setCant_dcto(String cant_dcto) {
        this.cant_dcto = cant_dcto;
    }

    public String getCant_read() {
        return cant_read;
    }

    public void setCant_read(String cant_read) {
        this.cant_read = cant_read;
    }

    public String getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(String diferencia) {
        this.diferencia = diferencia;
    }
}
