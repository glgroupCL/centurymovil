package com.glgroup.centurycloudmovil.Models.Firebase;

public class Login {
    String Usuario;
    String Fecha;

    public Login() {
    }

    public Login(String usuario, String fecha) {
        this.Usuario = usuario;
        Fecha = fecha;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        this.Usuario = usuario;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }
}
