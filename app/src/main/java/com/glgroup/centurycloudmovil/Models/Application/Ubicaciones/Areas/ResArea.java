package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas;

import com.google.gson.annotations.SerializedName;

public class ResArea {

    @SerializedName("id_area")
    private int idArea;

    @SerializedName("descripcion")
    private String descripcion;

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
