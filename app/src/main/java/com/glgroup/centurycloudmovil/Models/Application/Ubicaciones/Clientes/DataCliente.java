package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataCliente {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

   @SerializedName("resultado")
    private List<ResCliente> resCliente;

    public DataCliente(int exito, String mensaje, List<ResCliente> resCliente) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resCliente = resCliente;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResCliente> getResCliente() {
        return resCliente;
    }

    public void setResCliente(List<ResCliente> resCliente) {
        this.resCliente = resCliente;
    }
}
