package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataListaRfidDoc {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResListaRfidDoc> resultado;

    public DataListaRfidDoc(int exito, String mensaje, List<ResListaRfidDoc> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResListaRfidDoc> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResListaRfidDoc> resultado) {
        this.resultado = resultado;
    }
}
