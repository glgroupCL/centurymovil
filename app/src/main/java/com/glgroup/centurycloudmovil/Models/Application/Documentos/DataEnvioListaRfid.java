package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataEnvioListaRfid {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResEnvioListaRfid> resultado;

    public DataEnvioListaRfid(int exito, String mensaje, List<ResEnvioListaRfid> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResEnvioListaRfid> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResEnvioListaRfid> resultado) {
        this.resultado = resultado;
    }
}
