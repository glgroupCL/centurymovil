package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataProyecto {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResProyecto> resultado;

    public DataProyecto(int exito, String mensaje, List<ResProyecto> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResProyecto> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResProyecto> resultado) {
        this.resultado = resultado;
    }
}
