package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

public class ResInventarios {

    @SerializedName("id_inventario")
    private int idInventario;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("UsuarioInventario")
    private String usuarioInventario;

    @SerializedName("fechaCreacion")
    private String fechaCreacion;

    @SerializedName("Proyecto")
    private String proyecto;

    @SerializedName("Locacion")
    private String locacion;

    @SerializedName("Area")
    private String area;

    public ResInventarios(int idInventario, String descripcion, String usuarioInventario, String fechaCreacion, String proyecto, String locacion, String area) {
        this.idInventario = idInventario;
        this.descripcion = descripcion;
        this.usuarioInventario = usuarioInventario;
        this.fechaCreacion = fechaCreacion;
        this.proyecto = proyecto;
        this.locacion = locacion;
        this.area = area;
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuarioInventario() {
        return usuarioInventario;
    }

    public void setUsuarioInventario(String usuarioInventario) {
        this.usuarioInventario = usuarioInventario;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getLocacion() {
        return locacion;
    }

    public void setLocacion(String locacion) {
        this.locacion = locacion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
