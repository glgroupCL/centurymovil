package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

public class ResCierreInventario {

    @SerializedName("secuencia")
    private int secuencial;

    @SerializedName("id_msj")
    private int idMensaje;

    @SerializedName("mensaje")
    private String mensaje;

    public ResCierreInventario(int secuencial, int idMensaje, String mensaje) {
        this.secuencial = secuencial;
        this.idMensaje = idMensaje;
        this.mensaje = mensaje;
    }

    public int getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(int secuencial) {
        this.secuencial = secuencial;
    }

    public int getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
