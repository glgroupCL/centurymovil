package com.glgroup.centurycloudmovil.Models.Application.LocacionesInOut;

import com.google.gson.annotations.SerializedName;

public class ResLocacion {

    @SerializedName("id_opcion")
    private int idOpcion;

    @SerializedName("opcion")
    private String opcion;

    public ResLocacion(int idOpcion, String opcion) {
        this.idOpcion = idOpcion;
        this.opcion = opcion;
    }

    public int getIdOpcion() {
        return idOpcion;
    }

    public void setIdOpcion(int idOpcion) {
        this.idOpcion = idOpcion;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }
}
