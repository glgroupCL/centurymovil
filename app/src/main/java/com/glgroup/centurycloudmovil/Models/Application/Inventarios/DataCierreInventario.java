package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataCierreInventario {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResCierreInventario> resultado;

    public DataCierreInventario(int exito, String mensaje, List<ResCierreInventario> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResCierreInventario> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResCierreInventario> resultado) {
        this.resultado = resultado;
    }
}
