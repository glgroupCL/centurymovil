package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas;

import com.google.gson.annotations.SerializedName;

public class Area {

    @SerializedName("datos")
    private DataArea datos;

    public Area(DataArea datos) {
        this.datos = datos;
    }

    public DataArea getDatos() {
        return datos;
    }

    public void setDatos(DataArea datos) {
        this.datos = datos;
    }
}
