package com.glgroup.centurycloudmovil.Models.Application.PermisosBotones;

import com.google.gson.annotations.SerializedName;

public class ResPermisoBtn {

    @SerializedName("id_boton")
    private int idBoton;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("permiso")
    private int permiso;

    public ResPermisoBtn(int idBoton, String descripcion, int permiso) {
        this.idBoton = idBoton;
        this.descripcion = descripcion;
        this.permiso = permiso;
    }

    public int getIdBoton() {
        return idBoton;
    }

    public void setIdBoton(int idBoton) {
        this.idBoton = idBoton;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPermiso() {
        return permiso;
    }

    public void setPermiso(int permiso) {
        this.permiso = permiso;
    }
}
