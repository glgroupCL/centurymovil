package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

public class CierreInventario {

    @SerializedName("datos")
    private DataCierreInventario datos;

    public CierreInventario(DataCierreInventario datos) {
        this.datos = datos;
    }

    public DataCierreInventario getDatos() {
        return datos;
    }

    public void setDatos(DataCierreInventario datos) {
        this.datos = datos;
    }
}
