package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes;

import com.google.gson.annotations.SerializedName;

public class ResCliente {

    @SerializedName("id_cliente")
    private int idCliente;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("fotoCliente")
    private String fotoCliente;

    public ResCliente(int idCliente, String descripcion, String fotoCliente) {
        this.idCliente = idCliente;
        this.descripcion = descripcion;
        this.fotoCliente = fotoCliente;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFotoCliente() {
        return fotoCliente;
    }

    public void setFotoCliente(String fotoCliente) {
        this.fotoCliente = fotoCliente;
    }
}
