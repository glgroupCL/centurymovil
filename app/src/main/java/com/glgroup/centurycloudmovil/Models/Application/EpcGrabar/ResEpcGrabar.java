package com.glgroup.centurycloudmovil.Models.Application.EpcGrabar;

import com.google.gson.annotations.SerializedName;

public class ResEpcGrabar {

    @SerializedName("id_epc")
    private int idEpc;

    @SerializedName("epc")
    private String epc;


    public ResEpcGrabar(int idEpc, String epc) {
        this.idEpc = idEpc;
        this.epc = epc;
    }

    public int getIdEpc() {
        return idEpc;
    }

    public void setIdEpc(int idEpc) {
        this.idEpc = idEpc;
    }

    public String getEpc() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }
}
