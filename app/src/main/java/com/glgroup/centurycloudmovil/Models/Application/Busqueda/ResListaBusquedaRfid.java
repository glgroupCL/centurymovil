package com.glgroup.centurycloudmovil.Models.Application.Busqueda;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class ResListaBusquedaRfid {

    @SerializedName("id_item")
    private int idItem;

    @SerializedName("id_msj")
    private int idMsj;

    @SerializedName("codigoItem")
    private String codigoItem;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("RFID")
    private String rfid;

    public ResListaBusquedaRfid(int idItem, String codigoItem, String descripcion, String rfid, int idMsj) {
        this.idItem = idItem;
        this.codigoItem = codigoItem;
        this.descripcion = descripcion;
        this.rfid = rfid;
        this.idMsj = idMsj;
    }

    public int getIdMsj() {
        return idMsj;
    }

    public void setIdMsj(int idMsj) {
        this.idMsj = idMsj;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getCodigoItem() {
        return codigoItem;
    }

    public void setCodigoItem(String codigoItem) {
        this.codigoItem = codigoItem;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResListaBusquedaRfid that = (ResListaBusquedaRfid) o;
        return idItem == that.idItem &&
                idMsj == that.idMsj &&
                Objects.equals(codigoItem, that.codigoItem) &&
                Objects.equals(descripcion, that.descripcion) &&
                Objects.equals(rfid, that.rfid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idItem, idMsj, codigoItem, descripcion, rfid);
    }
}
