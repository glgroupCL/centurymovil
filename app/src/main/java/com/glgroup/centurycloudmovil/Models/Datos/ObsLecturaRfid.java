package com.glgroup.centurycloudmovil.Models.Datos;

public class ObsLecturaRfid {

    private String dato;
    private String valor;
    private String id;

    public ObsLecturaRfid(String dato, String valor, String id) {
        this.dato = dato;
        this.valor = valor;
        this.id = id;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
