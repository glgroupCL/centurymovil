package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos;

import com.google.gson.annotations.SerializedName;

public class Proyecto {

    @SerializedName("datos")
    private DataProyecto datos;

    public Proyecto(DataProyecto datos) {
        this.datos = datos;
    }

    public DataProyecto getDatos() {
        return datos;
    }

    public void setDatos(DataProyecto datos) {
        this.datos = datos;
    }
}
