package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones;

import com.google.gson.annotations.SerializedName;

public class ResLocacion {

    @SerializedName("id_locacion")
    private int idLocacion;

    @SerializedName("descripcion")
    private  String descripcion;

    public ResLocacion(int idLocacion, String descripcion) {
        this.idLocacion = idLocacion;
        this.descripcion = descripcion;
    }

    public int getIdLocacion() {
        return idLocacion;
    }

    public void setIdLocacion(int idLocacion) {
        this.idLocacion = idLocacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
