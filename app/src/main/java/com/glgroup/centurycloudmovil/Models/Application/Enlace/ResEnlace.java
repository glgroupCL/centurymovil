package com.glgroup.centurycloudmovil.Models.Application.Enlace;

import com.google.gson.annotations.SerializedName;

public class ResEnlace {

    @SerializedName("id_msj")
    private int idMsj;

    @SerializedName("Mensaje")
    private String mensaje;

    public ResEnlace(int idMsj, String mensaje) {
        this.idMsj = idMsj;
        this.mensaje = mensaje;
    }

    public int getIdMsj() {
        return idMsj;
    }

    public void setIdMsj(int idMsj) {
        this.idMsj = idMsj;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
