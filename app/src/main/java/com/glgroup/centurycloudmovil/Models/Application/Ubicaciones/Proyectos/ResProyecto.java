package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos;

import com.google.gson.annotations.SerializedName;

public class ResProyecto {

    @SerializedName("id_proyecto")
    private int idProyecto;

    @SerializedName("descripcion")
    private String descripcion;

    public ResProyecto(int idProyecto, String descripcion) {
        this.idProyecto = idProyecto;
        this.descripcion = descripcion;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
