package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

public class ResEnvioListaRfid {

    @SerializedName("id_msg")
    private int idMsg;

    @SerializedName("tipo_msg")
    private String tipoMsg;

    @SerializedName("msg_1")
    private String msg1;

    @SerializedName("msg_2")
    private String msg2;

    @SerializedName("msg_3")
    private String msg3;

    public ResEnvioListaRfid(int idMsg, String tipoMsg, String msg1, String msg2, String msg3) {
        this.idMsg = idMsg;
        this.tipoMsg = tipoMsg;
        this.msg1 = msg1;
        this.msg2 = msg2;
        this.msg3 = msg3;
    }

    public int getIdMsg() {
        return idMsg;
    }

    public void setIdMsg(int idMsg) {
        this.idMsg = idMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public String getMsg1() {
        return msg1;
    }

    public void setMsg1(String msg1) {
        this.msg1 = msg1;
    }

    public String getMsg2() {
        return msg2;
    }

    public void setMsg2(String msg2) {
        this.msg2 = msg2;
    }

    public String getMsg3() {
        return msg3;
    }

    public void setMsg3(String msg3) {
        this.msg3 = msg3;
    }
}
