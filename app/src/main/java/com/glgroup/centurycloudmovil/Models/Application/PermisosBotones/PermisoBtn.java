package com.glgroup.centurycloudmovil.Models.Application.PermisosBotones;

import com.google.gson.annotations.SerializedName;

public class PermisoBtn {

    @SerializedName("datos")
    private DataPermisoBtn datos;

    public PermisoBtn(DataPermisoBtn datos) {
        this.datos = datos;
    }

    public DataPermisoBtn getDatos() {
        return datos;
    }

    public void setDatos(DataPermisoBtn datos) {
        this.datos = datos;
    }
}
