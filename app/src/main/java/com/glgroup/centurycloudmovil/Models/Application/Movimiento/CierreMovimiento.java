package com.glgroup.centurycloudmovil.Models.Application.Movimiento;

import com.google.gson.annotations.SerializedName;

public class CierreMovimiento {

    @SerializedName("datos")
    private DataCierreMovimiento datos;

    public CierreMovimiento(DataCierreMovimiento datos) {
        this.datos = datos;
    }

    public DataCierreMovimiento getDatos() {
        return datos;
    }

    public void setDatos(DataCierreMovimiento datos) {
        this.datos = datos;
    }
}
