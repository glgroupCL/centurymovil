package com.glgroup.centurycloudmovil.Models.Application.ListaItem;

import com.google.gson.annotations.SerializedName;

public class ListaItems {

    @SerializedName("datos")
    private DataListaItems datos;

    public ListaItems(DataListaItems datos) {
        this.datos = datos;
    }

    public DataListaItems getDatos() {
        return datos;
    }

    public void setDatos(DataListaItems datos) {
        this.datos = datos;
    }
}
