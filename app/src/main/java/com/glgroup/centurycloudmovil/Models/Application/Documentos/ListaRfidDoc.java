package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

public class ListaRfidDoc {

    @SerializedName("datos")
    private DataListaRfidDoc datos;

    public ListaRfidDoc(DataListaRfidDoc datos) {
        this.datos = datos;
    }

    public DataListaRfidDoc getDatos() {
        return datos;
    }

    public void setDatos(DataListaRfidDoc datos) {
        this.datos = datos;
    }
}
