package com.glgroup.centurycloudmovil.Models.Application.NombreItem;

import com.google.gson.annotations.SerializedName;

public class ResIdItem {

    @SerializedName("id_item")
    private int idItem;

    @SerializedName("item")
    private String item;

    @SerializedName("barcode")
    private String barcode;

    public ResIdItem(int idItem, String item, String barcode) {
        this.idItem = idItem;
        this.item = item;
        this.barcode = barcode;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
