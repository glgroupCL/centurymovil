package com.glgroup.centurycloudmovil.Models;

import android.animation.TimeInterpolator;

public class ItemModel {
    public final String titulo;
    public final int colorId1;
    public final int colorId2;
    public final TimeInterpolator interpolator;
    public final String description;

    public ItemModel(String titulo, int colorId1, int colorId2, TimeInterpolator interpolator,String description) {
        this.titulo = titulo;
        this.colorId1 = colorId1;
        this.colorId2 = colorId2;
        this.interpolator = interpolator;
        this.description = description;
    }
}
