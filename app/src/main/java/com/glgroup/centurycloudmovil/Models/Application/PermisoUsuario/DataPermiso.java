package com.glgroup.centurycloudmovil.Models.Application.PermisoUsuario;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPermiso {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResPermiso> resultado;

    public DataPermiso(int exito, String mensaje, List<ResPermiso> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResPermiso> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResPermiso> resultado) {
        this.resultado = resultado;
    }
}
