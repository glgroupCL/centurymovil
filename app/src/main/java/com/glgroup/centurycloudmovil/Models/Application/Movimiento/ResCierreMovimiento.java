package com.glgroup.centurycloudmovil.Models.Application.Movimiento;

import com.google.gson.annotations.SerializedName;

public class ResCierreMovimiento {

    @SerializedName("id_msj")
    private int idMsj;

    @SerializedName("mensaje")
    private String mensaje;

    public ResCierreMovimiento(int idMsj, String mensaje) {
        this.idMsj = idMsj;
        this.mensaje = mensaje;
    }

    public int getIdMsj() {
        return idMsj;
    }

    public void setIdMsj(int idMsj) {
        this.idMsj = idMsj;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
