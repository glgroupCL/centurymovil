package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataListaRfid {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResListaRfid> resultado;

    public DataListaRfid(int exito, String mensaje, List<ResListaRfid> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResListaRfid> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResListaRfid> resultado) {
        this.resultado = resultado;
    }
}
