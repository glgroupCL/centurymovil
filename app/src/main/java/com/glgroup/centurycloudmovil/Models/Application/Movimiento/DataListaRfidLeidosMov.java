package com.glgroup.centurycloudmovil.Models.Application.Movimiento;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataListaRfidLeidosMov {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResListaRfidLeidosMov> resultado;

    public DataListaRfidLeidosMov(int exito, String mensaje, List<ResListaRfidLeidosMov> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResListaRfidLeidosMov> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResListaRfidLeidosMov> resultado) {
        this.resultado = resultado;
    }
}
