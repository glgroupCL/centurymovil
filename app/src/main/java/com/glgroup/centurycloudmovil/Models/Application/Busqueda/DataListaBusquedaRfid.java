package com.glgroup.centurycloudmovil.Models.Application.Busqueda;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataListaBusquedaRfid {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResListaBusquedaRfid> resultado;

    public DataListaBusquedaRfid(int exito, String mensaje, List<ResListaBusquedaRfid> resultado) {
        this.exito = exito;
        this.mensaje = mensaje;
        this.resultado = resultado;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResListaBusquedaRfid> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResListaBusquedaRfid> resultado) {
        this.resultado = resultado;
    }
}
