package com.glgroup.centurycloudmovil.Models.Application;

import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("ok")
    private int ok;

    @SerializedName("token")
    private String token;

    public Login() {
    }


    public Login(int ok, String token) {
        this.ok = ok;
        this.token = token;
    }

    public int getOk() {
        return ok;
    }

    public void setOk(int ok) {
        this.ok = ok;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
