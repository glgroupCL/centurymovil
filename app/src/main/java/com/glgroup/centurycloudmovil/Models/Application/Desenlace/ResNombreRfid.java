package com.glgroup.centurycloudmovil.Models.Application.Desenlace;

import com.google.gson.annotations.SerializedName;

public class ResNombreRfid {

    @SerializedName("id_item")
    private int idItem;

    @SerializedName("RFID")
    private String rfid;

    @SerializedName("descripcion")
    private String descripcion;

    public ResNombreRfid(int idItem, String rfid, String descripcion) {
        this.idItem = idItem;
        this.rfid = rfid;
        this.descripcion = descripcion;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
