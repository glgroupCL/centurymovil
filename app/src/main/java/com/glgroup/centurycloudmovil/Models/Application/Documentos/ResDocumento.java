package com.glgroup.centurycloudmovil.Models.Application.Documentos;

import com.google.gson.annotations.SerializedName;

public class ResDocumento {

    @SerializedName("secuencial")
    private int secuencial;

    @SerializedName("id_documento")
    private int idDocumento;

    @SerializedName("numero_documento")
    private String numeroDocumento;

    @SerializedName("tipo_documento")
    private int tipoDocumento;

    @SerializedName("id_proveedor")
    private int idProveedor;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("observaciones_dcto")
    private String observacionesDcto;

    public ResDocumento(int secuencial, int idDocumento, String numeroDocumento, int tipoDocumento, int idProveedor, String descripcion, String observacionesDcto) {
        this.secuencial = secuencial;
        this.idDocumento = idDocumento;
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumento = tipoDocumento;
        this.idProveedor = idProveedor;
        this.descripcion = descripcion;
        this.observacionesDcto = observacionesDcto;
    }

    public int getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(int secuencial) {
        this.secuencial = secuencial;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public int getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(int tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacionesDcto() {
        return observacionesDcto;
    }

    public void setObservacionesDcto(String observacionesDcto) {
        this.observacionesDcto = observacionesDcto;
    }
}
