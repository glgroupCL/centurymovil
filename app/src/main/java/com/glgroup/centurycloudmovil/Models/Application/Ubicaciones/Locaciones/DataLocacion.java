package com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones;

import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos.ResProyecto;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataLocacion {


    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<ResLocacion> resultado;

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ResLocacion> getResultado() {
        return resultado;
    }

    public void setResultado(List<ResLocacion> resultado) {
        this.resultado = resultado;
    }
}
