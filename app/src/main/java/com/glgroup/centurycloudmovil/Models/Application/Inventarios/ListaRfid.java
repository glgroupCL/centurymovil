package com.glgroup.centurycloudmovil.Models.Application.Inventarios;

import com.google.gson.annotations.SerializedName;

public class ListaRfid {

    @SerializedName("datos")
    private DataListaRfid datos;

    public ListaRfid(DataListaRfid datos) {
        this.datos = datos;
    }

    public DataListaRfid getDatos() {
        return datos;
    }

    public void setDatos(DataListaRfid datos) {
        this.datos = datos;
    }
}
