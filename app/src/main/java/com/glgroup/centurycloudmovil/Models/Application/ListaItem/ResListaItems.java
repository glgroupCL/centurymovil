package com.glgroup.centurycloudmovil.Models.Application.ListaItem;

import com.google.gson.annotations.SerializedName;

public class ResListaItems {

    @SerializedName("id_item")
    private int idItem;

    @SerializedName("item")
    private String item;

    public ResListaItems(int idItem, String item) {
        this.idItem = idItem;
        this.item = item;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
