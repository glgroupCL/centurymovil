package com.glgroup.centurycloudmovil.Models.Application.Movimiento;

import com.google.gson.annotations.SerializedName;

public class ListaRfidLeidosMov {

    @SerializedName("datos")
    private DataListaRfidLeidosMov datos;

    public ListaRfidLeidosMov(DataListaRfidLeidosMov datos) {
        this.datos = datos;
    }

    public DataListaRfidLeidosMov getDatos() {
        return datos;
    }

    public void setDatos(DataListaRfidLeidosMov datos) {
        this.datos = datos;
    }
}
