package com.glgroup.centurycloudmovil.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas.Area;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Areas.ResArea;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes.Cliente;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones.Locacion;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Locaciones.ResLocacion;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos.Proyecto;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Proyectos.ResProyecto;
import com.glgroup.centurycloudmovil.Models.Application.Ubicaciones.Clientes.ResCliente;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.AlertCarga;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbicacionesActivity extends AppCompatActivity implements View.OnClickListener{

    /** peticiones retrofit **/
    private Call<Cliente> reqCliente;
    private Call<Proyecto> reqProyecto;
    private Call<Locacion> reqLocacion;
    private Call<Area> reqArea;

    /** datos obtenidos retrofit **/
    private Cliente resCliente;
    private Proyecto resProyecto;
    private Locacion resLocacion;
    private Area resArea;


    /** spinner de datos **/
    private Spinner spinnerCliente, spinnerProyecto, spinnerLocacion, spinnerArea;

    /** Llave para peticiones **/
    private String token;

    /** variables al cambiar los datos **/
    private String idCliente, idProyectoSel, idLocacionSel, idAreaSel;
    private String nomCliente, nomProyecto, nomLocacion, nomArea;
    private String urlImg;

    /** botones de la vista **/
    private Button btnIngresar, btn_regresar;

    /** ventana de carga **/
    private AlertCarga alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicaciones);

        alert = new AlertCarga(getString(R.string.obteniendo_ubicaciones), UbicacionesActivity.this, getLayoutInflater());
        alert.alertCarga();
        alert.alertShow();

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }else{
            setToolbar();
        }
        init();
        String idusuario = Local.getData("login", getApplicationContext(), "idUsuario");
        consultaCliente(idusuario);
    }

    /** inicio de componentes **/
    public void init(){
        spinnerCliente = findViewById(R.id.spinnerCliente);
        spinnerProyecto = findViewById(R.id.spinnerProyecto);
        spinnerLocacion = findViewById(R.id.spinnerLocacion);
        spinnerArea = findViewById(R.id.spinnerArea);

        btnIngresar = findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(this);
        btnIngresar.setVisibility(View.INVISIBLE);

        btn_regresar = findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(this);

        token = Local.getData("login", getApplicationContext(), "token");
        //token = Constantes.TOKEN;
    }

    /** se carga un toolbar personalizado **/
    private void setToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubicaciones");
    }


    /**
     * Realiza una petición a la base de datos para obtener los clientes asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idUsuario - id obtenido en el login
     */
    public void consultaCliente(final String idUsuario){

        final ArrayList<String> listaCliente = new ArrayList<>();
        reqCliente = ApiService.getApiService(getApplicationContext()).getCliente(idUsuario, token);
        reqCliente.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if(response.isSuccessful()){
                    resCliente = response.body();
                    for(ResCliente cliente : resCliente.getDatos().getResCliente()){
                        listaCliente.add(cliente.getDescripcion());
                    }
                    cargaCliente(listaCliente, idUsuario);
                }
            }
            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                alert.alerDismiss();
              //  Toast.makeText(getApplicationContext(), (CharSequence) call,Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * Se rellena el spinner corriespondiente con los clientes asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idUsuario id obtenido en el login
     * @param listClientes  lista corresponde a los datos obtenidos en "consultaClientes"
     */
    public void cargaCliente(ArrayList<String> listClientes, final String idUsuario){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listClientes);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCliente.setAdapter(adaptadorCliente);
        spinnerCliente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idClienteSel = 0;
                for (ResCliente data : resCliente.getDatos().getResCliente()) {
                    if(data.getDescripcion().equals(item)){
                        idClienteSel = data.getIdCliente();
                        nomCliente = item;
                        idCliente = String.valueOf(idClienteSel);
                        urlImg = data.getFotoCliente();
                        break;
                    }
                }
                alert.alertShow();
                if(idClienteSel != 0){
                    consultaProyecto(idClienteSel, idUsuario);
                }else {
                    Toasty.warning(getApplicationContext(),getString(R.string.no_clientes), Toasty.LENGTH_LONG).show();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //  Toast.makeText(getApplicationContext(),"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Realiza una petición a la base de datos para obtener los proyectos asociados al usuario en relación al cliente seleccionado
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idCliente se obtiene en base al cliente seleccionado
     * @param idUsuario obtenido en el login
     */
    public void consultaProyecto(final int idCliente, final String idUsuario) {
        final ArrayList<String> listaProyectos = new ArrayList<>();
        reqProyecto = ApiService.getApiService(getApplicationContext()).getProyecto(idUsuario, String.valueOf(idCliente), token);
        reqProyecto.enqueue(new Callback<Proyecto>() {
            @Override
            public void onResponse(Call<Proyecto> call, Response<Proyecto> response) {
                if(response.isSuccessful()){
                    resProyecto = response.body();
                    boolean validaProyecto = true;
                    for(ResProyecto data : resProyecto.getDatos().getResultado()){
                        if(data.getIdProyecto() > 0) {
                            listaProyectos.add(data.getDescripcion());
                        }else{
                            validaProyecto = false;
                            break;
                        }
                    }
                    if(validaProyecto){
                        cargaProyectos(listaProyectos, idCliente, idUsuario);
                    }else{
                        Toasty.warning(getApplicationContext(),getString(R.string.no_proyectos), Toasty.LENGTH_LONG).show();
                        alert.alerDismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<Proyecto> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }


    /**
     * Se rellena el spinner corriespondiente con los proyectos asociados al usuario
     * @author: Erick Oyarce
     * @version:14/07/2020
     * @param listProyectos corresponde a los datos obtenidos en "consultaProyecto"
     */
    public void cargaProyectos(ArrayList<String> listProyectos, final int idCliente, final String idUsuario){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listProyectos);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProyecto.setAdapter(adaptadorCliente);
        spinnerProyecto.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

            @Override public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idProyecto = 0;
                for (ResProyecto data : resProyecto.getDatos().getResultado()) {
                    if(data.getDescripcion().equals(item)){
                        idProyecto = data.getIdProyecto();
                        nomProyecto = item;
                        idProyectoSel = String.valueOf(idProyecto);
                        break;
                    }
                }
                if(idProyecto != 0){
                   consultaLocacion(idCliente,idUsuario,idProyecto);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //   Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Realiza una petición a la base de datos para obtener las loccaciones asociados al usuario en relación al proyecto seleccionado
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idCliente se obtiene en base al cliente seleccionado
     * @param idUsuario obtenido en el login
     * @param idProyecto obtenido en base al proyecto seleccionado
     */
    public void consultaLocacion(final int idCliente, final String idUsuario, final int idProyecto){
        reqLocacion = ApiService.getApiService(getApplicationContext()).getLocacion(idUsuario, String.valueOf(idCliente), String.valueOf(idProyecto), token);
        reqLocacion.enqueue(new Callback<Locacion>() {
            @Override
            public void onResponse(Call<Locacion> call, Response<Locacion> response) {
                if(response.isSuccessful()){
                    resLocacion = response.body();
                    boolean locacionValida = true;
                    ArrayList<String> listLocacion = new ArrayList<>();
                    for (ResLocacion data : resLocacion.getDatos().getResultado()) {
                        if(data.getIdLocacion() > 0){
                            listLocacion.add(data.getDescripcion());
                        }else {
                            locacionValida = false;
                            break;
                        }
                    }
                    if (locacionValida){
                        cargaLocaciones(listLocacion, idCliente, idUsuario, idProyecto);
                    }else {
                        Toasty.warning(getApplicationContext(),getString(R.string.no_locaciones), Toasty.LENGTH_LONG).show();
                        alert.alerDismiss();
                    }
                }
            }
            @Override
            public void onFailure(Call<Locacion> call, Throwable t) {
                alert.alerDismiss();
            }
        });
    }


    /**
     * Se rellena el spinner corriespondiente con las locaciones asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param listLocacion corresponde a los datos obtenidos en "consultaLocacion"
     */
    public void cargaLocaciones(ArrayList<String> listLocacion, final int idCliente, final String idUsuario, final int idProyecto){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listLocacion);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocacion.setAdapter(adaptadorCliente);
        spinnerLocacion.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

            @Override public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idLocacion = 0;
                for (ResLocacion data : resLocacion.getDatos().getResultado()) {
                    if(data.getDescripcion().equals(item)){
                        idLocacion = data.getIdLocacion();
                        nomLocacion = item;
                        idLocacionSel =  String.valueOf(idLocacion);
                        break;
                    }
                }
                if(idLocacion != 0) {
                    consultaArea(idCliente, idUsuario,idProyecto,idLocacion);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //   Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Realiza una petición a la base de datos para obtener las areas asociados al usuario en relación a la locacion seleccionado
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param idCliente se obtiene en base al cliente seleccionado
     * @param idUsuario obtenido en el login
     * @param idProyecto obtenido en base al proyecto seleccionado
     * @param  idLocacion obtenido en base a la locacion seleccionada
     */
    public void consultaArea(int idCliente, String idUsuario, int idProyecto, int idLocacion){
        reqArea = ApiService.getApiService(getApplicationContext()).getArea(idUsuario, String.valueOf(idCliente), String.valueOf(idProyecto), String.valueOf(idLocacion), token);
        reqArea.enqueue(new Callback<Area>() {
            @Override
            public void onResponse(Call<Area> call, Response<Area> response) {
                if(response.isSuccessful()){
                    resArea = response.body();
                    boolean areaValida = true;
                    ArrayList<String> listArea = new ArrayList<>();
                    for(ResArea data : resArea.getDatos().getResultado()){
                        if(data.getIdArea() > 0){
                            listArea.add(data.getDescripcion());
                        }else {
                            areaValida = false;
                            break;
                        }
                    }
                    alert.alerDismiss();
                    if (areaValida){
                        btnIngresar.setVisibility(View.VISIBLE);
                        cargaAreas(listArea);
                    }else {
                        Toasty.warning(getApplicationContext(),getString(R.string.no_areas), Toasty.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<Area> call, Throwable t) {
                alert.alerDismiss();
                Log.d("Ubicaciones", String.valueOf(call));
                Log.d("Ubicaciones", String.valueOf(t));
            }
        });
    }

    /**
     * Se rellena el spinner corriespondiente con las Areas asociados al usuario
     * @author: Erick Oyarce
     * @version: 14/07/2020
     * @param listAreas corresponde a los datos obtenidos en "consultaAreas"
     */
    public void cargaAreas(ArrayList<String> listAreas){
        ArrayAdapter<String> adaptadorCliente = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listAreas);
        adaptadorCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerArea.setAdapter(adaptadorCliente);
        spinnerArea.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

            @Override public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String item = String.valueOf(parent.getItemAtPosition(position));
                int idArea = 0;
                for(ResArea data : resArea.getDatos().getResultado()){
                    if(data.getDescripcion().equals(item)){
                        idArea = data.getIdArea();
                        nomArea = item;
                        idAreaSel = String.valueOf(idArea);
                        break;
                    }
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
                //   Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnIngresar:
                gruardaConfiguracion();
                startActivity(new Intent(UbicacionesActivity.this, MainActivity.class));
                finish();
                break;

            case R.id.btn_regresar:
                startActivity(new Intent(UbicacionesActivity.this, Login.class));
                finish();
                break;
        }
    }
    /** Al guardar los datos se aplican los cambios **/
    public void gruardaConfiguracion(){

        Local.setData("login", getApplicationContext(), "nomCliente", nomCliente);
        Local.setData("login", getApplicationContext(), "idCLiente", idCliente);

        Local.setData("login", getApplicationContext(), "nomProyecto", nomProyecto);
        Local.setData("login", getApplicationContext(), "idProyecto",idProyectoSel);

        Local.setData("login", getApplicationContext(), "nomLocacion", nomLocacion);
        Local.setData("login", getApplicationContext(), "idLocacion",idLocacionSel);

        Local.setData("login", getApplicationContext(), "nomArea", nomArea);
        Local.setData("login", getApplicationContext(), "idArea", idAreaSel);

        Local.setData("login", getApplicationContext(), "imgCliente", urlImg);
    }
}
