package com.glgroup.centurycloudmovil.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.glgroup.centurycloudmovil.Controllers.Firebase.MyReceiver;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Constantes;

import es.dmoral.toasty.Toasty;


public class MainUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    MyReceiver objMyReceiver;
    Button btn_descarga, continuar;
    String url, version, urlSet;
    TextView mensaje;
    ProgressBar progressBar;

    private final int PHONE_CHECK_CODE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_update);

        urlSet = getIntent().getStringExtra(Constantes.PARAMETRO_ACT);

        btn_descarga = (Button) findViewById(R.id.update);
        btn_descarga.setOnClickListener(this);

        progressBar = (ProgressBar)findViewById(R.id.my_progressBar);

        continuar = (Button)findViewById(R.id.continuar);
        continuar.setOnClickListener(this);

        mensaje = (TextView)findViewById(R.id.msj);

        init();
    }

    private void init(){

        objMyReceiver = new MyReceiver(MainUpdateActivity.this);
        objMyReceiver.registrar(objMyReceiver);

    }

    @Override
    protected void onPause() {
        super.onPause();
        objMyReceiver.borrarRegistro(objMyReceiver);
        //si el usuario cancela la instalación de la aplicación, se vuelve al login
        //startActivity(new Intent(MainUpdateActivity.this, MainLoginActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        objMyReceiver.registrar(objMyReceiver);
       // startActivity(new Intent(MainUpdateActivity.this, MainLoginActivity.class));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.update:
                System.out.println("url: "+urlSet);

                if (urlSet == null || urlSet.equals("")) {
                    Toasty.warning(getApplicationContext(), getString(R.string.error_actualizacion), Toasty.LENGTH_SHORT).show();
                } else {
                    //se comprueba si los permisos fueron acpetados por el usuario
                    if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            //nuevas versiones
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PHONE_CHECK_CODE);
                        } else {
                            //versiones antiguas
                            IngresarActualizacion();
                        }
                    }else{
                       IngresarActualizacion();
                    }
                }

                break;

            case R.id.continuar:
                startActivity(new Intent(MainUpdateActivity.this, Login.class));
                break;
        }

    }

    //método que se ejecuta para la actualizacion
    public void IngresarActualizacion(){
        progressBar.setVisibility(View.VISIBLE);
        mensaje.setVisibility(View.VISIBLE);
        btn_descarga.setEnabled(false);
        continuar.setEnabled(false);
        objMyReceiver.Descargar(urlSet, getApplicationContext());
    }




    //Método de escucha para validar la respuesta del usuario ante la solicitud de permisos
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){

            case PHONE_CHECK_CODE:

                String permiso = permissions[0];
                String permiso2 = permissions[1];
                int result = grantResults[0];
                int result2 = grantResults[1];

                if(permiso.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && permiso2.equals(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    //Comprobar si se acepto la petición
                    if(result == PackageManager.PERMISSION_GRANTED){
                        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        IngresarActualizacion();
                    }else{
                        //No se concedió el permiso
                        Toast.makeText(getApplicationContext(), "Permiso denegado", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainUpdateActivity.this, Login.class));
                    }
                }

                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}