package com.glgroup.centurycloudmovil.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.KeyguardManager;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.glgroup.centurycloudmovil.Fragments.LoginCredentialFragment;
import com.glgroup.centurycloudmovil.Fragments.LoginFingerFragment;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;



public class Login extends AppCompatActivity {
    KeyguardManager keyguardManager;
   // FingerprintManager fingerprintManager;

    String TAG = "PANTALLA";


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);


//        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        LoginFingerFragment fingerFragment = new LoginFingerFragment();
        LoginCredentialFragment credentialFragment = new LoginCredentialFragment();

        /** se valida si el equipo cuenta con lector de huellas, para cargar un tipo de inicio u otro **/
        if(Local.getData("acceso", getApplicationContext(),"finger").equals("1")) {
//            if (!fingerprintManager.isHardwareDetected()) {
            if (true) {
                fragmentTransaction.add(R.id.frameContent, credentialFragment);
                fragmentTransaction.commit();
            } else {
                fragmentTransaction.add(R.id.frameContent, fingerFragment);
                fragmentTransaction.commit();
            }
        }else{
            fragmentTransaction.add(R.id.frameContent, credentialFragment);
            fragmentTransaction.commit();
        }

        /** mide los detalles de la pantalla **/
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;
        Log.i(TAG, "Ancho en píxeles  = " + widthPixels);
        Log.i(TAG, "Alto en píxeles   = " + heightPixels);
        Log.i(TAG, "Densidad dpi      = " + densityDpi);
        Log.i(TAG, "x dpi             = " + xdpi);
        Log.i(TAG, "y dpi             = " + ydpi);

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        Log.i(TAG,dpHeight +"------" +dpWidth);
    }

    /** permite acceder al envío de correo **/
    public void email(View v) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "soporte@glgroup.cl", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Ayuda - Century Cloud Movil");
        startActivity(Intent.createChooser(emailIntent, "Enviar email"));

    }

    /** lleva a la web de glgroup **/
    public void web(View v) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse("https://centurycloud.net/"));
        startActivity(intent);
    }

    /** indica la ubicación de glgroup **/
    public void mapa(View v) {
        Uri intentUri = Uri.parse("geo:-33.407250,-70.638712?z=8&q=-33.407250,-70.638712(Esta+Es+La+Etiqueta)");
        Intent intent = new Intent(Intent.ACTION_VIEW, intentUri);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish(); finishAffinity(); System.exit(0);
        super.onBackPressed();
    }
}
