package com.glgroup.centurycloudmovil.ui;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Fragments.Configuracion.AyudaFragment;
import com.glgroup.centurycloudmovil.Fragments.Configuracion.ConfiguracionFragment;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Fragments.Desenlace.DesenlaceFragment;
import com.glgroup.centurycloudmovil.Fragments.Enlace.EnlaceFragment;
import com.glgroup.centurycloudmovil.Fragments.Entrada.DocumentosEntradaFragment;
import com.glgroup.centurycloudmovil.Fragments.Inventario.ListaInventariosFragment;
import com.glgroup.centurycloudmovil.Fragments.Movimiento.MovimientoFragment;
import com.glgroup.centurycloudmovil.Fragments.Salida.DocumentosSalidaFragment;
import com.glgroup.centurycloudmovil.Models.Application.PermisosBotones.PermisoBtn;
import com.glgroup.centurycloudmovil.Models.Application.PermisosBotones.ResPermisoBtn;
import com.glgroup.centurycloudmovil.Models.Firebase.ConexionCentury;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Alertas;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.FileUtils;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.Utils.Mensajes;
import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.AppDatabase;
import com.glgroup.centurycloudmovil.Controllers.ROOMSQLite.Entity.PermisoBD;
import com.glgroup.centurycloudmovil.Utils.SPUtils;
import com.glgroup.centurycloudmovil.Utils.Utils;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.rscja.deviceapi.RFIDWithUHFBLE;
import com.rscja.deviceapi.RFIDWithUHFUART;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.ConnectionStatusCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {



    private DrawerLayout drawerLayout;
    private NavigationView navigationView;


    /**peticion retrofit **/
    private Call<PermisoBtn> reqPermiso;

    /** respuesta retrofit **/
    private PermisoBtn resPermiso;

    /** base de datos **/
    private AppDatabase bd;

    /** FIREBASE CloudFirestor **/
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private DocumentReference docReference = db.document("Conexion/CenturyCloud");
    private int inicioApp = 1;


    public boolean isScanning = false;
    public String remoteBTName = "";
    public String remoteBTAdd = "";
    private final static String TAG = "MainActivity";
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_SELECT_DEVICE = 1;

    public BluetoothDevice mDevice = null;
    public TextView txtInfo, infoUbicacion;
    public BluetoothAdapter mBtAdapter = null;
    public RFIDWithUHFBLE uhf = RFIDWithUHFBLE.getInstance();
    BTStatus btStatus = new BTStatus();



    public boolean mIsActiveDisconnect = true; // 是否主动断开连接
    private static final int RECONNECT_NUM = Integer.MAX_VALUE; // 重连次数
    private int mReConnectCount = RECONNECT_NUM; // 重新连接次数

    private Timer mDisconnectTimer = new Timer();
    private DisconnectTimerTask timerTask;
    private long timeCountCur; // 断开时间选择
    private long period = 1000 * 30; // 隔多少时间更新一次
    private long lastTouchTime = System.currentTimeMillis(); // 上次接触屏幕操作的时间戳

    private static final int RUNNING_DISCONNECT_TIMER = 10;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case RUNNING_DISCONNECT_TIMER:
                    long time = (long) msg.obj;
                    formatConnectButton(time);
                    break;
            }
        }
    };

    private Toast toast;

    public void showToast(String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showToast(int resId) {
        showToast(getString(resId));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if(Local.getData("theme",getApplicationContext(), "tema").equals("1")){
//            setTheme(R.style.AppTheme2);
//        }else{
//            setTheme(R.style.AppTheme);
//        }
        /** inicializar la base de datos **/
        bd = Room.databaseBuilder(getApplicationContext(),AppDatabase.class, Constantes.BD_NAME)
                .allowMainThreadQueries()
                .build();

        obtenerPermisos();

        setContentView(R.layout.activity_main);

        checkLocationEnable();
        uhf.init(getApplicationContext());
        Utils.initSound(getApplicationContext());
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        setToolbar();
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nvNavigationView);

        txtInfo = findViewById(R.id.txtInfo);

        infoUbicacion = findViewById(R.id.marqueeMain);
        cargaMarquee();
        cargarImagen();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                boolean doChange=false;
                Fragment fragment = null;
                int permiso = 0;
                switch (menuItem.getItemId()){
                    case R.id.itHome:
                        doChange=true;
                        fragment = new DefaultFragment();
                        break;
                    case R.id.itEnlace:
                        /** se validan los permisos del boton **/
                        if(bd.permisoDao().sp_Sel_Permiso(1) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(1).getPermiso(); }
                        if(permiso == 1) {
                            doChange = true;
                            fragment = new EnlaceFragment();
                        }else{
                            Mensajes.errorPermiso(getApplicationContext());
                            }
                        break;
                    case R.id.itDesenlace:
                        /** se validan los permisos del boton **/
                        if(bd.permisoDao().sp_Sel_Permiso(2) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(2).getPermiso(); }
                        if(permiso == 1) {
                            doChange = true;
                            fragment = new DesenlaceFragment();
                        }else{
                            Mensajes.errorPermiso(getApplicationContext());
                        }
                        break;

                    case R.id.itEntrada:
                        /** se validan los permisos del boton **/
                        if(bd.permisoDao().sp_Sel_Permiso(4) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(4).getPermiso(); }
                        if(permiso == 1) {
                            doChange = true;
                            fragment = new DocumentosEntradaFragment();
                        }else{
                            Mensajes.errorPermiso(getApplicationContext());
                        }
                        break;

                    case R.id.itSalida:
                        /** se validan los permisos del boton **/
                        if(bd.permisoDao().sp_Sel_Permiso(3) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(3).getPermiso(); }
                        if(permiso == 1) {
                            doChange = true;
                            fragment = new DocumentosSalidaFragment();
                        }else{
                            Mensajes.errorPermiso(getApplicationContext());
                        }
                        break;

                    case R.id.itMovimiento:
                        /** se validan los permisos del boton **/
                        if(bd.permisoDao().sp_Sel_Permiso(5) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(5).getPermiso(); }
                        if(permiso == 1) {
                            doChange = true;
                            fragment = new MovimientoFragment();
                        }else{
                            Mensajes.errorPermiso(getApplicationContext());
                        }
                        break;

                    case R.id.itInventario:
                        /** se validan los permisos del boton **/
                        if(bd.permisoDao().sp_Sel_Permiso(6) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(6).getPermiso(); }
                        if(permiso == 1) {
                            doChange = true;
                            fragment = new ListaInventariosFragment();
                        }else{
                            Mensajes.errorPermiso(getApplicationContext());
                        }
                        break;

//                    case R.id.itBusqueda:
//                        if(bd.permisoDao().sp_Sel_Permiso(7) != null){ permiso = bd.permisoDao().sp_Sel_Permiso(7).getPermiso(); }
//                        if(permiso == 1) {
//                            Alertas.alertBusqueda(getApplicationContext(), getLayoutInflater(), MainActivity.this);
//                        }else{
//
//                        }
//                        break;


                    case R.id.itPreferencias:
                        doChange = true;
                        fragment = new ConfiguracionFragment();

                        break;

                    case R.id.itAyuda:
                        doChange=true;
                        fragment = new AyudaFragment();
                        break;

                    case R.id.itLogout:
                        if(isScanning){
                            uhf.stopInventory();
                        }
                        if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                            disconnect(true);
                        }
                        startActivity(new Intent(MainActivity.this, Login.class));
                        finish();
                        break;
                }
                if (doChange){
                    uncheckItems(navigationView);
                    changeFragment(fragment,menuItem);
                }
                return true;
            }
        });

        setDefaultFragment();
    }

    /** carga la ubicación actual en la parte inferior **/
    public void cargaMarquee(){
        String cliente = Local.getData("login", getApplicationContext(),"nomCliente");
        String proyecto = Local.getData("login", getApplicationContext(),"nomProyecto");
        String locacion = Local.getData("login", getApplicationContext(),"nomLocacion");
        String area = Local.getData("login", getApplicationContext(),"nomArea");
        infoUbicacion.setText(cliente+" - "+proyecto+" - "+locacion+" - "+area);
        infoUbicacion.setSelected(true);
    }

    /** se cambia la imagen de la tienda en el navigationdrawer **/
    public void cargarImagen(){
        View header = navigationView.getHeaderView(0);
        ImageView imgHeader = header.findViewById(R.id.imageView);
        Glide.with(getApplicationContext()).load("https://centurycloud.net/Resources"+Local.getData("login", getApplicationContext(), "imgCliente")).into(imgHeader);
    }

    /** se obtienen los permisos asociados al usuario **/
    private void obtenerPermisos(){
        String token = Local.getData("login", getApplicationContext(), "token");
        String usuario = Local.getData("login", getApplicationContext(), "idUsuario");
        reqPermiso = ApiService.getApiService(getApplicationContext()).getpermisos(usuario,"1", "0", token);
        reqPermiso.enqueue(new Callback<PermisoBtn>() {
            @Override
            public void onResponse(Call<PermisoBtn> call, Response<PermisoBtn> response) {
                if(response.isSuccessful()){
                    resPermiso = response.body();
                    for(ResPermisoBtn data : resPermiso.getDatos().getResultado()){
                        PermisoBD obj = new PermisoBD(data.getIdBoton(), data.getDescripcion(), data.getPermiso());

                        /** se valida la existencia del permiso **/
                        if(bd.permisoDao().sp_Sel_Descripcion(data.getIdBoton()) == null){
                            /** insertar en la base de datos **/
                            bd.permisoDao().sp_Ins_Permiso(obj);
                        }else{
                            /** se actualiza el permiso **/
                            bd.permisoDao().sp_Upd_Permiso(data.getPermiso(), data.getIdBoton());
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<PermisoBtn> call, Throwable t) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void uncheckItems(NavigationView navigationView){
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    private void setDefaultFragment(){
        changeFragment(new DefaultFragment(), null);
        getSupportActionBar().setTitle("");
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            if(navigationView.getMenu().getItem(i).getTitle().toString().equals(getString(R.string.inicio))) {
                navigationView.getMenu().getItem(i).setChecked(true);
            }
        }
    }

    /** permite cambiar el fragment de la vista en base al menú **/
    private void changeFragment(Fragment fragment, MenuItem item){
        if(isScanning){
            uhf.stopInventory();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame,fragment)
                .addToBackStack(null)
                .commit();
        if (item!=null){
            item.setChecked(true);
            if(item.getTitle().toString().equals(getString(R.string.inicio))){
                getSupportActionBar().setTitle("");
            }else {
                getSupportActionBar().setTitle(item.getTitle());
            }
        }
        drawerLayout.closeDrawers();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                // Código necesario para desplegar el Navigation Drawer.
                drawerLayout.openDrawer(GravityCompat.START);
                return false;

            case R.id.action_bluetooth:
                if (isScanning) {
                    showToast(R.string.title_stop_read_card);
                } else if (uhf.getConnectStatus() == ConnectionStatus.CONNECTING) {
                    showToast(R.string.connecting);
                } else {
                    showBluetoothDevice(false);
                }
                break;

            case R.id.action_conection:
                if (isScanning) {
                    showToast(R.string.title_stop_read_card);
                } else if (uhf.getConnectStatus() == ConnectionStatus.CONNECTING) {
                    showToast(R.string.connecting);
                } else if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    disconnect(true);
                } else {
                   // showBluetoothDevice(true);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /** SDK ***/
    private void formatConnectButton(long disconnectTime) {
        if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            if (!isScanning && System.currentTimeMillis() - lastTouchTime > 1000 * 30 && timerTask != null) {
                long minute = disconnectTime / 1000 / 60;
                if(minute > 0) {
                    txtInfo.setText(getString(R.string.disConnectForMinute, minute)); //倒计时分
                } else {
                   // btn_connect.setText(getString(R.string.disConnectForSecond, disconnectTime / 1000)); // 倒计时秒
                }
            } else {
              //  btn_connect.setText(R.string.disConnect);
            }
        } else {
           // btn_connect.setText("R.string.Connect");
        }
    }

    /**
     * 重置断开时间
     */
    public void resetDisconnectTime() {
        timeCountCur = SPUtils.getInstance(getApplicationContext()).getSPLong(SPUtils.DISCONNECT_TIME, 0);
        if (timeCountCur > 0) {
            formatConnectButton(timeCountCur);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        lastTouchTime = System.currentTimeMillis();
        resetDisconnectTime();
        return super.dispatchTouchEvent(ev);
    }

    /** resultado del dispositivo bluetooth seleccionado **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                        disconnect(true);
                    }
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);
                    txtInfo.setTextColor(Color.parseColor("#afb42b"));
                    txtInfo.setText(String.format("%s(%s)"+getString(R.string.conectado), mDevice.getName(), deviceAddress));
                    Local.setData("dispositivo", getApplicationContext(), "nombre",mDevice.getName());
                    connect(deviceAddress);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    showToast("Bluetooth has turned on ");
                } else {
                    showToast("Problem in BT Turning ON ");
                }
                break;
            default:
                break;
        }
    }

    /** se solicita mostrar la lista de bluetooth **/
    private void showBluetoothDevice(boolean isHistory) {
        if (mBtAdapter == null) {
            showToast("Bluetooth is not available");
            return;
        }
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onClick - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
            newIntent.putExtra(Constantes.SHOW_HISTORY_CONNECTED_LIST, isHistory);
            startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
            cancelDisconnectTimer();
        }
    }

    public void connect(String deviceAddress) {
        if (uhf.getConnectStatus() == ConnectionStatus.CONNECTING) {
            showToast(R.string.connecting);
        } else {
            uhf.connect(deviceAddress, btStatus);
        }
    }

    public void disconnect(boolean isActiveDisconnect) {
        cancelDisconnectTimer();
        mIsActiveDisconnect = isActiveDisconnect; // 主动断开为true
        uhf.disconnect();
    }


    /**
     * 重新连接
     *
     * @param deviceAddress
     */
    private void reConnect(String deviceAddress) {
        if (!mIsActiveDisconnect && mReConnectCount > 0) {
            connect(deviceAddress);
            mReConnectCount--;
        }
    }

    /**
     * 应该提示未连接状态
     *
     * @return
     */
    private boolean shouldShowDisconnected() {
        return mIsActiveDisconnect || mReConnectCount == 0;
    }

    /** estado del bluetooth**/
    class BTStatus implements ConnectionStatusCallback<Object> {
        @Override
        public void getStatus(final ConnectionStatus connectionStatus, final Object device1) {
            runOnUiThread(new Runnable() {
                public void run() {
                    BluetoothDevice device = (BluetoothDevice) device1;
                    remoteBTName = "";
                    remoteBTAdd = "";
                    if (connectionStatus == ConnectionStatus.CONNECTED) {
                        remoteBTName = device.getName();
                        remoteBTAdd = device.getAddress();

                        txtInfo.setTextColor(Color.parseColor("#FF4ABD00"));
                        txtInfo.setText(String.format("%s(%s)"+getString(R.string.conectado), remoteBTName, remoteBTAdd));
                        Local.setData("dispositivo", getApplicationContext(), "nombre", remoteBTName);
                        if (shouldShowDisconnected()) {
                          //  showToast(R.string.connect_success);
                        }

                        timeCountCur = SPUtils.getInstance(getApplicationContext()).getSPLong(SPUtils.DISCONNECT_TIME, 0);
                        if (timeCountCur > 0) {
                            startDisconnectTimer(timeCountCur);
                        } else {
                            formatConnectButton(timeCountCur);
                        }

                        // 保存已链接记录
                        if (!TextUtils.isEmpty(remoteBTAdd)) {
                            saveConnectedDevice(remoteBTAdd, remoteBTName);
                        }


                        mIsActiveDisconnect = false;
                        mReConnectCount = RECONNECT_NUM;
                    } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                        cancelDisconnectTimer();
                        formatConnectButton(timeCountCur);
                        if (device != null) {
                            remoteBTName = device.getName();
                            remoteBTAdd = device.getAddress();
//                            if (shouldShowDisconnected())
                            txtInfo.setTextColor(Color.parseColor("#f44336"));
                            txtInfo.setText(String.format("%s(%s) "+getString(R.string.desconectado), remoteBTName, remoteBTAdd));
                        } else {
//                            if (shouldShowDisconnected())
                            txtInfo.setText("disconnected");
                        }
                        if (shouldShowDisconnected()){}
                           // showToast(R.string.disConnect);

                        boolean reconnect = SPUtils.getInstance(getApplicationContext()).getSPBoolean(SPUtils.AUTO_RECONNECT, false);
                        if (mDevice != null && reconnect) {
                            reConnect(mDevice.getAddress()); // 重连
                        }
                    }

                    for (IConnectStatus iConnectStatus : connectStatusList) {
                        if (iConnectStatus != null) {
                            iConnectStatus.getStatus(connectionStatus);
                        }
                    }
                }
            });
        }
    }

    public void saveConnectedDevice(String address, String name) {
        List<String[]> list = FileUtils.readXmlList();
        for (int k = 0; k < list.size(); k++) {
            if (address.equals(list.get(k)[0])) {
                list.remove(list.get(k));
                break;
            }
        }
        String[] strArr = new String[]{address, name};
        list.add(0, strArr);
        FileUtils.saveXmlList(list);
    }



    public void updateConnectMessage(String oldName, String newName) {
        if (!TextUtils.isEmpty(oldName) && !TextUtils.isEmpty(newName)) {
          //  tvAddress.setText(tvAddress.getText().toString().replace(oldName, newName));
            remoteBTName = newName;
        }
    }

    private List<IConnectStatus> connectStatusList = new ArrayList<>();

    public void addConnectStatusNotice(IConnectStatus iConnectStatus) {
        connectStatusList.add(iConnectStatus);
    }

    public void removeConnectStatusNotice(IConnectStatus iConnectStatus) {
        connectStatusList.remove(iConnectStatus);
    }

    public interface IConnectStatus {
        void getStatus(ConnectionStatus connectionStatus);
    }


    private static final int ACCESS_FINE_LOCATION_PERMISSION_REQUEST = 100;
    private static final int REQUEST_ACTION_LOCATION_SETTINGS = 3;

    public int checkLocationEnable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_PERMISSION_REQUEST);
            }
        }
        if (!isLocationEnabled()) {
            Utils.alert(this, R.string.get_location_permission, getString(R.string.tips_open_the_ocation_permission), R.drawable.webtext, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, REQUEST_ACTION_LOCATION_SETTINGS);

                }
            });
            return 2;
        }else{
            return 1;
        }
    }

    /** valida si el acceso a la locacion gps esta activa **/
    private boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    private void startDisconnectTimer(long time) {
        timeCountCur = time;
        timerTask = new DisconnectTimerTask();
        mDisconnectTimer.schedule(timerTask, 0, period);
    }

    public void cancelDisconnectTimer() {
        timeCountCur = 0;
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
    }

    /** tiempo de conexión **/
    private class DisconnectTimerTask extends TimerTask {
        @Override
        public void run() {
            Log.e(TAG, "timeCountCur = " + timeCountCur);
            Message msg = mHandler.obtainMessage(RUNNING_DISCONNECT_TIMER, timeCountCur);
            mHandler.sendMessage(msg);
            if(isScanning) {
                resetDisconnectTime();
            } else if (timeCountCur <= 0){
                disconnect(true);
            }
            timeCountCur -= period;
        }
    }
    /** da una acción al botón de retroceso **/
    @Override public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (!(fragment instanceof IOnBackPressed) || !((IOnBackPressed) fragment).onBackPressed()) {
            Log.d("fragment", String.valueOf(fragment));
            /** si la vista actual no es el fragment default, volver a la vista anterior **/
            if (!String.valueOf(fragment).contains("DefaultFragment")){
                super.onBackPressed();
            }

        }
    }

    /** permite usar el onbackpressed del activity en un fragment **/
    public interface IOnBackPressed {
        boolean onBackPressed();
    }



    /***
     * Metodo que permiten mostrar los cambios en tiempo real
     * ****/
    @Override
    protected void onStart() {
        super.onStart();

            //TODO se cargan documentos desde una ruta especifica
            docReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                    if(inicioApp > 1) {
                        if (e != null) {
                         //   Toast.makeText(getApplicationContext(), "Error al cargar!", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, e.toString());
                            return;
                        }
                        ConexionCentury conexionCentury = documentSnapshot.toObject(ConexionCentury.class);
                        if (conexionCentury != null) {
                            if(!Local.getData("conexion", getApplicationContext(), "url").equals(conexionCentury.getUrl())){
                                Alertas.cambioDatos(MainActivity.this);
                            }
                            if(!Local.getData("conexion", getApplicationContext(), "puerto").equals(conexionCentury.getPuerto())){
                                Alertas.cambioDatos(MainActivity.this);
                            }
                        }
                        //Toast.makeText(getApplicationContext(), "cambios en la ip y url", Toast.LENGTH_SHORT).show();
                    }
                    inicioApp++;

                }
            });

    }


}
