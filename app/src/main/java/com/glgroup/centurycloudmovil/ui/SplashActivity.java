package com.glgroup.centurycloudmovil.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Models.Firebase.Actualizacion;
import com.glgroup.centurycloudmovil.Models.Firebase.ConexionCentury;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Constantes;
import com.glgroup.centurycloudmovil.Utils.InternetAcces;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class SplashActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference conexionReference;

    private Locale locale;
    private Configuration config = new Configuration();

    private TextView msg, version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        msg = findViewById(R.id.msg);

        version = findViewById(R.id.version);
        version.setText("Version: "+Constantes.VERSION_APP);

        String lang = Local.getData("lenguaje",getApplicationContext(),"lang");

        if(!lang.equals("") || lang != null) {

            locale = new Locale(lang);
            config.locale = locale;
            getResources().updateConfiguration(config, null);
        }

        new verificaConexion().execute();
    }

    public class verificaConexion extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            if (InternetAcces.isOnlineNet()) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {

                obtieneDatosConexion();

            }else{
                msg.setVisibility(View.VISIBLE);
                msg.setText(getString(R.string.error_interenet));
                //Toasty.error(getApplicationContext(), "Es necesaria una conexión de internet para usar la aplicación", Toasty.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public void obtieneDatosConexion(){
        ApiService.reiniciarApiService();
        String coll = Constantes.COLLECTION_CONEXION;
        String doc = Constantes.DOCUMENT_CONEXION;
        conexionReference = db.collection(coll);
        conexionReference.document(doc)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {

                            ConexionCentury conexionCentury = documentSnapshot.toObject(ConexionCentury.class);
                            if (conexionCentury != null) {
                                Local.setData("conexion", getApplicationContext(), "url", conexionCentury.getUrl());
                                Local.setData("conexion", getApplicationContext(), "puerto", conexionCentury.getPuerto());
                            }

                        }
                        startActivity(new Intent(SplashActivity.this, Login.class));
                        finish();
                        //validaActualizacion();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        msg.setVisibility(View.VISIBLE);
                        msg.setText(getString(R.string.error_obtener_datos));
                        //Toast.makeText(getApplicationContext(),"Error al obtener los datos, contacte al administrador del sistema", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void validaActualizacion(){
        String coll = Constantes.COLLECTION_ACTUALIZACION;
        String doc = Constantes.DOCUMENT_ACTUALIZACION;
        conexionReference = db.collection(coll);
        conexionReference.document(doc)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Actualizacion actualizacion = documentSnapshot.toObject(Actualizacion.class);
                            if (actualizacion != null) {
                                System.out.println("version: "+actualizacion.getVersion());
                                System.out.println("urk: "+actualizacion.getUrl());
                               if(Constantes.VERSION_APP.equals(actualizacion.getVersion())){
                                   startActivity(new Intent(SplashActivity.this, Login.class));
                                   finish();
                               }else{
                                   Intent intent = new Intent(SplashActivity.this, MainUpdateActivity.class);
                                   intent.putExtra(Constantes.PARAMETRO_ACT, actualizacion.getUrl());
                                   startActivity(intent);
                                   finish();
                               }
                            }
                        }else{
                            startActivity(new Intent(SplashActivity.this, Login.class));
                            finish();
                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        msg.setVisibility(View.VISIBLE);
                        msg.setText(getString(R.string.error_obtener_datos));
                        //Toast.makeText(getApplicationContext(),"Error al obtener los datos, contacte al administrador del sistema", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
