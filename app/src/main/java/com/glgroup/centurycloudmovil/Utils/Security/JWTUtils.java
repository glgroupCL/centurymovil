package com.glgroup.centurycloudmovil.Utils.Security;

import android.app.Activity;
import android.content.Context;
import android.util.Base64;

import com.glgroup.centurycloudmovil.Utils.Memory.Local;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class JWTUtils {

    public static String getHeader(String jwtEncoded) {
        try {
            String[] split = jwtEncoded.split("\\.");
            return getJson(split[0]);
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    private static String getPayload(String jwtEncoded) {
        try {
            String[] split = jwtEncoded.split("\\.");
            return getJson(split[1]);
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }


    public static int descifraToken(String token, Context context){
        int resp = 0;
        Local.setData("login", context, "token", token);
        String jwt = getPayload(token);
        try {
            JSONObject payload = new JSONObject(jwt);
            for(int i=0; i < payload.names().length(); i++) {
                String keyPayload = payload.names().getString(i);
                if(keyPayload.equals("data")){
                    String valueDat = payload.getString(keyPayload);
                    JSONArray arrayRes = new JSONArray(valueDat);
                    for(int k=0; k < arrayRes.length(); k++){
                        JSONObject obj = arrayRes.getJSONObject(k);
                        if(Integer.parseInt(obj.getString("id_usuario"))> 0){
                            resp = 1;
                        }
                        Local.setData("login", context, "idUsuario", obj.getString("id_usuario"));
                        Local.setData("login", context, "descripcion", obj.getString("descripcion"));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return resp;
    }
}
