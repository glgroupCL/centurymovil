package com.glgroup.centurycloudmovil.Utils;

import android.content.Context;
import android.widget.Toast;

import com.glgroup.centurycloudmovil.R;

import es.dmoral.toasty.Toasty;

public class Mensajes {

    private Context context;

    public Mensajes(Context context) {
        this.context = context;
    }


    public void Login(int codigo) {
        switch (codigo) {
            case -1:
                //parametros
                Toasty.warning(context, context.getString(R.string.error_interno), Toasty.LENGTH_LONG).show();
                break;
            case -2:
                //usuario o contraseña
                Toasty.warning(context, context.getString(R.string.error_usuario), Toasty.LENGTH_LONG).show();
                break;
            case -3:
                //dispositivo no registrado
                Toasty.warning(context, context.getString(R.string.dispositivo_no_registrado), Toasty.LENGTH_LONG).show();
                break;
            case -4:
                //dispositivo no asociado
                Toasty.warning(context, context.getString(R.string.dispositivo_no_asociado), Toasty.LENGTH_LONG).show();
                break;

            case -5:
                //error interno
                Toasty.error(context, context.getString(R.string.error_interno), Toasty.LENGTH_LONG).show();
                break;
        }
    }

    public static void errorPermiso(Context context){
        Toasty.warning(context, context.getString(R.string.no_tiene_permisos), Toasty.LENGTH_LONG).show();
    }
}
