package com.glgroup.centurycloudmovil.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.ApiService;
import com.glgroup.centurycloudmovil.Controllers.Retrofit.Peticiones.NombreItem;
import com.glgroup.centurycloudmovil.Fragments.Busqueda.BusquedaCenturyFragment;
import com.glgroup.centurycloudmovil.Fragments.Busqueda.BusquedaOtrasFragment;
import com.glgroup.centurycloudmovil.Fragments.DefaultFragment;
import com.glgroup.centurycloudmovil.Fragments.Desenlace.DesenlaceMasivoFragment;
import com.glgroup.centurycloudmovil.Fragments.Desenlace.DesenlaceUnicoFragment;
import com.glgroup.centurycloudmovil.Fragments.Enlace.EnlaceMasivoFragment;
import com.glgroup.centurycloudmovil.Fragments.Enlace.EnlaceUnicoFragment;
import com.glgroup.centurycloudmovil.Models.Application.ListaItem.ListaItems;
import com.glgroup.centurycloudmovil.Models.Application.ListaItem.ResListaItems;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.IdItem;
import com.glgroup.centurycloudmovil.Models.Application.NombreItem.ResIdItem;
import com.glgroup.centurycloudmovil.R;
import com.glgroup.centurycloudmovil.Utils.Memory.Local;
import com.glgroup.centurycloudmovil.ui.MainActivity;
import com.glgroup.centurycloudmovil.ui.SplashActivity;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Alertas {

    /** alert que muestra la imgen asociada a un producto **/
    public static void mostrarImagen(String url, String nombre, Context context, LayoutInflater layoutInflater) {
        AlertDialog alertImagen;
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        LayoutInflater inflater = layoutInflater;

        View dialoglayout = inflater.inflate(R.layout.image_view, null);
        final ImageView imagen = dialoglayout.findViewById(R.id.imagen);
        final TextView nombreItem = dialoglayout.findViewById(R.id.nombre);
        nombreItem.setText(nombre);
        if(url == ""){url = "https://centurycloud.net/Resources/img/sinFoto.jpg";}
        imagen.setRotation(0);
        Glide.with(context).load(url).into(imagen);

        builder.setView(dialoglayout);
        alertImagen = builder.create();
        alertImagen.setCancelable(true);
        alertImagen.show();
    }


    /** muestra un alert dialog con las opciones para fijar un item -  NO PREGUNTEN, SOLO APLIQUEN **/
    public static void fijarItem(final Context context, LayoutInflater layoutInflater, final TextView nombre_item, final int vista) {

        final String idItem = Local.getData("enlace", context, "idItem");
        final String token = Local.getData("login", context, "token");
        final String usuario = Local.getData("login", context, "idUsuario");
        final String cliente = Local.getData("login", context, "idCLiente");
        final String proyecto = Local.getData("login", context, "idProyecto");
        final String locacion = Local.getData("login", context, "idLocacion");
        final String area = Local.getData("login", context, "idArea");

        final ArrayList<String> items = new ArrayList<String>();

        final AlertDialog alertFijar;
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        LayoutInflater inflater = layoutInflater;

        View dialoglayout = inflater.inflate(R.layout.alert_fijar_item, null);
        final ImageButton btnSearch = dialoglayout.findViewById(R.id.searchitem);
        final TextInputLayout txt_buscar = dialoglayout.findViewById(R.id.edSearch);
        final RadioButton completo = dialoglayout.findViewById(R.id.radio_completo);
        final RadioButton exacto = dialoglayout.findViewById(R.id.radio_exacto);
        final RadioButton implicito = dialoglayout.findViewById(R.id.radio_implicito);
        final ListView listaItems = dialoglayout.findViewById(R.id.listaItems);
        final TextView resultados = dialoglayout.findViewById(R.id.resultados);


        builder.setView(dialoglayout);
        alertFijar = builder.create();
        alertFijar.setCancelable(true);


        /** se validan los checks y el botón de buscar **/
        completo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(completo.isChecked()){
                    exacto.setChecked(false);
                    implicito.setChecked(false);
                }
            }
        });

        exacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(exacto.isChecked()){
                    completo.setChecked(false);
                    implicito.setChecked(false);
                }
            }
        });

        implicito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(implicito.isChecked()){
                    exacto.setChecked(false);
                    completo.setChecked(false);
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                String textobuscar = txt_buscar.getEditText().getText().toString();
                /** se valida que por lo menos uno de los check esté seleccionado **/
                if(implicito.isChecked() || exacto.isChecked() || completo.isChecked()){

                    /** si el completo está seleccionado se agrega un texto al campo de busqueda, solo por si está vacío, ya que no es requerido **/
                    if(completo.isChecked()){
                        if(textobuscar.equals("")) textobuscar = "completo";
                    }

                    /** si el campo de busqueda está vacío se solicita **/
                    if(!textobuscar.equals("")){
                        txt_buscar.setError(null);
                        //TODO revisar funcionamiento
                        Call<ListaItems> reqListaItems;
                        items.clear();

                        /** se asigna un número al tipo de busqueda a realizar (en base a los checks **/
                        String Lista = "";
                        if(implicito.isChecked()) Lista = "3";
                        if(exacto.isChecked()) Lista = "2";
                        if(completo.isChecked()) Lista = "1";

                        /** consulta a la api **/
                        reqListaItems = ApiService.getApiService(context).getlistaitems(usuario,cliente,proyecto,locacion,area,textobuscar,Lista,token);
                        reqListaItems.enqueue(new Callback<ListaItems>() {
                            @Override
                            public void onResponse(Call<ListaItems> call, Response<ListaItems> response) {
                                if(response.isSuccessful()){
                                    final ListaItems resListaItems = response.body();
                                    int validar = 0;
                                    String mensaje = "";
                                    for(ResListaItems data : resListaItems.getDatos().getResultado()){
                                        validar = data.getIdItem();
                                        mensaje = data.getItem();
                                        items.add(data.getItem());
                                    }
                                    /** si el valor id es mayor a 0, la respuesta es correcta **/
                                    if(validar > 0){
                                        resultados.setVisibility(View.VISIBLE);

                                        /**se agregan los elementos a la vista **/
                                        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,android.R.layout.simple_list_item_1, items);
                                        listaItems.setAdapter(adapter);

                                        listaItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                int item = i;
                                                String valor = (String)listaItems.getItemAtPosition(i);
                                                nombre_item.setText(valor);
                                                for(ResListaItems data : resListaItems.getDatos().getResultado()){
                                                    if(valor.equals(data.getItem()));

                                                    /** se guardan los datos en base a enlace unico o masivo para evitar confusión en las validaciones **/
                                                    if(vista == 1){
                                                        Local.setData("enlace",  context, "idItem", String.valueOf(data.getIdItem()));
                                                        Local.setData("enlace",  context, "fijarItem", "1");
                                                    }
                                                    if(vista == 2){
                                                        Local.setData("enlaceMasivo",  context, "idItem", String.valueOf(data.getIdItem()));
                                                        Local.setData("enlaceMasivo",  context, "fijarItem", "1");
                                                    }
                                                    break;
                                                }

                                                alertFijar.dismiss();

                                            }
                                        });
                                    }else{
                                        /** si la respuesta es negativa, se oculta el texto "resultado", se limpia la lista y se muestra el mensaje adjunto **/
                                        resultados.setVisibility(View.GONE);
                                        listaItems.setAdapter(null);
                                        Toasty.warning(context, mensaje, Toasty.LENGTH_LONG).show();
                                    }

                                }
                            }

                            @Override
                            public void onFailure(Call<ListaItems> call, Throwable t) {
                                System.out.println(call);
                            }
                        });

                    }else{
                        /** cuando no hay texto, se cambia el estado del edittext **/
                       resultados.setVisibility(View.GONE);
                       listaItems.setAdapter(null);
                       txt_buscar.setErrorEnabled(true);
                       txt_buscar.setError(context.getString(R.string.requerido));
                    }

                }else{
                    /** si no existe un tipo de filtro, se solicita **/
                    resultados.setVisibility(View.GONE);
                    listaItems.setAdapter(null);
                    Toasty.warning(context, context.getString(R.string.tipo_busqueda), Toasty.LENGTH_LONG).show();
                }
            }
        });

        alertFijar.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                alertFijar.dismiss();
            }
        });

        alertFijar.show();
    }


    /** muestra las observaciones de un documento **/
    public static void verObservación(String mensaje, Context context){
        androidx.appcompat.app.AlertDialog.Builder alertObservaciones = new androidx.appcompat.app.AlertDialog.Builder(context);
        alertObservaciones.setTitle(context.getString(R.string.observaciones));
        alertObservaciones.setMessage(mensaje);
        alertObservaciones.setCancelable(false);
        alertObservaciones.setPositiveButton(context.getString(R.string.confirmar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.dismiss();
            }
        });
        alertObservaciones.show();
    }


    /** cierre correcto de un documento **/
    public static void alertCorrecto(String tit, String msg, Context context, LayoutInflater layoutInflater, final MainActivity mainActivity){
        final android.app.AlertDialog alertCorrecto;
        final android.app.AlertDialog.Builder correcto = new android.app.AlertDialog.Builder(context);


        View dialog = layoutInflater.inflate(R.layout.alert_cierrecorrecto_doc, null);

        final Button ok = dialog.findViewById(R.id.btn_send);
        final TextView titulo = dialog.findViewById(R.id.txt_title);
        final TextView mensaje = dialog.findViewById(R.id.mensaje);

        titulo.setText(tit);
        mensaje.setText(msg);

        correcto.setView(dialog);

        alertCorrecto = correcto.create();
        alertCorrecto.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCorrecto.dismiss();
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new DefaultFragment())
                        .commit();
            }
        });
        alertCorrecto.show();
    }

    /** cierre incorrecto de un documento **/
    public static void alertIncorrecto(String tit, String msg, Context context, LayoutInflater layoutInflater, final MainActivity mainActivity){
        final android.app.AlertDialog alertIncorrecto;
        final android.app.AlertDialog.Builder incorrecto = new android.app.AlertDialog.Builder(context);


        View dialog = layoutInflater.inflate(R.layout.alert_cierreincorrecto_doc, null);

        final Button ok = dialog.findViewById(R.id.btn_send);
        final Button volver = dialog.findViewById(R.id.btn_regresar);
        final TextView titulo = dialog.findViewById(R.id.txt_title);
        final TextView mensaje = dialog.findViewById(R.id.mensaje);

        titulo.setText(tit);
        mensaje.setText(msg);

        incorrecto.setView(dialog);

        alertIncorrecto = incorrecto.create();
        alertIncorrecto.setCancelable(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertIncorrecto.dismiss();
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new DefaultFragment())
                        .commit();
            }
        });

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertIncorrecto.dismiss();
            }
        });

        alertIncorrecto.show();

    }

    /** alerta para indicar que se generó un error en el cierre **/
    public static void errorCierre(String mensaje, Context context){
        androidx.appcompat.app.AlertDialog.Builder dialogo1 = new androidx.appcompat.app.AlertDialog.Builder(context);
        dialogo1.setTitle(context.getString(R.string.importante));
        dialogo1.setMessage(mensaje);
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton(context.getString(R.string.confirmar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.dismiss();
            }
        });
        dialogo1.show();
    }

    /** solicita el tipo de etiquetas a buscar **/
    public static void alertBusqueda(Context context, LayoutInflater layoutInflater, final MainActivity mainActivity){
        final android.app.AlertDialog alertBusqueda;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(context);


        View dialog = layoutInflater.inflate(R.layout.alert_select_busqueda, null);

        final Button otras = dialog.findViewById(R.id.btn_otras);
        final Button century = dialog.findViewById(R.id.btn_century);

        busqueda.setView(dialog);
        alertBusqueda = busqueda.create();
        alertBusqueda.setCancelable(true);

        otras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertBusqueda.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.busqueda));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new BusquedaOtrasFragment())
                        .commit();
            }
        });
        century.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertBusqueda.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.busqueda_century));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new BusquedaCenturyFragment())
                        .commit();
            }
        });
        alertBusqueda.show();
    }


    /** solicita el tipo de etiquetas a buscar **/
    public static void alertEnlace(Context context, LayoutInflater layoutInflater, final MainActivity mainActivity){
        final android.app.AlertDialog alertEnlace;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(context);


        View dialog = layoutInflater.inflate(R.layout.alert_select_enlace, null);

        final Button unico = dialog.findViewById(R.id.btnUnico);
        final Button masivo = dialog.findViewById(R.id.btnMasivo);

        busqueda.setView(dialog);
        alertEnlace = busqueda.create();
        alertEnlace.setCancelable(false);

        unico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.unico));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new EnlaceUnicoFragment())
                        .commit();
            }
        });
        masivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.masivo));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new EnlaceMasivoFragment())
                        .commit();
            }
        });
        alertEnlace.show();
    }

    /** solicita el tipo de etiquetas a buscar **/
    public static void alertEnlaceMenu(Context context, LayoutInflater layoutInflater, final MainActivity mainActivity){
        final android.app.AlertDialog alertEnlace;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(context);


        View dialog = layoutInflater.inflate(R.layout.alert_select_enlace, null);

        final Button unico = dialog.findViewById(R.id.btnUnico);
        final Button masivo = dialog.findViewById(R.id.btnMasivo);

        busqueda.setView(dialog);
        alertEnlace = busqueda.create();
        alertEnlace.setCancelable(true);

        unico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.unico));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new EnlaceUnicoFragment())
                        .commit();
            }
        });
        masivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.masivo));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new EnlaceMasivoFragment())
                        .commit();
            }
        });
        alertEnlace.show();
    }

    /** solicita el tipo de etiquetas a buscar **/
    public static void alertDesenlaceMenu(Context context, LayoutInflater layoutInflater, final MainActivity mainActivity){
        final android.app.AlertDialog alertEnlace;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(context);


        View dialog = layoutInflater.inflate(R.layout.alert_select_enlace, null);

        final Button unico = dialog.findViewById(R.id.btnUnico);
        final Button masivo = dialog.findViewById(R.id.btnMasivo);

        busqueda.setView(dialog);
        alertEnlace = busqueda.create();
        alertEnlace.setCancelable(true);

        unico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.unico));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new DesenlaceUnicoFragment())
                        .commit();
            }
        });
        masivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                mainActivity.getSupportActionBar().setTitle(mainActivity.getString(R.string.masivo));
                mainActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new DesenlaceMasivoFragment())
                        .commit();
            }
        });
        alertEnlace.show();
    }

    /** alerta para indicar que se cambio la direccion de conexion para la aplicación **/
    public static void cambioDatos(final Activity activity){
        androidx.appcompat.app.AlertDialog.Builder dialogo1 = new androidx.appcompat.app.AlertDialog.Builder(activity);
        dialogo1.setTitle(activity.getApplication().getString(R.string.importante));
        dialogo1.setMessage(activity.getString(R.string.mensaje_cambio_ruta));
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton(activity.getApplication().getString(R.string.confirmar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.dismiss();
                activity.startActivity(new Intent(activity, SplashActivity.class));
                activity.finish();
            }
        });
        dialogo1.show();
    }






}
