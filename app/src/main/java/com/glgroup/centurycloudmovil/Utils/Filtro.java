package com.glgroup.centurycloudmovil.Utils;

import java.util.ArrayList;
import java.util.Map;

public class Filtro {

    //metodo que toma las etiquetas leidas y filtra los campos para evitar datos innecesarios.
    public  static String Filtro(Map<String, String> tagList) {
        ArrayList<String> listEtiquetas = new ArrayList<String>();
        String etiquetas = "";
        try{
            for(String key : tagList.keySet()) {
                listEtiquetas.add(key.trim());
            }
           etiquetas = String.valueOf(listEtiquetas).replace("[","").replace("]","").replace(" ","");



        }catch(Exception e){
            etiquetas = String.valueOf(e);
        }

        return etiquetas;
    }
}
