package com.glgroup.centurycloudmovil.Utils;

import android.content.Context;

import com.glgroup.centurycloudmovil.R;
import com.google.android.material.textfield.TextInputLayout;

public class Validaciones {

    public boolean camposLogin(TextInputLayout usuario, TextInputLayout pass, Context context){
        boolean valido = true;

        if(usuario.getEditText().getText().toString().equals("")){
            usuario.setError(context.getString(R.string.requerido));
            valido = false;
        }else{
            usuario.setError(null);
        }
        if(pass.getEditText().getText().toString().equals("")){
            pass.setError(context.getString(R.string.requerido));
            valido = false;
        }else{
            pass.setError(null);
        }


        return valido;
    }
}
